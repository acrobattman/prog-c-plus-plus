/**
 * @file circle.h
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie IV - La classe cercle 
 * 
 **/

#ifndef circle_h
#define circle_h

#include "point.h"

/**
 * @brief Classe permettant de représenter des cercles.
 */
class Circle
{
public:
    Circle(const Point &center, int diameter);

    const Point getCenter();
    void setCenter(const Point &newCenter);
    inline int getDiameter();
    inline void setDiameter(int newDiameter);

    float calculatePerimeter();
    float calculateArea();

    bool returnsIfAPointIsOnTheCircle(const Point &pointToTest);
    bool returnsIfAPointIsInTheCircle(const Point &pointToTest);

    void display();

private:
    Point m_center;
    int m_diameter;
};

#endif /* circle_h */