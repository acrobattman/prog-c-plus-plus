/**
 * @file utils.cpp
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Fonctions utiles pour le TP.
 * 
 **/

#include <iostream>
#include <math.h>

/**
 * @brief Fonction qui permet de comparer deux float
 * @param a Un float
 * @param b Un float
 * @return True si a est égal à b false sinon.
 */
bool compareFloat(double a, double b)
{
    return abs(a - b) < 0.001;
}