/**
 * @file rectangle.h
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie II - La classe rectangle
 * 
 **/

#include "point.h"

/**
 * @brief Classe permettant de représenter des rectangle.
 */
class Rectangle
{

public:
    Rectangle(float length, float width);
    Rectangle(float length, float width, const Point &coordinatesOfTheLeftCorner);

    inline int getLength();
    inline void setLength(int newLength);
    inline int getWidth();
    inline void setWidth(int newWidth);
    const Point getCoordinatesOfTheLeftCorner();
    void setCoordinatesOfTheLeftCorner(const Point &newCoordinatesOfTheLeftCorner);

    float calculatePerimeter();
    float calculateArea();

    bool comparesThePerimeterOfTheCurrentRectangleWithTheOnePassedAsParameter(Rectangle &rectangle);
    bool comparesTheAreaOfTheCurrentRectangleWithTheOnePassedAsParameter(Rectangle &rectangle);

    void display();

private:
    int m_length;
    int m_width;

    Point m_coordinatesOfTheLeftCorner;
};