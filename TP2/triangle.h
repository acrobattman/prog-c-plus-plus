/**
 * @file triangle.h
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie III - La classe triangle
 * 
 **/

#include "point.h"

/**
 * Classe permettant de représenter des triangles.
 */
class Triangle
{
public:
    Triangle(const Point &firstPoint, const Point &secondPoint, const Point &thirdPoint);
    const Point getFirstPointTriangle();
    void setFirstPointTriangle(const Point &newFirstPointTriangle);
    const Point getSecondPointTriangle();
    void setSecondPointTriangle(const Point &newSecondPointTriangle);
    const Point getThirdPointTriangle();
    void setThirdPointTriangle(const Point &newThirdPointTriangle);

    float returnTheBase();
    float calculateTheHeight();
    float calculateArea();

    bool isIsosceles();
    bool isRightAngled();
    bool isEquilateral();

    void display();

private:
    Point m_firstPointTriangle;
    Point m_secondPointTriangle;
    Point m_thirdPointTriangle;

    // Méthode en privé car appelé dans plein de méthode de la classe mais pas utile pour l'utilisateur de la classe.
    void lengths(float *pointerLenghtFirstPointToSecondPoint, float *pointerLenghtSecondPointToThirdPoint, float *pointerLenghtThirdPointToFirstPoint);
};