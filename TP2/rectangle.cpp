/**
 * @file rectangle.cpp
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie II - La classe rectangle
 * 
 **/

#include <iostream>

#include "rectangle.h"

/**
 * @brief Constructeur de la classe.
 * @param length Longueur du rectangle.
 * @param width Largeur du rectangle.
 */
Rectangle::Rectangle(float length, float width)
{
    // La longueur doit être plus grande que la largeur et tous deux doivent de plus être positifs !
    if (length < width && length > 0 && width > 0)
    {
        m_length = width;
        m_width = length;
    }
    else if (length >= width && length > 0 && width > 0)
    {
        m_length = length;
        m_width = width;
    }
    else
    {
        // Si les conditions ne sont pas respectées, on met les valeurs à 1.
        m_length = 1;
        m_width = 1;
    }
}

/**
 * @brief Constructeur de la classe.
 * @param length Longueur du rectangle.
 * @param width Largeur du rectangle.
 * @param coordinatesOfTheLeftCorne Coordonnées du coin supérieur gauche.
 */
Rectangle::Rectangle(float length, float width, const Point &coordinatesOfTheLeftCorner)
{
    // La longueur doit être plus grande que la largeur et tous deux doivent de plus être positifs !
    if (length < width && length > 0 && width > 0)
    {
        m_length = width;
        m_width = length;
    }
    else if (length >= width && length > 0 && width > 0)
    {
        m_length = length;
        m_width = width;
    }
    else
    {
        // Si les conditions ne sont pas respectées, on met les valeurs à 1.
        m_length = 1;
        m_width = 1;
    }

    m_coordinatesOfTheLeftCorner = coordinatesOfTheLeftCorner;
}

/**
 * @brief Getter pour la longeur du rectangle.
 * @return La longueur du rectangle.
 */
inline int Rectangle::getLength()
{
    return m_length;
}

/**
 * @brief Setter pour la longueur du rectangle.
 * @param newLength La nouvelle longueur du rectangle.
 */
inline void Rectangle::setLength(int newLenght)
{
    m_length = newLenght;
}

/**
 * @brief Getter pour la largeur du rectangle.
 * @return La largeur du rectangle.
 */
inline int Rectangle::getWidth()
{
    return m_width;
}

/**
 * @brief Setter pour la largeur du rectangle.
 * @param newWidth La nouvelle largeur du rectangle. 
 */
inline void Rectangle::setWidth(int newWidth)
{
    m_width = newWidth;
}

/**
 * @brief Getter pour le Point supérieur droit.
 * @return Le Point supérieur droit.
 */
const Point Rectangle::getCoordinatesOfTheLeftCorner()
{
    return m_coordinatesOfTheLeftCorner;
}

/**
 * @brief Setter pour le Point supérieur droit.
 * @param newCoordinatesOfTheLeftCorner Le nouveau Point supérieur droit.
 */
void Rectangle::setCoordinatesOfTheLeftCorner(const Point &newCoordinatesOfTheLeftCorner)
{
    m_coordinatesOfTheLeftCorner = newCoordinatesOfTheLeftCorner;
}

/**
 * @brief Méthode qui calcule le périmètre d'un rectangle.
 * @return Le périmètre du rectangle.
 */
float Rectangle::calculatePerimeter()
{
    return (getLength() + getWidth()) * 2;
}

/**
 * @brief Méthode qui calcule l'air d'un rectangle.
 * @return L'air du rectangle.
 */
float Rectangle::calculateArea()
{
    return getLength() * getWidth();
}

/**
 * @brief Méthode qui compare le périmètre du rectangle courant avec celui du rectangle passé en paramètre.
 * @param rectangle Référence sur le rectangle avec lequel on veut comparer le périmètre du rectangle courant.
 * @return Vrai ou faux selon si le périmètre du rectangle courant est inférieur ou supérieur au rectangle passé en paramètre.
 */
bool Rectangle::comparesThePerimeterOfTheCurrentRectangleWithTheOnePassedAsParameter(Rectangle &rectangle)
{
    return rectangle.calculatePerimeter() <= calculatePerimeter();
}

/**
 * @brief Méthode qui compare la surface du rectangle courant avec celle du rectangle passé en paramètre.
 * @param rectangle Référence sur le rectangle avec lequel on veut comparer la surface du rectangle courant.
 * @return Vrai ou faux selon si la surface du rectangle courant est inférieur ou supérieur au rectangle passé en paramètre.
 */
bool Rectangle::comparesTheAreaOfTheCurrentRectangleWithTheOnePassedAsParameter(Rectangle &rectangle)
{
    return rectangle.calculateArea() <= calculateArea();
}

/**
 * @brief Affiche les informations sur un rectangle.
 */
void Rectangle::display()
{
    std::cout << "La longeur du rectangle est : " << getLength() << std::endl
              << "La largeur du rectangle est : " << getWidth() << std::endl
              << "Le périmètre du rectangle est : " << calculatePerimeter() << std::endl
              << "La surface du rectangle est : " << calculateArea() << std::endl
              << "Les coordonnées du sommet droit sont : (" << getCoordinatesOfTheLeftCorner().x
              << " ," << getCoordinatesOfTheLeftCorner().y << ")" << std::endl;
}