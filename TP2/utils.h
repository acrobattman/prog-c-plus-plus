/**
 * @file utils.h
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Fonctions utiles pour le TP.
 * 
 **/

#ifndef utils_h
#define utils_h

bool compareFloat(double a, double b);

#endif /* utils_h */