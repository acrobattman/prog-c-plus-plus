#include <iostream>
#include <math.h>

#include "point.h"
#include "circle.h"

/**
 * @brief Constructeur de la classe Circle.
 * @param center Le point représentant le centre du cercle.
 * @param diameter Le diamètre du cercle.
 */
Circle::Circle(const Point &center, int diameter) : m_center(center), m_diameter(diameter)
{
}

/**
 * @brief Getter qui retourne le centre du cercle. 
 * @return le centre du cercle.
 */
const Point Circle::getCenter()
{
    return m_center;
}

/**
 * @brief Setter qui permet de modifier le centre du cercle.
 * @param newCenter La référence sur le point qui sera le nouveau centre du cercle.
 */
void Circle::setCenter(const Point &newCenter)
{
    m_center = newCenter;
}

/**
 * Getter qui retourne le diametre du cercle.
 * @return Le diamètre du cercle.
 */
inline int Circle::getDiameter()
{
    return m_diameter;
}

/**
 * @brief Setter qui permet de modifier le diamètre du cercle.
 * @param newCenter La valeur du nouveau diamètre du cercle.
 */
inline void Circle::setDiameter(int newDiameter)
{
    m_diameter = newDiameter;
}

/**
 * @brief Méthode qui calcul le périmètre du cercle.
 * @return Le périmètre du cercle.
 */
float Circle::calculatePerimeter()
{
    float radius = getDiameter() / 2;
    return 2 * M_PI * radius;
}

/**
 * @brief Méthode qui calcul la surface du cercle.
 * @return La surface du cercle.
 */
float Circle::calculateArea()
{
    float radius = getDiameter() / 2;
    return M_PI * powf(radius, 2);
}

/**
 * @brief Méthode qui retourne si un point est sur le cercle ou non.
 * @details Pour savoir si un point est sur le cercle, on regarde si le rayon du cercle est égal à la distance 
 * entre le centre du cercle et point souhaité.
 * @param pointToTest Le point dont on veut savoir s'il est sur le cercle ou non.
 * @return Vrai ou faux selon que le point est sur le cercle ou non.
 */
bool Circle::returnsIfAPointIsOnTheCircle(const Point &pointToTest)
{
    float distanceBetweenCenterAndThePointToTest = Point::calculateTheDistanceBetweenTwoPoint(getCenter(), pointToTest);
    float radius = getDiameter() / 2;

    return radius == distanceBetweenCenterAndThePointToTest;
}

/**
 * @brief Méthode qui retourne si un point est dans le cercle ou non (mais pas sur le cercle).
 * @details Pour savoir si un point est dans le cercle, on regarde si le rayon du cercle est supérieur à la distance 
 * entre le centre du cercle et point souhaité.
 * @param pointToTest Le point dont on veut savoir s'il est dans le cercle ou non.
 * @return Vrai ou faux selon que le point est dans le cercle ou non.
 */
bool Circle::returnsIfAPointIsInTheCircle(const Point &pointToTest)
{
    float distanceBetweenCenterAndThePointToTest = Point::calculateTheDistanceBetweenTwoPoint(getCenter(), pointToTest);
    float radius = getDiameter() / 2;

    return distanceBetweenCenterAndThePointToTest < radius;
}

/**
 * @brief Méthode qui affiche les informations sur un cercle.
 * @details Affiche les coordonnées du centre du cercle, son diamètre, son périmètre et sa surface.
 */
void Circle::display()
{
    std::cout << "The coordinates of the center of the circle are : (" << getCenter().x << "," << getCenter().y
              << "), its diameter is : " << getDiameter() << ", its perimeter is : " << calculatePerimeter()
              << " and its area is : " << calculateArea() << std::endl;
}