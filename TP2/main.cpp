#include <iostream>

#include "point.h"
#include "circle.h"
#include "rectangle.h"
#include "triangle.h"

int main()
{
    std::cout << "----------- STRUCTURE POINT -----------" << std::endl
              << std::endl;

    Point A = Point(3, 4);
    Point B = Point(2, 6);
    Point C = Point(-3, -1);

    A.display();
    std::cout << std::endl;

    B.display();
    std::cout << std::endl;

    C.display();
    std::cout << std::endl;

    std::cout << "----------- FIN STRUCTURE POINT -----------" << std::endl
              << std::endl;

    // --------------------------------------- Classe Rectangle --------------------------------

    std::cout << "----------- CLASSE RECTANGLE -----------" << std::endl
              << std::endl;

    Rectangle rectangleA = Rectangle(10, 5);

    // Ici la largeur et plus grande que la longueur. Le constructeur devrait donc inverser les 2 valeurs.
    Rectangle rectangleB = Rectangle(10, 20);

    // Ici la longueur est négative, le constructeur devrait donc créer un rectangle de coté 1 et 1 soit un carré.
    Rectangle rectangleC = Rectangle(-10, 2);

    // Ici on crée un rectangle en passant le point représantant le coin supérieur gauche.
    Rectangle rectangleD = Rectangle(2, 5, A);

    rectangleA.display();
    std::cout << std::endl;

    rectangleB.display();
    std::cout << std::endl;

    rectangleC.display();
    std::cout << std::endl;

    rectangleD.display();
    std::cout << std::endl;

    std::cout << "Le périmètre du rectangleB plus grand que rectangleA : " << rectangleB.comparesThePerimeterOfTheCurrentRectangleWithTheOnePassedAsParameter(rectangleA) << std::endl
              << std::endl;

    std::cout << "Le périmètre du rectangleD plus grand que rectangleA : " << rectangleD.comparesThePerimeterOfTheCurrentRectangleWithTheOnePassedAsParameter(rectangleA) << std::endl
              << std::endl;

    std::cout << "L'air du rectangleB plus grand que rectangleA : " << rectangleB.comparesTheAreaOfTheCurrentRectangleWithTheOnePassedAsParameter(rectangleA) << std::endl
              << std::endl;

    std::cout << "L'air du rectangleD plus grand que rectangleA : " << rectangleD.comparesTheAreaOfTheCurrentRectangleWithTheOnePassedAsParameter(rectangleA) << std::endl
              << std::endl;

    std::cout << "----------- FIN CLASSE RECTANGLE -----------" << std::endl
              << std::endl;

    // --------------------------------------- Classe Triangle --------------------------------
    std::cout << "----------- CLASSE TRIANGLE -----------" << std::endl
              << std::endl;

    // Triangle isocèle
    Point D = Point(0, 1);
    Point E = Point(4, 1);
    Point F = Point(2, 4);

    Triangle triangleA = Triangle(D, E, F);

    triangleA.display();
    std::cout << std::endl;

    // Triangle rectangle
    Point G = Point(2, 2);
    Point H = Point(2, 5);
    Point I = Point(6, 5);

    Triangle triangleB = Triangle(G, H, I);

    triangleB.display();
    std::cout << std::endl;

    // Triangle rectangle isocele
    Point J = Point(5, 5);

    Triangle triangleC = Triangle(G, H, J);

    triangleC.display();
    std::cout << std::endl;

    // triangle equilatèral

    std::cout << "----------- FIN CLASSE TRIANGLE ------------" << std::endl
              << std::endl;

    // --------------------------------------- Classe Cercle --------------------------------
    std::cout << "----------- CLASSE CERCLE -----------" << std::endl
              << std::endl;

    Point K = Point(0, 0);

    Circle circleA = Circle(A, 10);
    Circle circleB = Circle(K, 20);

    circleA.display();
    std::cout << std::endl;

    circleB.display();
    std::cout << std::endl;

    // Point qui sera dans le cercle
    Point L = Point(0, 3);
    std::cout << "Le Point L est dans le cercle : " << circleB.returnsIfAPointIsInTheCircle(L) << std::endl
              << std::endl;

    // Point qui sera sur le cercle
    Point M = Point(0, 10);
    std::cout << "Le Point M est sur le cercle : " << circleB.returnsIfAPointIsOnTheCircle(M) << std::endl
              << std::endl;

    // Le point est dans le cercle mais pas dessus.
    std::cout << "Le Point L est sur le cercle : " << circleB.returnsIfAPointIsOnTheCircle(L) << std::endl
              << std::endl;

    std::cout << "----------- FIN CLASSE CERCLE -----------" << std::endl
              << std::endl;

    return 0;
}