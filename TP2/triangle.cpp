/**
 * @file triangle.cpp
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie III - La classe triangle
 * 
 **/

#include <iostream>
#include <math.h>
#include <initializer_list>

#include "utils.h"

#include "point.h"
#include "triangle.h"

/**
 * @brief Constructeur de la classe Triangle.
 */
Triangle::Triangle(const Point &firstPoint, const Point &secondPoint, const Point &thirdPoint) : m_firstPointTriangle(firstPoint), m_secondPointTriangle(secondPoint), m_thirdPointTriangle(thirdPoint)
{
}

/**
 * @brief Getter pour le premier Point. 
 */
const Point Triangle::getFirstPointTriangle()
{
    return m_firstPointTriangle;
}

/**
 * @brief Setter pour le premier point.
 * @param newFirstPointTriangle Les nouvelles coordonnées du premier Point.
 */
void Triangle::setFirstPointTriangle(const Point &newFirstPointTriangle)
{
    m_firstPointTriangle = newFirstPointTriangle;
}

/**
 * @brief Getter pour le second Point.
 */
const Point Triangle::getSecondPointTriangle()
{
    return m_secondPointTriangle;
}

/**
 * @brief Setter pour le deuxieme point.
 * @param newSecondPointTriangle Les nouvelles coordonnées du deuxieme Point.
 */
void Triangle::setSecondPointTriangle(const Point &newSecondPointTriangle)
{
    m_secondPointTriangle = newSecondPointTriangle;
}

/**
 *  Getter pour le troisieme point.
 */
const Point Triangle::getThirdPointTriangle()
{
    return m_thirdPointTriangle;
}

/**
 * @brief Setter pour le troisieme Point.
 * @param newThirdPointTriangle  Les nouvelles coordonnées du troisieme Point.
 */
void Triangle::setThirdPointTriangle(const Point &newThirdPointTriangle)
{
    m_thirdPointTriangle = newThirdPointTriangle;
}

/**
 * @brief Méthode qui retourne le coté le plus long du triangle qui servira de base.
 * @return La longueur du plus long coté.
 */
float Triangle::returnTheBase()
{
    float lengthFirstPointToSecondPoint;
    float lengthSecondPointToThirdPoint;
    float lengthThirdPointToFirstPoint;

    lengths(&lengthFirstPointToSecondPoint, &lengthSecondPointToThirdPoint, &lengthThirdPointToFirstPoint);

    return std::max({lengthFirstPointToSecondPoint, lengthSecondPointToThirdPoint, lengthThirdPointToFirstPoint});
}

/**
 * @brief Méthode qui calcule la hauteur du triangle.
 * @return La hauteur du triangle.
 */
float Triangle::calculateTheHeight()
{
    float areaOfTheTriangle = calculateArea();
    float baseOfTheTriangle = returnTheBase();

    return (2 * areaOfTheTriangle) / baseOfTheTriangle;
}

/**
 * @brief Méthode qui calcule l'air du triangle.
 * @return L'air du triangle.
 */
float Triangle::calculateArea()
{
    float lengthFirstPointToSecondPoint;
    float lengthSecondPointToThirdPoint;
    float lengthThirdPointToFirstPoint;

    lengths(&lengthFirstPointToSecondPoint, &lengthSecondPointToThirdPoint, &lengthThirdPointToFirstPoint);

    float halfPerimeter = (lengthFirstPointToSecondPoint + lengthSecondPointToThirdPoint + lengthThirdPointToFirstPoint) / 2;

    return sqrtf(halfPerimeter * (halfPerimeter - lengthFirstPointToSecondPoint) * (halfPerimeter - lengthSecondPointToThirdPoint) * (halfPerimeter - lengthThirdPointToFirstPoint));
}

/**
 * @brief Méthode qui retourne si un triangle est isocèle ou non.
 * @return Retourne true si le triangle est isocèle et false sinon.
 */
bool Triangle::isIsosceles()
{
    float lengthFirstPointToSecondPoint;
    float lengthSecondPointToThirdPoint;
    float lengthThirdPointToFirstPoint;

    lengths(&lengthFirstPointToSecondPoint, &lengthSecondPointToThirdPoint, &lengthThirdPointToFirstPoint);

    return ((compareFloat(lengthFirstPointToSecondPoint, lengthSecondPointToThirdPoint) && !compareFloat(lengthSecondPointToThirdPoint, lengthThirdPointToFirstPoint)) ||
            (compareFloat(lengthFirstPointToSecondPoint, lengthThirdPointToFirstPoint) && !compareFloat(lengthSecondPointToThirdPoint, lengthThirdPointToFirstPoint)) ||
            (compareFloat(lengthSecondPointToThirdPoint, lengthThirdPointToFirstPoint) && !compareFloat(lengthFirstPointToSecondPoint, lengthSecondPointToThirdPoint)));
}

/**
 * @brief Méthode qui retourne si un triangle est rectangle ou non.
 * @return Retourne true si le triangle est rectangle et false sinon.
 */
bool Triangle::isRightAngled()
{
    float lengthFirstPointToSecondPoint;
    float lengthSecondPointToThirdPoint;
    float lengthThirdPointToFirstPoint;

    lengths(&lengthFirstPointToSecondPoint, &lengthSecondPointToThirdPoint, &lengthThirdPointToFirstPoint);

    float lengthFirstPointToSecondPointPluslengthSecondPointToThirdPointSquared = powf(lengthFirstPointToSecondPoint, 2) + powf(lengthSecondPointToThirdPoint, 2);

    float lengthSecondPointToThirdPointPluslengthThirdPointToFirstPointSquared = powf(lengthSecondPointToThirdPoint, 2) + powf(lengthThirdPointToFirstPoint, 2);

    float lengthThirdPointToFirstPointPlusenghtFirstPointToSecondPointSquared = powf(lengthThirdPointToFirstPoint, 2) + powf(lengthFirstPointToSecondPoint, 2);

    float hypotenuseSquared = powf(returnTheBase(), 2);

    return compareFloat(hypotenuseSquared, lengthFirstPointToSecondPointPluslengthSecondPointToThirdPointSquared) || compareFloat(hypotenuseSquared, lengthSecondPointToThirdPointPluslengthThirdPointToFirstPointSquared) || compareFloat(hypotenuseSquared, lengthThirdPointToFirstPointPlusenghtFirstPointToSecondPointSquared);
}

/**
 * @brief Méthode qui retourne si un triangle est équilatéral ou non.
 * @return Retourne true si le triangle est équilatéral et false sinon.
 */
bool Triangle::isEquilateral()
{
    float lengthFirstPointToSecondPoint;
    float lengthSecondPointToThirdPoint;
    float lengthThirdPointToFirstPoint;

    lengths(&lengthFirstPointToSecondPoint, &lengthSecondPointToThirdPoint, &lengthThirdPointToFirstPoint);

    return compareFloat(lengthFirstPointToSecondPoint, lengthSecondPointToThirdPoint) && compareFloat(lengthSecondPointToThirdPoint, lengthThirdPointToFirstPoint);
}

/**
 * @brief Affiche les informations sur un triangle.
 */
void Triangle::display()
{
    std::cout << "Les coordonnées du premier point sont : (" << getFirstPointTriangle().x << " ," << getFirstPointTriangle().y << ")" << std::endl
              << "Les coordonnées du second point sont : (" << getSecondPointTriangle().x << " ," << getSecondPointTriangle().y << ")" << std::endl
              << "Les coordonnées du troisième point sont : (" << getThirdPointTriangle().x << " ," << getThirdPointTriangle().y << ")" << std::endl
              << "La base du triangle est : " << returnTheBase() << std::endl
              << "La hauteur du triangle est : " << calculateTheHeight() << std::endl
              << "L'air du triangle est : " << calculateArea() << std::endl
              << "Le triangle est isocèle : " << isIsosceles() << std::endl
              << "Le triangle est rectangle : " << isRightAngled() << std::endl
              << "Le triangle est équilatèral : " << isEquilateral() << std::endl;
}

/**
 * @brief Méthode qui calcule la longueur entre tous les Point du triangle.
 * @param pointerlengthFirstPointToSecondPoint Pointeur sur la longueur entre le 1er et le 2eme Point du triangle.
 * @param pointerlengthSecondPointToThirdPoint Pointeur sur la longueur entre le 2eme et le 3eme Point du triangle.
 * @param pointerlengthThirdPointToFirstPoint Pointeur sur la longueur entre le 3eme et le 1er Point du trianlge.
 */
void Triangle::lengths(float *pointerlengthFirstPointToSecondPoint, float *pointerlengthSecondPointToThirdPoint, float *pointerlengthThirdPointToFirstPoint)
{
    Point firstPoint = getFirstPointTriangle();
    Point secondPoint = getSecondPointTriangle();
    Point thirdPoint = getThirdPointTriangle();

    *pointerlengthFirstPointToSecondPoint = Point::calculateTheDistanceBetweenTwoPoint(firstPoint, secondPoint);
    *pointerlengthSecondPointToThirdPoint = Point::calculateTheDistanceBetweenTwoPoint(secondPoint, thirdPoint);
    *pointerlengthThirdPointToFirstPoint = Point::calculateTheDistanceBetweenTwoPoint(thirdPoint, firstPoint);
}
