/**
 * @file point.h
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie I - La structure point
 * 
 **/

#ifndef point_h
#define point_h

/**
 * @brief Structure permettant de représenter des points.
 */
struct Point
{
    float x;
    float y;

    Point();
    Point(float x, float y);

    static float calculateTheDistanceBetweenTwoPoint(const Point &A, const Point &B);

    void display();
};

#endif /* point_h */