/**
 * @file point.cpp
 * @brief Séance 2 - Quelques bonnes pratiques liées à la programmation en C++
 * @author Maxime ECKSTEIN
 * @date 30 Septembre 2020
 * 
 * Partie I - La structure point
 * 
 **/

#include <iostream>
#include <math.h>

#include "point.h"

/**
 * @brief Constructeur par défaut.
 */
Point::Point()
{
    x = 0;
    y = 0;
}

/**
 * @brief Constructeur de la structure Point
 * @param _x La valeur de la coordonnée en x
 * @param _y La valeur de la coordonnée en y
 */
Point::Point(float _x, float _y)
{
    x = _x;
    y = _y;
}

/**
 * @brief Méthode qui calcul la distance entre 2 points passés en paramètre.
 * @param A La référence du premier point.
 * @param B La référence du second point.
 * @return Retourne la distance entre les points A et B.
 */
float Point::calculateTheDistanceBetweenTwoPoint(const Point &A, const Point &B)
{
    return sqrtf(powf(B.x - A.x, 2) + powf(B.y - A.y, 2));
}

/**
 * @brief Méthode qui affiche les informations sur un point.
 * @details Affiche les coordonnée d'un point.
 */
void Point::display()
{
    std::cout << "The coordinates of the point are : (" << x << "," << y << ")" << std::endl;
}