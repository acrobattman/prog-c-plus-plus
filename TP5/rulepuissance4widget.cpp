#include "rulepuissance4widget.h"

RulePuissance4Widget::RulePuissance4Widget(QWidget *parentWidget)
{
    this->setParent(parentWidget);
    this->setFixedSize(parentWidget->size());

    m_lblTitle = new QLabel("Connect 4", this);
    m_lblTitle->setFont(QFont("Arial", 36));
    m_lblTitle->move(200, 20);
    m_lblTitle->setFixedSize(480,60);

    m_lblCurrentPlayer = new QLabel("current player", this);
    m_lblCurrentPlayer->setFont(QFont("Arial", 15));
    m_lblCurrentPlayer->move(500,150);
    m_lblCurrentPlayer->setFixedSize(200,50);
    m_lblCurrentPlayer->setWordWrap(true);

    m_lblInfos = new QLabel("Info panel", this);
    m_lblInfos->setFont(QFont("Arial", 15));
    m_lblInfos->move(500, 200);
    m_lblInfos->setFixedSize(200,50);
    m_lblInfos->setWordWrap(true);

    m_btnReturn = new QPushButton("Return to selection", this);
    m_btnReturn->setFont(QFont("Arial", 10));
    m_btnReturn->move(250, 450);
    m_btnReturn->setFixedSize(150, 25);
    QObject::connect(m_btnReturn, SIGNAL(clicked()), this->parent(), SLOT(GameSelection()));

    m_currentPlayer = Player();

    m_saveFile = "savePuissance4.json";

    InitBoard();
}

void RulePuissance4Widget::PlayerTurn(const Player & player){
    m_currentPlayer = player;
    m_lblCurrentPlayer->setText("Your turn player "+QString::number(player.valueOfToken));
}

int RulePuissance4Widget::FirstLineOfEmptyCaseInColumn(int columnNumber){
    Column columnToTest=this->grid->CreateColumn(columnNumber);
    for(int lineNumber=this->grid->GetNumberOfLine()-1;lineNumber>=0;--lineNumber){
        if(this->grid->GetCase(lineNumber,columnNumber).CaseIsEmpty()){
            return lineNumber;
        }
    }
    return -1;
}

void RulePuissance4Widget::PlayerPutsToken(){
    int columnNumber = sender()->property(columnIndex).toInt();

    CheckCoordinates(columnNumber);
}

void RulePuissance4Widget::CheckCoordinates(int columnNumber){
    int lineNumber = FirstLineOfEmptyCaseInColumn(columnNumber);

    if(!(this->grid->CaseCoordinatesAreValid(lineNumber,columnNumber))){
        m_lblInfos->setText("Please select valid coordinates");
    }
    else if(!(this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber,columnNumber)))){
        m_lblInfos->setText("Please select an empty column");
    }
    else{
        this->grid->PlayAToken(m_currentPlayer , this->grid->GetCase(lineNumber,columnNumber));
        QString text=QString::number(grid->GetCase(lineNumber,columnNumber).playerOnCase);
        m_boardButtons.at((lineNumber*grid->GetNumberOfColumn())+columnNumber)->setText(text);
        IterateGame();
    }
}

void RulePuissance4Widget::IterateGame(){
    m_lblInfos->setText("Please select a column");

    if(this->VictoryCondition(m_currentPlayer)){
        m_lblInfos->setText("Player "+QString::number(m_currentPlayer.valueOfToken)+" has won !");
        //RemoveSaveFile();
        Rematch();
        return;
    }
    if(NoWinnerEndCondition()){
        m_lblInfos->setText("Game Over : the grid is full");
        //RemoveSaveFile();
        Rematch();
    }
    else{    
    NextPlayer();
    SaveGame();
    }
}

void RulePuissance4Widget::NextPlayer(){
    if(m_currentPlayer.name == playerList[0].name){
        PlayerTurn(playerList[1]);
    }
    else{
        PlayerTurn(playerList[0]);
    }
}

bool RulePuissance4Widget::VictoryCondition(const Player& player){
    return(this->LineVictory(player)||this->ColumnVictory(player)||this->DiagonalVictory(player));
}

bool RulePuissance4Widget::LineVictory(const Player &player){
    for(int lineNumber=0;lineNumber<this->grid->GetNumberOfLine();lineNumber++){
        if(this->grid->LineCompletedByPlayer(lineNumber,player)){
            return true;
        }
    }
    return false;
}



bool RulePuissance4Widget::ColumnVictory(const Player &player){
    for(int columnNumber=0;columnNumber<this->grid->GetNumberOfColumn();columnNumber++){
        if(this->grid->ColumnCompletedByPlayer(columnNumber,player)){
            return true;
        }
    }
    return false;
}

bool RulePuissance4Widget::DiagonalULDRVictory(const Player &player){
    for(int numColumn=0;numColumn<grid->GetNumberOfColumn();++numColumn){
        if(grid->DiagonalULDRCompletedByPlayer(0,numColumn,player)){
            return true;
        }
    }
    return false;
}

bool RulePuissance4Widget::DiagonalURDLVictory(const Player &player){
    for(int numColumn=0;numColumn<grid->GetNumberOfColumn();++numColumn){
        if(grid->DiagonalURDLCompletedByPlayer(0,numColumn,player)){
            return true;
        }
    }
    return false;
}

bool RulePuissance4Widget::DiagonalVictory(const Player &player){
    return (this->DiagonalULDRVictory(player)||this->DiagonalURDLVictory(player));

}

bool RulePuissance4Widget::NoWinnerEndCondition(){
    return this->grid->GridIsFull();
}

void RulePuissance4Widget::GameFlow(){
    m_lblInfos->setText("Would you like to load your previous game?");
    int saveChoice=-1;

    QMessageBox* saveMessage=new QMessageBox(this);
    saveMessage->setText("Load previous file ?");
    saveMessage->setInformativeText("Selecting No will create a new game");
    saveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    saveChoice=saveMessage->exec();

    //
    if(saveChoice == QMessageBox::Yes){
        m_saveFile = QFileDialog::getOpenFileName(this, tr("Open save file"), "..", tr("json Files (*.json)"));
        if(m_saveFile == NULL){
            m_saveFile = "savePuissance4.json";
            m_lblInfos->setText("New Game");
            grid->ClearGrid();
            this->PlayerTurn(playerList[0]);
        }
        else{
            m_lblInfos->setText("Game loaded");
            LoadGame();
            RefreshBoard();
        }
    }
    else{
        m_lblInfos->setText("New Game");
        grid->ClearGrid();
        this->PlayerTurn(playerList[0]);
    }
};

void RulePuissance4Widget::LoadGame(){
    QFile loadFile(m_saveFile);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    QJsonObject json = loadDoc.object();
    if(json["GameType"].toString().compare(m_gameType) == 0){
        Read(json);
    }
    else{
        qWarning("Wrong save file.");
        m_lblInfos->setText("New Game");
        grid->ClearGrid();
        this->PlayerTurn(playerList[0]);
    }
}

void RulePuissance4Widget::SaveGame() const{
    QFile saveFile(m_saveFile);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonObject gameObject;
    Write(gameObject);
    QJsonDocument saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());
}

void RulePuissance4Widget::RemoveSaveFile() const{
    QFile saveFile(m_saveFile);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    saveFile.remove();
}

void RulePuissance4Widget::Read(const QJsonObject &json){

    grid->ClearGrid();
    QJsonArray caseArray = json["cases"].toArray();
    for (int caseIndex = 0; caseIndex < caseArray.size(); ++caseIndex) {
        QJsonObject caseObject = caseArray[caseIndex].toObject();
        Case caseToRead;
        caseToRead.Read(caseObject);
        grid->SetCaseInGrid(caseToRead);
    }

    if(json["playerTurn"].toInt() == 1){
        this->PlayerTurn(playerList[0]);
    }
    else{
        this->PlayerTurn(playerList[1]);
    }
}

void RulePuissance4Widget::Write(QJsonObject &json) const{

    QJsonArray caseArray;
    for(std::vector<Case> listCases : grid->GetGrid()) {
        for(Case caseToWrite : listCases) {
                QJsonObject caseObject;
                caseToWrite.Write(caseObject);
                caseArray.append(caseObject);
            }
    }
    json["GameType"] = m_gameType;
    json["playerTurn"] = m_currentPlayer.valueOfToken;
    json["cases"] = caseArray;
}

void RulePuissance4Widget::InitBoard()
{
    grid->ClearGrid();
    for(int i = 0; i < grid->GetNumberOfLine(); i++)
    {
        for(int j = 0; j < grid->GetNumberOfColumn(); j++)
        {
            QPushButton* btn = new QPushButton(this);
            btn->move(100+j*50, 150+i*50);
            btn->setFixedSize(50, 50);
            btn->setFont(QFont("Arial", 15));
            auto indexI=QString{"%1"}.arg(i);
            auto indexJ=QString{"%1"}.arg(j);
            btn->setProperty(lineIndex,indexI );
            btn->setProperty(columnIndex,indexJ);
            connect(btn,SIGNAL(clicked()),this, SLOT(PlayerPutsToken()));
            m_boardButtons.push_back(btn);
        }
    }
}

void RulePuissance4Widget::RefreshBoard(){
    for(QPushButton* btn : m_boardButtons){
        int lineNumber = btn->property(lineIndex).toInt();
        int columnNumber = btn->property(columnIndex).toInt();
        if(!(grid->GetCase(lineNumber,columnNumber).playerOnCase==0)){
            QString text=QString::number(grid->GetCase(lineNumber,columnNumber).playerOnCase);
            btn->setText(text);
        }
        else{
            btn->setText("");
        }
    }
}

void RulePuissance4Widget::Rematch(){
    QMessageBox* rematchMessage=new QMessageBox(this);
    rematchMessage->setText("Would you like a rematch ?");
    rematchMessage->setInformativeText("Selecting No will close this window");
    rematchMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int rematchChoice=rematchMessage->exec();
    switch(rematchChoice){
        case QMessageBox::Yes:
            grid->ClearGrid();
            RefreshBoard();
            PlayerTurn(playerList[0]);
        break;
        default:
        ;
    }
}

