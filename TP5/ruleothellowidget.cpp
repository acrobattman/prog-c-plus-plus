#include "ruleothellowidget.h"

RuleOthelloWidget::RuleOthelloWidget(QWidget *parentWidget)
{
    this->setParent(parentWidget);
    this->setFixedSize(parentWidget->size());

    m_lblTitle = new QLabel("Othello", this);
    m_lblTitle->setFont(QFont("Arial", 36));
    m_lblTitle->move(200, 20);
    m_lblTitle->setFixedSize(480,60);

    m_lblCurrentPlayer = new QLabel("current player", this);
    m_lblCurrentPlayer->setFont(QFont("Arial", 15));
    m_lblCurrentPlayer->move(500,150);
    m_lblCurrentPlayer->setFixedSize(200,50);
    m_lblCurrentPlayer->setWordWrap(true);

    m_lblInfos = new QLabel("Info panel", this);
    m_lblInfos->setFont(QFont("Arial", 15));
    m_lblInfos->move(500, 200);
    m_lblInfos->setFixedSize(200,50);
    m_lblInfos->setWordWrap(true);

    m_btnReturn = new QPushButton("Return to selection", this);
    m_btnReturn->setFont(QFont("Arial", 10));
    m_btnReturn->move(250, 550);
    m_btnReturn->setFixedSize(150, 25);
    QObject::connect(m_btnReturn, SIGNAL(clicked()), this->parent(), SLOT(GameSelection()));

    m_currentPlayer = Player();

    m_saveFile = "saveOthello.json";

    InitBoard();
}

std::unique_ptr<GridDynamic>& RuleOthelloWidget::GetGrid(){
    return this->grid;
}

void RuleOthelloWidget::SetupPlayer1(){
    Case& caseToSetup1=grid->GetCase(4,3);
    caseToSetup1.playerOnCase=1;
    Case& caseToSetup2=grid->GetCase(3,4);
    caseToSetup2.playerOnCase=1;
}
void RuleOthelloWidget::SetupPlayer2(){
    Case& caseToSetup1=grid->GetCase(3,3);
    caseToSetup1.playerOnCase=2;
    Case& caseToSetup2=grid->GetCase(4,4);
    caseToSetup2.playerOnCase=2;
}

void RuleOthelloWidget::Setup(){
    for(int numLine=0;numLine<grid->GetNumberOfLine();++numLine){
        for(int numColumn=0;numColumn<grid->GetNumberOfColumn();++numColumn){
            Case& caseToSetup=grid->GetCase(numLine,numColumn);
            caseToSetup.ResetDynamicCase();
        }
    }
    this->SetupPlayer1();
    this->SetupPlayer2();
}

void RuleOthelloWidget::PlayerTurn(const Player &player){
    if(PlayerCanPutValidTokenInGrid(player)){
        m_currentPlayer = player;
        m_lblCurrentPlayer->setText("Your turn player "+QString::number(player.valueOfToken));
    }
}

bool RuleOthelloWidget::PlayerCanPutValidTokenInGrid(const Player & player){
    for (int lineTest=0 ;lineTest<grid->GetNumberOfLine();lineTest++){
        for (int columnTest=0 ;columnTest<grid->GetNumberOfColumn();columnTest++){
            Case caseToCheck = grid->GetCase(lineTest,columnTest);
            if (PlayerCanPutValidTokenOnCase(player, caseToCheck)) {
                return true;
            }
        }
    }
return false;
};

bool RuleOthelloWidget::PlayerCanPutValidTokenOnCase(const Player & player, const Case & caseToCheck){
if(!caseToCheck.CaseIsEmpty()){
    return false;
}
else{
    return ValidMovePossibleOnLineOrColumnOrDiagonal(player, caseToCheck);
    };
}

bool RuleOthelloWidget::ValidMovePossibleOnLineOrColumnOrDiagonal(const Player &player, const Case &caseToCheck){
  return ValidMovePossibleOnLine(player, caseToCheck)||ValidMovePossibleOnColumn(player,caseToCheck)||ValidMovePossibleOnDiagonal(player, caseToCheck);
};

bool RuleOthelloWidget::ValidMovePossibleOnLine(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnLineToLeft(player, caseToCheck)||ValidMovePossibleOnLineToRight(player, caseToCheck);
}

bool RuleOthelloWidget::ValidMovePossibleOnLineToLeft(const Player &player, const Case &caseToPlay){
   int lineToCheck=caseToPlay.GetNumLine();
   int columnToCheck=caseToPlay.GetNumColumn();
   if(columnToCheck==0){
       return false;
   }
   columnToCheck=columnToCheck-1;
   Case & caseOnLeft=grid->GetCase(lineToCheck,columnToCheck);
   if(columnToCheck==0||caseOnLeft.CaseBelongsToPlayerValue(player.valueOfToken)||caseOnLeft.CaseIsEmpty()){
       return false;
   }
   for(columnToCheck=columnToCheck-1;columnToCheck>=0;--columnToCheck){
        Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
        if(caseToCheck.CaseIsEmpty()){
            return false;
        }
        else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
            return true;
        }
   }
    return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnLineToRight(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(columnToCheck==grid->GetNumberOfColumn()-1){
        return false;
    }
    columnToCheck=columnToCheck+1;
    Case & caseOnRight=grid->GetCase(lineToCheck,columnToCheck);
    if(columnToCheck==0||caseOnRight.CaseBelongsToPlayerValue(player.valueOfToken)||caseOnRight.CaseIsEmpty()){
        return false;
    }
    for(columnToCheck=columnToCheck+1;columnToCheck<grid->GetNumberOfColumn();++columnToCheck){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
    }
     return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnColumn(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnColumnDown(player, caseToCheck)||ValidMovePossibleOnColumnUp(player,caseToCheck);
}

bool RuleOthelloWidget::ValidMovePossibleOnColumnUp(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==0){
        return false;
    }
    lineToCheck=lineToCheck-1;
    Case & caseUp=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==0||caseUp.CaseBelongsToPlayerValue(player.valueOfToken)||caseUp.CaseIsEmpty()){
        return false;
    }
    for(lineToCheck=lineToCheck-1;lineToCheck>=0;--lineToCheck){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
    }
     return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnColumnDown(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==grid->GetNumberOfLine()-1){
        return false;
    }
    lineToCheck=lineToCheck+1;
    Case & caseUp=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==lineToCheck+1||caseUp.CaseBelongsToPlayerValue(player.valueOfToken)||caseUp.CaseIsEmpty()){
        return false;
    }
    for(lineToCheck=lineToCheck+1;lineToCheck<grid->GetNumberOfLine();++lineToCheck){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
    }
     return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonal(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnDiagonalULDR(player,caseToCheck)||ValidMovePossibleOnDiagonalURDL(player, caseToCheck);
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonalULDR(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnDiagonalToUpLeft(player, caseToCheck)||ValidMovePossibleOnDiagonalToDownRight(player,caseToCheck);
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonalURDL(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnDiagonalToUpRight(player, caseToCheck)||ValidMovePossibleOnDiagonalToDownLeft(player, caseToCheck);
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonalToUpLeft(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==0||columnToCheck==0){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck-1;
    Case & caseUpLeft=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==0||columnToCheck==0||caseUpLeft.CaseBelongsToPlayerValue(player.valueOfToken)||caseUpLeft.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck-1;
    while(lineToCheck>=0 && columnToCheck>=0){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck-1;
             columnToCheck=columnToCheck-1;
         }
    }
     return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonalToUpRight(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==0||columnToCheck==grid->GetNumberOfColumn()-1){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck+1;
    Case & caseUpRight=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==0||columnToCheck==grid->GetNumberOfColumn()-1||caseUpRight.CaseBelongsToPlayerValue(player.valueOfToken)||caseUpRight.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck+1;
    while(lineToCheck>=0 && columnToCheck<grid->GetNumberOfColumn()){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck-1;
             columnToCheck=columnToCheck+1;
         }
    }
     return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonalToDownLeft(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==0){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck-1;
    Case & caseDownLeft=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==0||caseDownLeft.CaseBelongsToPlayerValue(player.valueOfToken)||caseDownLeft.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck-1;
    while(lineToCheck<grid->GetNumberOfLine() && columnToCheck>=0){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck+1;
             columnToCheck=columnToCheck-1;
         }
    }
     return false;
}

bool RuleOthelloWidget::ValidMovePossibleOnDiagonalToDownRight(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==grid->GetNumberOfColumn()-1){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck+1;
    Case & caseDownRight=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==grid->GetNumberOfColumn()-1||caseDownRight.CaseBelongsToPlayerValue(player.valueOfToken)||caseDownRight.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck+1;
    while(lineToCheck<grid->GetNumberOfLine() && columnToCheck<grid->GetNumberOfColumn()){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck+1;
             columnToCheck=columnToCheck+1;
         }
    }
     return false;
}

void RuleOthelloWidget::PlayerPutsTokenInGrid(){
    int lineNumber = sender()->property(lineIndex).toInt();
    int columnNumber = sender()->property(columnIndex).toInt();

    CheckCoordinates(lineNumber, columnNumber);
}

void RuleOthelloWidget::CheckCoordinates(int lineNumber, int columnNumber)
{
    bool validMove=false;
    Case & caseToTest=grid->GetCase(lineNumber,columnNumber);
    validMove=PlayerCanPutValidTokenOnCase(m_currentPlayer, caseToTest);

    if(!validMove){
        m_lblInfos->setText("Please select valid coordinates");
    }
    else{
        Case & caseToModify=grid->GetCase(lineNumber,columnNumber);
        FlipTokens(m_currentPlayer, caseToModify);
        caseToModify.playerOnCase=m_currentPlayer.valueOfToken;
        QString text=QString::number(grid->GetCase(lineNumber,columnNumber).playerOnCase);
        m_boardButtons.at((lineNumber*grid->GetNumberOfColumn())+columnNumber)->setText(text);
        IterateGame();
    }
}

void RuleOthelloWidget::IterateGame(){

    m_lblInfos->setText("Please select a square");

    if(EndOfTheGame()){
        EndGameManager();
        //RemoveSaveFile();
        Rematch();
        return;
    }
    else{    
    NextPlayer();
    SaveGame();
    }
}

void RuleOthelloWidget::FlipTokens(const Player &player, const Case &caseToCheck){
    if(ValidMovePossibleOnLineToLeft(player, caseToCheck)){
        FlipTokensToTheLeft(player, caseToCheck);
    }
    if(ValidMovePossibleOnLineToRight(player, caseToCheck)){
        FlipTokensToTheRight(player, caseToCheck);
    }
    if(ValidMovePossibleOnColumnUp(player, caseToCheck)){
        FlipTokensUp(player, caseToCheck);
    }
    if(ValidMovePossibleOnColumnDown(player, caseToCheck)){
        FlipTokensDown(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToUpLeft(player, caseToCheck)){
        FlipTokensToUpLeft(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToUpRight(player, caseToCheck)){
        FlipTokensToUpRight(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToDownLeft(player, caseToCheck)){
        FlipTokensToDownLeft(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToDownRight(player, caseToCheck)){
        FlipTokensToDownRight(player, caseToCheck);
    }
}

void RuleOthelloWidget::FlipTokensToTheLeft(const Player & player, const Case & caseToCheck){
    int lineToCheck=caseToCheck.GetNumLine();
    for(int columnToCheck=caseToCheck.GetNumColumn()-1;columnToCheck>=0;--columnToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
         }
    }
     return;
};

//TODO
void RuleOthelloWidget::FlipTokensToTheRight(const Player & player, const Case & caseToCheck){
    int lineToCheck=caseToCheck.GetNumLine();
    for(int columnToCheck=caseToCheck.GetNumColumn()+1;columnToCheck<grid->GetNumberOfColumn();++columnToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
         }
    }
     return;
};
//TODO
void RuleOthelloWidget::FlipTokensUp(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn();
    for(int lineToCheck=caseToCheck.GetNumLine()-1;lineToCheck>=0;--lineToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
         }
    }
     return;
};
//TODO
void RuleOthelloWidget::FlipTokensDown(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn();
    for(int lineToCheck=caseToCheck.GetNumLine()+1;lineToCheck<grid->GetNumberOfLine();++lineToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
         }
    }
     return;
};
//TODO
void RuleOthelloWidget::FlipTokensToUpLeft(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()-1;
    int lineToCheck=caseToCheck.GetNumLine()-1;
    while(lineToCheck>=0&&columnToCheck>=0){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
             columnToCheck=columnToCheck-1;
             lineToCheck=lineToCheck-1;
         }
    }
     return;
};
//TODO
void RuleOthelloWidget::FlipTokensToUpRight(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()+1;
    int lineToCheck=caseToCheck.GetNumLine()-1;
    while(lineToCheck>=0&&columnToCheck<grid->GetNumberOfColumn()){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
             columnToCheck=columnToCheck+1;
             lineToCheck=lineToCheck-1;
         }
    }
     return;
};
//TODO
void RuleOthelloWidget::FlipTokensToDownLeft(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()-1;
    int lineToCheck=caseToCheck.GetNumLine()+1;
    while(lineToCheck<grid->GetNumberOfLine()&&columnToCheck>=0){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
             columnToCheck=columnToCheck-1;
             lineToCheck=lineToCheck+1;
         }
    }
     return;
};
//TODO
void RuleOthelloWidget::FlipTokensToDownRight(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()+1;
    int lineToCheck=caseToCheck.GetNumLine()+1;
    while(lineToCheck<grid->GetNumberOfLine()||columnToCheck<grid->GetNumberOfColumn()){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             QString text=QString::number(grid->GetCase(caseToModify.GetNumLine(),caseToModify.GetNumColumn()).playerOnCase);
             m_boardButtons.at((caseToModify.GetNumLine()*grid->GetNumberOfColumn())+caseToModify.GetNumColumn())->setText(text);
             columnToCheck=columnToCheck+1;
             lineToCheck=lineToCheck+1;
         }
    }
     return;
};


bool RuleOthelloWidget::VictoryCondition(const Player &player){
    int numberOfCaseForPlayer=0;
    int numberOfCaseForOpponent=0;
    for(int lineNumber=0;lineNumber<grid->GetNumberOfLine();++lineNumber){
        for(int columnNumber=0;columnNumber<grid->GetNumberOfColumn();++columnNumber){
            Case & caseToCheck=grid->GetCase(lineNumber,columnNumber);
            if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
                numberOfCaseForPlayer=numberOfCaseForPlayer+1;
            }
            else if (caseToCheck.CaseBelongsToOpponent(player.valueOfToken)){
                numberOfCaseForOpponent=numberOfCaseForOpponent+1;
            }
        }
    }
    return numberOfCaseForPlayer>numberOfCaseForOpponent;
}

Player& RuleOthelloWidget::GetPlayer1(){
    return playerList[0];
}

Player& RuleOthelloWidget::GetPlayer2(){
    return playerList[1];
}

bool RuleOthelloWidget::EndOfTheGame(){
    return !(PlayerCanPutValidTokenInGrid(GetPlayer1())||PlayerCanPutValidTokenInGrid(GetPlayer2()));
}

bool RuleOthelloWidget::NoWinnerEndCondition(){
  return false;
};

void RuleOthelloWidget::EndGameManager(){
    m_lblInfos->setText("End of the game");
    if(VictoryCondition(GetPlayer1())){
        m_lblInfos->setText("Player "+QString::number(GetPlayer1().valueOfToken)+" has won");
    }
    else if (VictoryCondition(GetPlayer2())){
        m_lblInfos->setText("Player "+QString::number(GetPlayer2().valueOfToken)+" has won");
    }
    else{
        m_lblInfos->setText("personne ne gagne");
    }
    return;
}

void RuleOthelloWidget::GameFlow(){    
    m_lblInfos->setText("Would you like to load your previous game?");
    int saveChoice=-1;

    QMessageBox* saveMessage=new QMessageBox(this);
    saveMessage->setText("Load previous file ?");
    saveMessage->setInformativeText("Selecting No will create a new game");
    saveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    saveChoice=saveMessage->exec();

    if(saveChoice == QMessageBox::Yes){
        m_saveFile = QFileDialog::getOpenFileName(this, tr("Open save file"), "..", tr("json Files (*.json)"));
        if(m_saveFile == NULL){
            m_saveFile = "saveOthello.json";
            m_lblInfos->setText("New Game");
            Setup();
            this->PlayerTurn(playerList[0]);
        }
        else{
            m_lblInfos->setText("Game loaded");
            LoadGame();
            RefreshBoard();
        }
    }
    else{
        m_lblInfos->setText("New Game");
        Setup();
        this->PlayerTurn(playerList[0]);
    }
}

void RuleOthelloWidget::NextPlayer()
{
    if(m_currentPlayer.name == playerList[0].name){
        PlayerTurn(playerList[1]);
    }
    else{
        PlayerTurn(playerList[0]);
    }
}

void RuleOthelloWidget::LoadGame(){
    QFile loadFile(m_saveFile);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    QJsonObject json = loadDoc.object();
    if(json["GameType"].toString().compare(m_gameType) == 0){
        Read(json);
    }
    else{
        qWarning("Wrong save file.");
        m_lblInfos->setText("New Game");
        Setup();
        this->PlayerTurn(playerList[0]);
    }
}

void RuleOthelloWidget::SaveGame() const{
    QFile saveFile(m_saveFile);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonObject gameObject;
    Write(gameObject);
    QJsonDocument saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());
}

void RuleOthelloWidget::RemoveSaveFile() const{
    QFile saveFile(m_saveFile);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    saveFile.remove();
}

void RuleOthelloWidget::Read(const QJsonObject &json){

    grid->ClearGrid();
    QJsonArray caseArray = json["cases"].toArray();
    for (int caseIndex = 0; caseIndex < caseArray.size(); ++caseIndex) {
        QJsonObject caseObject = caseArray[caseIndex].toObject();
        Case caseToRead;
        caseToRead.Read(caseObject);
        grid->SetCaseInGrid(caseToRead);
    }

    if(json["playerTurn"].toInt() == 1){
        this->PlayerTurn(playerList[0]);
    }
    else{
        this->PlayerTurn(playerList[1]);
    }
}

void RuleOthelloWidget::Write(QJsonObject &json) const{

    QJsonArray caseArray;
    for(std::vector<Case> listCases : grid->GetGrid()) {
        for(Case caseToWrite : listCases) {
                QJsonObject caseObject;
                caseToWrite.Write(caseObject);
                caseArray.append(caseObject);
            }
    }
    json["GameType"] = m_gameType;
    json["playerTurn"] = m_currentPlayer.valueOfToken;
    json["cases"] = caseArray;
}

void RuleOthelloWidget::InitBoard()
{
    grid->ClearGrid();
    Setup();
    for(int i = 0; i < grid->GetNumberOfLine(); i++)
    {
        for(int j = 0; j < grid->GetNumberOfColumn(); j++)
        {
            QPushButton* btn = new QPushButton(this);
            btn->move(100+j*40, 150+i*40);
            btn->setFixedSize(40, 40);
            btn->setFont(QFont("Arial", 15));
            auto indexI=QString{"%1"}.arg(i);
            auto indexJ=QString{"%1"}.arg(j);
            btn->setProperty(lineIndex,indexI );
            btn->setProperty(columnIndex,indexJ);
            connect(btn,SIGNAL(clicked()),this, SLOT(PlayerPutsTokenInGrid()));
            QString text=QString::number(grid->GetCase(i,j).playerOnCase);
            if(grid->GetCase(i,j).playerOnCase!=0){
                btn->setText(text);
            }
            m_boardButtons.push_back(btn);
        }
    }
}

void RuleOthelloWidget::RefreshBoard(){
    for(QPushButton* btn : m_boardButtons){
        int lineNumber = btn->property(lineIndex).toInt();
        int columnNumber = btn->property(columnIndex).toInt();
        if(!(grid->GetCase(lineNumber,columnNumber).playerOnCase==0)){
            QString text=QString::number(grid->GetCase(lineNumber,columnNumber).playerOnCase);
            btn->setText(text);
        }
        else{
            btn->setText("");
        }
    }
}

void RuleOthelloWidget::Rematch(){
    QMessageBox* rematchMessage=new QMessageBox(this);
    rematchMessage->setText("Would you like a rematch ?");
    rematchMessage->setInformativeText("Selecting No will close this window");
    rematchMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int rematchChoice=rematchMessage->exec();
    switch(rematchChoice){
        case QMessageBox::Yes:
            grid->ClearGrid();
            RefreshBoard();
            PlayerTurn(playerList[0]);
        break;
        default:
        ;
    }
}

