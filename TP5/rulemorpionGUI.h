#ifndef RuleMorpionGUI_H
#define RuleMorpionGUI_H
#include "gridmorpion.h"
#include "rule.h"
#include <memory>


class RuleMorpionGUI : public Rule
{
public:
    RuleMorpionGUI();
    void PlayerTurn(const Player& player);
    bool VictoryCondition(const Player& player);
    bool NoWinnerEndCondition();
    void GameFlow();
    bool LineVictory(const Player& player);
    bool ColumnVictory(const Player& player);
    bool DiagonalVictory(const Player& player);
    bool DiagonalULDRVictory(const Player& player);
    bool DiagonalURDLVictory(const Player& player);
    void PlayerPutsToken(const Player& player);

    void LoadGame() override;
    void SaveGame() const override;

    void Read(const QJsonObject &json) override;
    void Write(QJsonObject &json) const override;

    std::unique_ptr<GridStatic> grid = GridStaticFactory::createGrid(GridStaticFactory::Morpion);
};

#endif // RuleMorpionGUI_H
