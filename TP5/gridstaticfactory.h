#ifndef GRIDSTATICFACTORY_H
#define GRIDSTATICFACTORY_H

#include "iostream"
#include "gridstatic.h"
#include "gridmorpion.h"
#include "gridpuissance4.h"
#include <memory>

class GridStaticFactory
{
public:
    enum GridGame {
        Morpion,
        Puissance4};

   static std::unique_ptr<GridStatic>createGrid(GridGame gridGame);
};

#endif // GRIDSTATICFACTORY_H
