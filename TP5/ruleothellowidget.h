#ifndef RULEOTHELLOWIDGET_H
#define RULEOTHELLOWIDGET_H
#include "rule.h"
#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QSignalMapper>

class RuleOthelloWidget : public QWidget, public Rule
{
    Q_OBJECT
public:
    RuleOthelloWidget(QWidget *parent = nullptr);
    void PlayerTurn(const Player & player) override;
    bool VictoryCondition(const Player & player) override;
    bool NoWinnerEndCondition() override;
    void GameFlow() override;
    bool EndOfTheGame();
    void EndGameManager();
    void Setup();
    void SetupPlayer1();
    void SetupPlayer2();
    bool PlayerCanPutValidTokenInGrid(const Player & player);
    bool PlayerCanPutValidTokenOnCase(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnLineOrColumnOrDiagonal(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnLine(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnLineToLeft(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnLineToRight(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnColumn(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnColumnUp(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnColumnDown(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonal(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonalULDR(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonalURDL(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonalToUpLeft(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonalToUpRight(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonalToDownLeft(const Player & player, const Case & caseToCheck);
    bool ValidMovePossibleOnDiagonalToDownRight(const Player & player, const Case & caseToCheck);

    void FlipTokens(const Player & player, const Case & caseToCheck);
    void FlipTokensToTheLeft(const Player & player, const Case & caseToCheck);
    void FlipTokensToTheRight(const Player & player, const Case & caseToCheck);
    void FlipTokensUp(const Player & player, const Case & caseToCheck);
    void FlipTokensDown(const Player & player, const Case & caseToCheck);
    void FlipTokensToUpLeft(const Player & player, const Case & caseToCheck);
    void FlipTokensToUpRight(const Player & player, const Case & caseToCheck);
    void FlipTokensToDownLeft(const Player & player, const Case & caseToCheck);
    void FlipTokensToDownRight(const Player & player, const Case & caseToCheck);
    Player& GetPlayer1();
    Player& GetPlayer2();

    void IterateGame();
    void CheckCoordinates(int lineNumber, int columnNumber);
    void NextPlayer();

    void LoadGame() override;
    void SaveGame() const override;
    void RemoveSaveFile() const;

    void Read(const QJsonObject &json) override;
    void Write(QJsonObject &json) const override;

    std::unique_ptr<GridDynamic> grid = GridDynamicFactory::createGrid(GridDynamicFactory::Othello);

    std::unique_ptr<GridDynamic>& GetGrid();

private slots:
    void PlayerPutsTokenInGrid();

private:
    QLabel* m_lblTitle;
    QLabel* m_lblInfos;
    QLabel* m_lblCurrentPlayer;
    QPushButton* m_btnReturn;
    std::vector<QPushButton*> m_boardButtons;

    Player m_currentPlayer;

    QString m_saveFile;
    QString m_gameType = "othello";

    void InitBoard();
    void RefreshBoard();
    void Rematch();

};

#endif // RULEOTHELLOWIDGET_H
