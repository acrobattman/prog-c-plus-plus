#include "rulemorpion.h"
#include <string.h>

RuleMorpion::RuleMorpion()
{

}

void RuleMorpion::PlayerTurn(const Player & player){
    std::cout<<"Your turn player "<<player.valueOfToken<<std::endl;
    this->PlayerPutsToken(player);
}

void RuleMorpion::PlayerPutsToken(const Player &player){
    int lineNumber=-1;
    int columnNumber=-1;
    while(!(this->grid->CaseCoordinatesAreValid(lineNumber,columnNumber))||(!(this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber,columnNumber))))){
        this->grid->DisplayGrid();
        std::cout<<"Please select a line between 0 and 2"<<std::endl;
        std::cin>>lineNumber;
        std::cout<<"Please select a column between 0 and 2"<<std::endl;
        std::cin>>columnNumber;
        if(!(this->grid->CaseCoordinatesAreValid(lineNumber,columnNumber))){
            std::cout<<"Please select valid coordinates"<<std::endl;
        }
        else {
        if(!(this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber,columnNumber)))){
            std::cout<<"Please select an empty case"<<std::endl;
        }
        }
    }
    this->grid->PlayAToken(player, this->grid->GetCase(lineNumber,columnNumber));
}



bool RuleMorpion::VictoryCondition(const Player& player){
    return(this->LineVictory(player)||this->ColumnVictory(player)||this->DiagonalVictory(player));
}

bool RuleMorpion::LineVictory(const Player &player){
    for(int lineNumber=0;lineNumber<this->grid->GetNumberOfLine();lineNumber++){
        if(this->grid->LineCompletedByPlayer(lineNumber,player)){
            return true;
        }
    }
    return false;
}

bool RuleMorpion::ColumnVictory(const Player &player){
    for(int columnNumber=0;columnNumber<this->grid->GetNumberOfColumn();columnNumber++){
        if(this->grid->ColumnCompletedByPlayer(columnNumber,player)){
            return true;
        }
    }
    return false;
}

bool RuleMorpion::DiagonalULDRVictory(const Player &player){
    return this->grid->DiagonalULDRCompletedByPlayer(1,1,player);
}

bool RuleMorpion::DiagonalURDLVictory(const Player &player){
    return this->grid->DiagonalURDLCompletedByPlayer(1,1,player);
}

bool RuleMorpion::DiagonalVictory(const Player &player){
    return (this->DiagonalULDRVictory(player)||this->DiagonalURDLVictory(player));

}

bool RuleMorpion::NoWinnerEndCondition(){
    return this->grid->GridIsFull();
}

void RuleMorpion::GameFlow(){
    QFile loadFile(QStringLiteral("saveMorpion.json"));

    if (loadFile.open(QIODevice::ReadOnly)) {
        std::cout<<"Would you like to load your previous game? (1 for yes)"<<std::endl;
        int saveChoice;
        std::cin>>saveChoice;
        switch(saveChoice){
            case 1:
                LoadGame();
                break;
            default:
                break;
        }
    }

    std::cout<<"The game of Morpion has begun"<<std::endl;
    while(!NoWinnerEndCondition()){
        for(Player player : this->playerList){
            this->PlayerTurn(player);
            if(this->VictoryCondition(player)){
                this->grid->DisplayGrid();
                std::cout<<"Player "<<player.valueOfToken<<" has won !"<<std::endl;
                return;
            }
            if(NoWinnerEndCondition()){
                this->grid->DisplayGrid();
                if (this->Rematch())
                {
                    this->grid->ClearGrid();
                    std::cout<<"The game of Morpion has begun again"<<std::endl;
                }
            }
        }
        SaveGame();
    }
    std::cout<<"Game Over : the grid is full"<<std::endl;
    return;
};

void RuleMorpion::LoadGame(){
    QFile loadFile(QStringLiteral("saveMorpion.json"));

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    Read(loadDoc.object());
}

void RuleMorpion::SaveGame() const{
    QFile saveFile(QStringLiteral("saveMorpion.json"));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonObject gameObject;
    Write(gameObject);
    QJsonDocument saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());
}

void RuleMorpion::Read(const QJsonObject &json){

    grid->ClearGrid();
    QJsonArray caseArray = json["cases"].toArray();
    for (int caseIndex = 0; caseIndex < caseArray.size(); ++caseIndex) {
        QJsonObject caseObject = caseArray[caseIndex].toObject();
        Case caseToRead;
        caseToRead.Read(caseObject);
        grid->SetCaseInGrid(caseToRead);
    }
}

void RuleMorpion::Write(QJsonObject &json) const{

    QJsonArray caseArray;
    for(std::vector<Case> listCases : grid->GetGrid()) {
        for(Case caseToWrite : listCases) {
                QJsonObject caseObject;
                caseToWrite.Write(caseObject);
                caseArray.append(caseObject);
            }
    }
    json["cases"] = caseArray;
}
