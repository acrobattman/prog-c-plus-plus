TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle

QT += core gui widgets

SOURCES += \
        case.cpp \
        casevector.cpp \
        column.cpp \
        diagonaluldr.cpp \
        diagonalurdl.cpp \
        gamechoicewidget.cpp \
        grid.cpp \
        griddame.cpp \
        griddynamic.cpp \
        griddynamicfactory.cpp \
        gridmorpion.cpp \
        gridothello.cpp \
        gridpuissance4.cpp \
        gridstatic.cpp \
        gridstaticfactory.cpp \
        line.cpp \
        main.cpp \
        mainmenuwidget.cpp \
        mainwindow.cpp \
        player.cpp \
        rule.cpp \
        ruledames.cpp \
        ruledameswidget.cpp \
        rulemorpion.cpp \
        ruleothello.cpp \
        rulemorpionwidget.cpp \
        ruleothellowidget.cpp \
        rulepuissance4.cpp \
        rulepuissance4widget.cpp

HEADERS += \
    case.h \
    casevector.h \
    column.h \
    diagonaluldr.h \
    diagonalurdl.h \
    enumPiece.h \
    gamechoicewidget.h \
    grid.h \
    griddame.h \
    griddynamic.h \
    griddynamicfactory.h \
    gridmorpion.h \
    gridothello.h \
    gridpuissance4.h \
    gridstatic.h \
    gridstaticfactory.h \
    line.h \
    mainmenuwidget.h \
    mainwindow.h \
    player.h \
    rule.h \
    ruledames.h \
    ruledameswidget.h \
    rulemorpion.h \
    ruleothello.h \
    rulemorpionwidget.h \
    ruleothellowidget.h \
    rulepuissance4.h \
    rulepuissance4widget.h

FORMS += \
    mainwindow.ui
