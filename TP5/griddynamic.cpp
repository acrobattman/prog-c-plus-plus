#include "griddynamic.h"

GridDynamic::GridDynamic(int numberOfLine, int numberOfColumn):Grid(numberOfLine,numberOfColumn)
{

}


void GridDynamic::PlayAToken(const Player &player, Case &caseToPlay){
    caseToPlay.playerOnCase=player.valueOfToken;
    caseToPlay.pieceType=EnumPiece::TOKEN;
}

void GridDynamic::ClearGrid(){
for(int lineNumber=0;lineNumber<this->numberOfLine;++lineNumber){
    for(int columnNumber=0;columnNumber<this->numberOfColumn;columnNumber++){
        this->GetCase(lineNumber, columnNumber).ResetDynamicCase();
        }
    }
}


void GridDynamic::UpperLineDisplay(){
    std::cout<<"  ";
    for(int caseNumber=0;caseNumber<(int)this->numberOfColumn;++caseNumber){
        std::cout<<"   "<<caseNumber<<"  ";
    }
    std::cout<<std::endl;
    std::cout<<"  ";
    for(int caseNumber=0;caseNumber<(int)this->numberOfColumn;++caseNumber){
        std::cout<<" ____ ";
    }
    std::cout<<std::endl;
}

void GridDynamic::EmptyLineDisplay(){
    std::cout<<"  ";
    for(int caseNumber=0;caseNumber<(int)this->numberOfColumn;++caseNumber){
        std::cout<<"|    |";
    }
    std::cout<<std::endl;
};

void GridDynamic::EmptyLineSeparationDisplay(){
    std::cout<<"  ";
    for(int caseNumber=0;caseNumber<(int)this->numberOfColumn;++caseNumber){
            std::cout<<"|____|";
        }
        std::cout<<std::endl;
    };

void GridDynamic::DisplayGrid(){
    this->UpperLineDisplay();
    for(int lineNumber=0;lineNumber<this->numberOfLine;++lineNumber){
    this->EmptyLineDisplay();
        std::cout<<lineNumber<<" ";
        for(int columnNumber=0;columnNumber<this->numberOfColumn;columnNumber++){
            std::cout<<"| ";
            this->GetCase(lineNumber, columnNumber).DisplayCase();
            std::cout<<" |";
        }
        std::cout<<std::endl;
        this->EmptyLineSeparationDisplay();
    }
}
