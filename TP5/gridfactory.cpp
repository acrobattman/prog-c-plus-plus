#include "gridfactory.h"
#include <memory>

std::unique_ptr<Grid> GridFactory::createGrid(GridGame gridGame){
    switch (gridGame) {
        case Morpion:
            return std::make_unique<GridMorpion>();
        case Puissance4:
            return std::make_unique<GridPuissance4>();
        case Dame:
            return std::make_unique<GridDame>();
    }
    throw "invalid game type !";
}
