#ifndef ENUMPIECE_H
#define ENUMPIECE_H
#include <QMetaType>

enum class EnumPiece : char {
    EMPTY= 'X',
    EMPTYSPACEDISPLAY= '\0',
    TOKEN = 'X',
    PAWN= 'p',
    QUEEN = 'Q',
};
Q_DECLARE_METATYPE(EnumPiece);
#endif // ENUMPIECE_H
