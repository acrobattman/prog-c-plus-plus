#ifndef GRIDFACTORY_H
#define GRIDFACTORY_H

#include "iostream"
#include "grid.h"
#include "griddynamic.h"
#include "gridstatic.h"
#include "gridmorpion.h"
#include "gridpuissance4.h"
#include "griddame.h"
#include <memory>

class GridFactory
{
public:
    enum GridGame {
        Morpion,
        Puissance4,
        Dame
    };

    static std::unique_ptr<Grid>createGrid(GridGame gridGame);
};

#endif // GRIDFACTORY_H
