#ifndef GAMECHOICEWIDGET_H
#define GAMECHOICEWIDGET_H
#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QSignalMapper>

class GameChoiceWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GameChoiceWidget(QWidget *parentWidget = nullptr);

signals:

public slots:

private:
    QPushButton* m_btnReturn;
    QPushButton* m_btnMorpion;
    QPushButton* m_btnPuissance4;
    QPushButton* m_btnDame;
    QPushButton* m_btnOthello;
    QLabel* m_lblTitle;
};

#endif // GAMECHOICEWIDGET_H
