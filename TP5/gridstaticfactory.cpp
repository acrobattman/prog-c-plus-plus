#include "gridstaticfactory.h"

std::unique_ptr<GridStatic> GridStaticFactory::createGrid(GridGame gridGame){
    switch (gridGame) {
        case Morpion:
            return std::make_unique<GridMorpion>();
        case Puissance4:
            return std::make_unique<GridPuissance4>();
    }
    throw "invalid game type !";
}
