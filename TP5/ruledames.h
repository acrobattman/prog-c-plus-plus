#ifndef RULEDAMES_H
#define RULEDAMES_H
#include "rule.h"
#include "griddame.h"

class RuleDames:public Rule
{
public:
    RuleDames();
    void PlayerTurn(const Player & player);
    bool VictoryCondition(const Player & player);
    bool NoWinnerEndCondition();
    void GameFlow();

    void SetupGame();
    void SetupRowBeginBlack(int lineNumber, int valueOfPlayer);
    void SetupRowBeginWhite(int lineNumber, int valueOfPlayer);
    void SetupPlayer1();
    void SetupPlayer2();

    bool ValidCaseChosenByPlayer(int lineNumber, int columnNumber, const Player &player);
    bool CaseCoordinatesAreValid(int lineNumber, int columnNumber);
    bool LineCoordinateIsValid(int lineNumber);
    bool ColumnCoordinateIsValid(int columnNumber);
    bool CaseBelongsToPlayer(int lineNumber, int columnNumber, const Player & player);
    bool PieceOnCaseCanMoveOrTake(int lineNumber, int columnNumber,const Player & player);

    void PlayerChoosesACase(int & lineNumber, int & columnNumber,const Player & player);

    void PawnBecomesQueen(int lineNumber, int columnNuber);

    void TransformPawnArrivedOnEndLine();

    //Pawn Controllers
    void PawnMovement(int lineNumber, int columnNumber,const Player & player);
    void PawnTakeEnemyPiece(int & lineNumber, int & columnNumber,const Player & player);
    void PawnTurn(int lineNumber, int columnNumber,const Player & player);

    //Pawn Movements Checkers

    bool PawnCanMove(int lineNumber, int columnNumber,const Player & player);
    bool PawnCanMoveUpLeft(int lineNumber, int columnNumber);
    bool PawnCanMoveUpRight(int lineNumber, int columnNumber);
    bool PawnCanMoveDownLeft(int lineNumber, int columnNumber);
    bool PawnCanMoveDownRight(int lineNumber, int columnNumber);
    bool PawnCanMoveLeft(int lineNumber, int columnNumber,const Player & player);
    bool PawnCanMoveRight(int lineNumber, int columnNumber,const Player & player);
    bool PawnCanMovePlayer1(int lineNumber, int columnNumber);
    bool PawnCanMovePlayer2(int lineNumber, int columnNumber);

    //Pawn Jump Checkers

    bool PawnCanTakeEnemyPiece(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber);
    bool PieceIsBeingTaken(int lineNumber, int columnNumber);
    void RemovePiecesTaken();

    //Pawn Movements

    void PawnMoveLeft(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveRight(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveUpLeft(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveUpRight(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveDownLeft(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveDownRight(int lineNumber, int columnNumber,const Player & player);

    //Pawn Jumps

    void PawnTakeEnemyUpLeft(int& lineNumber, int& columnNumber,const Player &player);
    void PawnTakeEnemyUpRight(int& lineNumber, int& columnNumber,const Player &player);
    void PawnTakeEnemyDownLeft(int& lineNumber, int& columnNumber,const Player &player);
    void PawnTakeEnemyDownRight(int& lineNumber, int& columnNumber,const Player &player);

    //Queen Movements Checkers

    bool QueenCanMove(int lineNumber, int columnNumber);
    bool QueenCanMoveUpLeft(int lineNumber, int columnNumber);
    bool QueenCanMoveUpRight(int lineNumber, int columnNumber);
    bool QueenCanMoveDownLeft(int lineNumber, int columnNumber);
    bool QueenCanMoveDownRight(int lineNumber, int columnNumber);

    bool QueenPathIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathUpLeftIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathUpRightIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathDownLeftIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathDownRightIsClear(Case& CaseStart, Case& CaseFinish);

    bool QueenPathGoesUp(Case& CaseStart, Case& CaseFinish);
    bool QueenPathGoesLeft(Case& CaseStart, Case& CaseFinish);
    bool QueenPathIsDiagonal(Case& CaseStart, Case& CaseFinish);

    bool SelectValidDestinationCaseCoordonates(int& lineCaseFinish,int& columnCaseFinish);

    //Queen Jump Controller

    bool QueenCanTakeEnemyPiece(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber);

    //Queen Jump


    void QueenTakesEnemyPieceUpLeft(int & lineNumber, int & columnNumber,const Player & player);
    void QueenTakesEnemyPieceUpRight(int & lineNumber, int & columnNumber,const Player & player);
    void QueenTakesEnemyPieceDownLeft(int & lineNumber, int & columnNumber,const Player & player);
    void QueenTakesEnemyPieceDownRight(int & lineNumber, int & columnNumber,const Player & player);

    //Queen Controller

    void QueenTakesEnemyPiece(int & lineNumber, int & columnNumber,const Player & player);
    void QueenMovement(int lineNumber, int columnNumber,const Player & player);
    void QueenTurn(int lineNumber, int columnNumber,const Player & player);

    void SetupTest();

    void LoadGame() override;
    void SaveGame() const override;

    void Read(const QJsonObject &json) override;
    void Write(QJsonObject &json) const override;

    std::unique_ptr<GridDynamic> grid = GridDynamicFactory::createGrid(GridDynamicFactory::Dame);

    std::unique_ptr<GridDynamic>& GetGrid();
};

#endif // RULEDAMES_H
