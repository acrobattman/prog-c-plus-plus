#ifndef CASE_H
#define CASE_H

#include "enumPiece.h"
#include <stdio.h>
#include <iostream>
#include <QJsonObject>
#include <QString>
#include <QVariant>

class Case
{
protected:
    int numLine;
    int numColumn;

public:

    int playerOnCase;
    EnumPiece pieceType;

    Case();

    Case(int _numLine, int _numColumn);

    Case(int _numLine, int _numColumn, int _playerOnCase,const EnumPiece& _pieceType);

    ~Case();

    bool CaseIsEmpty() const;

    int GetNumLine() const;

    int GetNumColumn()const;

    EnumPiece& GetPieceType();

    void ResetCase();

    void ResetDynamicCase();

    void DisplayCase();

    bool CaseBelongsToPlayerValue(int playerValue);

    bool CaseBelongsToOpponent(int playerValue);

    void Read(const QJsonObject &json);
    void Write(QJsonObject &json) const;

};

#endif // CASE_H

