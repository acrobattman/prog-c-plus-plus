#ifndef DIAGONALULDR_H
#define DIAGONALULDR_H
#include "casevector.h"

class DiagonalULDR : public CaseVector
{
public:
    DiagonalULDR(CaseVector& casevector);
};

#endif // DIAGONALULDR_H
