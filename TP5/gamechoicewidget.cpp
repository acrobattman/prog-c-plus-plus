#include "gamechoicewidget.h"

GameChoiceWidget::GameChoiceWidget(QWidget* parentWidget)
{
    this->setParent(parentWidget);
    this->setFixedSize(parentWidget->size());

    m_lblTitle = new QLabel("Board Games", this);
    m_lblTitle->setFont(QFont("Arial", 48));
    m_lblTitle->move(200, 20);
    m_lblTitle->setFixedSize(480,60);

    m_btnMorpion = new QPushButton("Tic Tac Toe", this);
    m_btnMorpion->setFont(QFont("Arial", 20));
    m_btnMorpion->move(50, 150);
    m_btnMorpion->setFixedSize(200, 75);
    QObject::connect(m_btnMorpion, SIGNAL(clicked()), this->parent(), SLOT(StartMorpion()));

    m_btnPuissance4 = new QPushButton("Connect 4", this);
    m_btnPuissance4->setFont(QFont("Arial", 20));
    m_btnPuissance4->move(450, 150);
    m_btnPuissance4->setFixedSize(200, 75);
    QObject::connect(m_btnPuissance4, SIGNAL(clicked()), this->parent(), SLOT(StartPuissance4()));

    m_btnDame = new QPushButton("Checkers", this);
    m_btnDame->setFont(QFont("Arial", 20));
    m_btnDame->move(50, 250);
    m_btnDame->setFixedSize(200, 75);
    QObject::connect(m_btnDame, SIGNAL(clicked()), this->parent(), SLOT(StartDames()));

    m_btnOthello = new QPushButton("Othello", this);
    m_btnOthello->setFont(QFont("Arial", 20));
    m_btnOthello->move(450, 250);
    m_btnOthello->setFixedSize(200, 75);
    QObject::connect(m_btnOthello, SIGNAL(clicked()), this->parent(), SLOT(StartOthello()));

    m_btnReturn = new QPushButton("Return to menu", this);
    m_btnReturn->setFont(QFont("Arial", 20));
    m_btnReturn->move(250, 350);
    m_btnReturn->setFixedSize(200, 75);
    QObject::connect(m_btnReturn, SIGNAL(clicked()), this->parent(), SLOT(ReturnMenu()));
}
