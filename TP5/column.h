#ifndef COLUMN_H
#define COLUMN_H
#include "casevector.h"

class Column : public CaseVector
    {
    public:
        Column(CaseVector& casevector);
};

#endif // COLUMN_H
