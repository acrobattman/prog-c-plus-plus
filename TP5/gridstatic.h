#ifndef GRIDSTATIC_H
#define GRIDSTATIC_H

#include "grid.h"
#include <memory>

class GridStatic : public Grid
{
public:
    GridStatic(int numberOfLine, int numberOfColumn);

    void PlayAToken(const Player &player,Case &caseToPlay);

    virtual bool CaseVectorCompletedByAPlayer(CaseVector& casevector, const Player &currentPlayer);

    virtual bool GridIsFull();

    virtual bool LineNumberIsValid(int lineNumber);
    virtual bool ColumnNumberIsValid(int columnNumber);
    virtual bool CaseCoordinatesAreValid(int lineNumber, int columnNumber);

    virtual Line CreateLine(int lineNumber);
    virtual bool LineCompletedByPlayer(int lineNumber, const Player &currentPlayer);
    virtual Column CreateColumn(int columnNumber);
    virtual bool ColumnCompletedByPlayer(int columnNumber, const Player &currentPlayer);
    virtual DiagonalULDR CreateDiagonalULDR(int lineNumber, int columnNumber);
    virtual bool DiagonalULDRCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer);
    virtual DiagonalURDL CreateDiagonalURDL(int lineNumber, int columnNumber);
    virtual bool DiagonalURDLCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer);
};

#endif // GRIDSTATIC_H
