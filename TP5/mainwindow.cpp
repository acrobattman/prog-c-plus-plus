#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setFixedSize(800,600);
    MainMenu = new MainMenuWidget(this);
    ActiveWidget = MainMenu;
    GameChoice = new GameChoiceWidget(this);
    GameChoice->hide();
    GameMorpion = new RuleMorpionWidget(this);
    GameMorpion->hide();
    GamePuissance4 = new RulePuissance4Widget(this);
    GamePuissance4->hide();
    GameDames = new RuleDamesWidget(this);
    GameDames->hide();
    GameOthello = new RuleOthelloWidget(this);
    GameOthello->hide();
}

void MainWindow::ReturnMenu()
{
    this->ActiveWidget = MainMenu;
    HideWidgets();
    MainMenu->show();
}

void MainWindow::GameSelection()
{
    this->ActiveWidget = GameChoice;
    HideWidgets();
    GameChoice->show();
}

void MainWindow::StartMorpion()
{
    GameMorpion = new RuleMorpionWidget(this);
    this->ActiveWidget = GameMorpion;
    HideWidgets();
    GameMorpion->show();
    GameMorpion->GameFlow();
}

void MainWindow::StartPuissance4()
{
    GamePuissance4 = new RulePuissance4Widget(this);
    this->ActiveWidget = GamePuissance4;
    HideWidgets();
    GamePuissance4->show();
    GamePuissance4->GameFlow();
}

void MainWindow::StartDames()
{
    GameDames = new RuleDamesWidget(this);
    this->ActiveWidget = GameDames;
    HideWidgets();
    GameDames->show();
    GameDames->GameFlow();
}

void MainWindow::StartOthello()
{
    this->ActiveWidget = GameOthello;
    HideWidgets();
    GameOthello->show();
    GameOthello->GameFlow();
}

void MainWindow::HideWidgets()
{
    MainMenu->hide();
    GameChoice->hide();
    GameMorpion->hide();
    GamePuissance4->hide();
    GameDames->hide();
    GameOthello->hide();
}
