# Choix du dépot (Alexandre Colombo & Maxime Eckstein)

Nous avons choisi ce dépôt, car il implémente déjà certains concepts SOLID, par exemple l'open close pour la création des classes grille.

De plus la const correctness est également mise en place sur l'ensemble de ce projet.

Création de classe ligne, colonne et diagonal qui permet de gérer plus facilement les conditions de victoire.

Un des plus grands travaux sera de respecter la loi de Déméter et le refactoring de la classe jeu qui n'ai pas très bien implémentation.

Une amélioration pourrait être d'implémentation une classe abstraite règle de jeu qui aura des enfants règle jeu dame, règle de jeu morpion et règle de jeu puissance4 ce qui permettra de respecter le principe open close et au lieu de rouvrir une nouvelle classe de jeu pour la modifier on crée une nouvelle classe de jeu dépendant de la classe abstraite. 

Pour les grilles et le jeu on respecte la ségrégation des interfaces, ainsi que le principe d'inversion des dépendances une classe de jeu ne dépend pas d'une instanciation de physique de grille. Le code répond au principe de responsabilité unique, les classes de grille sont chargées un uniquement de la gestion des cases et les classes règles sont chargé du déroulement du jeu.

Il faudra faire attention à respecter le tell dont ask en créant des méthodes au lieu d'appeler les propriétés des objets.