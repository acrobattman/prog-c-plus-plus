#include "ruledameswidget.h"

RuleDamesWidget::RuleDamesWidget(QWidget *parentWidget)
{
    this->setParent(parentWidget);
    this->setFixedSize(parentWidget->size());

    m_lblTitle = new QLabel("Checkers", this);
    m_lblTitle->setFont(QFont("Arial", 36));
    m_lblTitle->move(200, 20);
    m_lblTitle->setFixedSize(480,60);

    m_lblCurrentPlayer = new QLabel("current player", this);
    m_lblCurrentPlayer->setFont(QFont("Arial", 15));
    m_lblCurrentPlayer->move(500,150);
    m_lblCurrentPlayer->setFixedSize(200,50);
    m_lblCurrentPlayer->setWordWrap(true);

    m_lblInfos = new QLabel("Info panel", this);
    m_lblInfos->setFont(QFont("Arial", 15));
    m_lblInfos->move(500, 200);
    m_lblInfos->setFixedSize(200,50);
    m_lblInfos->setWordWrap(true);

    m_btnReturn = new QPushButton("Return to selection", this);
    m_btnReturn->setFont(QFont("Arial", 10));
    m_btnReturn->move(250, 550);
    m_btnReturn->setFixedSize(150, 25);
    QObject::connect(m_btnReturn, SIGNAL(clicked()), this->parent(), SLOT(GameSelection()));

    m_currentPlayer = Player();

    m_saveFile = "saveDames.json";

    m_selectedPieceLine = -1;
    m_selectedPieceColumn = -1;

    InitBoard();
}

std::unique_ptr<GridDynamic>& RuleDamesWidget::GetGrid(){
    return this->grid;
}

void RuleDamesWidget::SetupGame(){
    this->grid->ClearGrid();
    this->SetupPlayer1();
    this->SetupPlayer2();
};

void RuleDamesWidget::SetupTest(){
    this->grid->ClearGrid();
    this->grid->GetCase(5,5).playerOnCase=1;
    this->grid->GetCase(5,5).pieceType=EnumPiece::QUEEN;

    this->grid->GetCase(4,4).playerOnCase=2;
    this->grid->GetCase(4,4).pieceType=EnumPiece::QUEEN;
}

void RuleDamesWidget::SetupPlayer1(){
    this->SetupRowBeginBlack(9,1);
    this->SetupRowBeginBlack(7,1);
    this->SetupRowBeginWhite(8,1);
    this->SetupRowBeginWhite(6,1);
}

void RuleDamesWidget::SetupPlayer2(){
    this->SetupRowBeginBlack(1,2);
    this->SetupRowBeginBlack(3,2);
    this->SetupRowBeginWhite(0,2);
    this->SetupRowBeginWhite(2,2);

}




void RuleDamesWidget::SetupRowBeginBlack(int lineNumber, int valueOfPlayer){
    for(int columnNumber=0; columnNumber<this->grid->GetNumberOfColumn();columnNumber+=2){
            this->grid->GetCase(lineNumber,columnNumber).pieceType=EnumPiece::PAWN;
            this->grid->GetCase(lineNumber,columnNumber).playerOnCase=valueOfPlayer;
    }
};

void RuleDamesWidget::SetupRowBeginWhite(int lineNumber,int valueOfPlayer){
    for(int columnNumber=1; columnNumber<this->grid->GetNumberOfColumn();columnNumber+=2){
            this->grid->GetCase(lineNumber,columnNumber).pieceType=EnumPiece::PAWN;
            this->grid->GetCase(lineNumber,columnNumber).playerOnCase=valueOfPlayer;
    }
};

void RuleDamesWidget::PawnBecomesQueen(int lineNumber, int columnNuber){
    this->grid->GetCase(lineNumber,columnNuber).pieceType=EnumPiece::QUEEN;
}

void RuleDamesWidget::TransformPawnArrivedOnEndLine(){
    for(int columnNumber=0; columnNumber<this->grid->GetNumberOfColumn();++columnNumber){
        if((this->grid->GetCase(0,columnNumber).playerOnCase==1)&&(this->grid->GetCase(0,columnNumber).pieceType==EnumPiece::PAWN)){
            this->PawnBecomesQueen(0,columnNumber);
        }
        if((this->grid->GetCase(this->grid->GetNumberOfLine()-1,columnNumber).playerOnCase==2)&&(this->grid->GetCase(this->grid->GetNumberOfLine()-1,columnNumber).pieceType==EnumPiece::PAWN)){
            this->PawnBecomesQueen(this->grid->GetNumberOfLine()-1,columnNumber);
        }
    }
}


bool RuleDamesWidget::ValidCaseChosenByPlayer(int lineNumber, int columnNumber, const Player & player){
    return(this->CaseCoordinatesAreValid(lineNumber,columnNumber)
           &&this->CaseBelongsToPlayer(lineNumber,columnNumber, player)
           &&this->PieceOnCaseCanMoveOrTake(lineNumber,columnNumber, player));
};

bool RuleDamesWidget::LineCoordinateIsValid(int lineNumber){
    return ((lineNumber>=0) && (lineNumber<this->grid->GetNumberOfLine()));
}

bool RuleDamesWidget::ColumnCoordinateIsValid(int columnNumber){
    return ((columnNumber>=0) && (columnNumber<this->grid->GetNumberOfColumn()));
}

bool RuleDamesWidget::CaseCoordinatesAreValid(int lineNumber, int columnNumber){
    return((this->LineCoordinateIsValid(lineNumber))&&(this->ColumnCoordinateIsValid(columnNumber)));
};

bool RuleDamesWidget::CaseBelongsToPlayer(int lineNumber, int columnNumber, const Player & player){
    if(!(this->grid->GetCase(lineNumber,columnNumber).playerOnCase==player.valueOfToken)){
        m_lblInfos->setText("The case doesn't belong to you");
    }
    return (this->grid->GetCase(lineNumber,columnNumber).playerOnCase==player.valueOfToken);
};

bool RuleDamesWidget::PieceOnCaseCanMoveOrTake(int lineNumber, int columnNumber,const Player & player){
    return ((this->PawnCanTakeEnemyPiece(lineNumber,columnNumber))||(this->PawnCanMove(lineNumber,columnNumber,player)));
};

bool RuleDamesWidget::PieceIsBeingTaken(int lineNumber, int columnNumber){
    return this->grid->GetCase(lineNumber,columnNumber).playerOnCase==-1;
};

bool RuleDamesWidget::PawnCanMove(int lineNumber, int columnNumber, const Player & player){
    if(player.valueOfToken==1){
        return this->PawnCanMovePlayer1(lineNumber,columnNumber);
    }
    else if (player.valueOfToken==2){
        return this->PawnCanMovePlayer2(lineNumber,columnNumber);
    }
    return false;
};

bool RuleDamesWidget::PawnCanTakeEnemyPiece(int lineNumber, int columnNumber){
    return (  (this->PawnCanTakeEnemyPieceUpLeft(lineNumber,columnNumber))
            ||(this->PawnCanTakeEnemyPieceUpRight(lineNumber,columnNumber))
            ||(this->PawnCanTakeEnemyPieceDownLeft(lineNumber,columnNumber))
            ||(this->PawnCanTakeEnemyPieceDownRight(lineNumber,columnNumber))
           );
};

bool RuleDamesWidget::PawnCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber-2,columnNumber-2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber-1,columnNumber-1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber-1,columnNumber-1))
           ||(this->grid->GetCase(lineNumber-1,columnNumber-1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber-2,columnNumber-2).CaseIsEmpty());
};

bool RuleDamesWidget::PawnCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber-2,columnNumber+2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber-1,columnNumber+1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber-1,columnNumber+1))
           ||(this->grid->GetCase(lineNumber-1,columnNumber+1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber-2,columnNumber+2).CaseIsEmpty());
};
bool RuleDamesWidget::PawnCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber+2,columnNumber-2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber+1,columnNumber-1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber+1,columnNumber-1))
           ||(this->grid->GetCase(lineNumber+1,columnNumber-1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber+2,columnNumber-2).CaseIsEmpty());
};

bool RuleDamesWidget::PawnCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber+2,columnNumber+2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber+1,columnNumber+1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber+1,columnNumber+1))
           ||(this->grid->GetCase(lineNumber+1,columnNumber+1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber+2,columnNumber+2).CaseIsEmpty());
};


bool RuleDamesWidget::PawnCanMovePlayer1(int lineNumber, int columnNumber){
    if(!(this->PawnCanMoveUpLeft(lineNumber,columnNumber)||this->PawnCanMoveUpRight(lineNumber,columnNumber))){
        m_lblInfos->setText("You cannot move that piece");
    }
    return (this->PawnCanMoveUpLeft(lineNumber,columnNumber)||this->PawnCanMoveUpRight(lineNumber,columnNumber));
};

bool RuleDamesWidget::PawnCanMovePlayer2(int lineNumber, int columnNumber){
    if(!(this->PawnCanMoveDownLeft(lineNumber,columnNumber)||this->PawnCanMoveDownRight(lineNumber,columnNumber))){
        m_lblInfos->setText("You cannot move that piece");
    }
    return (this->PawnCanMoveDownLeft(lineNumber,columnNumber)||this->PawnCanMoveDownRight(lineNumber,columnNumber));
};

bool RuleDamesWidget::PawnCanMoveUpLeft(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber-1,columnNumber-1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber-1,columnNumber-1)));
    }
    return false;
};

bool RuleDamesWidget::PawnCanMoveUpRight(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber-1,columnNumber+1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber-1,columnNumber+1)));
    }
        return false;
}

bool RuleDamesWidget::PawnCanMoveDownLeft(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber+1,columnNumber-1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber+1,columnNumber-1)));
    }
    return false;
};

bool RuleDamesWidget::PawnCanMoveDownRight(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber+1,columnNumber+1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber+1,columnNumber+1)));
    }
        return false;
}

bool RuleDamesWidget::PawnCanMoveLeft(int lineNumber, int columnNumber, const Player &player){
    if(player.valueOfToken==1){
        return this->PawnCanMoveUpLeft(lineNumber,columnNumber);
    }
    else if(player.valueOfToken==2){
        return this->PawnCanMoveDownLeft(lineNumber,columnNumber);
    }
    else{
        return false;
    }
}

bool RuleDamesWidget::PawnCanMoveRight(int lineNumber, int columnNumber, const Player &player){
    if(player.valueOfToken==1){
        return this->PawnCanMoveUpRight(lineNumber,columnNumber);
    }
    else if(player.valueOfToken==2){
        return this->PawnCanMoveDownRight(lineNumber,columnNumber);
    }
    else{
        return false;
    }
}


void RuleDamesWidget::PawnMoveLeft(int lineNumber, int columnNumber,const Player & player){
    if(player.valueOfToken==1){
        this->PawnMoveUpLeft(lineNumber,columnNumber,player);
    }
    if(player.valueOfToken==2){
        this->PawnMoveDownLeft(lineNumber,columnNumber,player);
    }
};
void RuleDamesWidget::PawnMoveRight(int lineNumber, int columnNumber,const Player & player){
    if(player.valueOfToken==1){
        this->PawnMoveUpRight(lineNumber,columnNumber,player);
    }
    if(player.valueOfToken==2){
        this->PawnMoveDownRight(lineNumber,columnNumber,player);
    }
};


void RuleDamesWidget::PawnMoveDownLeft(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+1,columnNumber-1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+1,columnNumber-1).playerOnCase=player.valueOfToken;
};

void RuleDamesWidget::PawnMoveDownRight(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+1,columnNumber+1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+1,columnNumber+1).playerOnCase=player.valueOfToken;
};

void RuleDamesWidget::PawnMoveUpLeft(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-1,columnNumber-1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-1,columnNumber-1).playerOnCase=player.valueOfToken;
};

void RuleDamesWidget::PawnMoveUpRight(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-1,columnNumber+1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-1,columnNumber+1).playerOnCase=player.valueOfToken;
};



void RuleDamesWidget::PawnTakeEnemyUpLeft(int & lineNumber, int  & columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceUpLeft(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-2,columnNumber-2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-2,columnNumber-2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber-1,columnNumber-1).playerOnCase=-1;
    this->grid->GetCase(lineNumber-1,columnNumber-1).pieceType = EnumPiece::EMPTY;
    lineNumber=lineNumber-2;
    columnNumber=columnNumber-2;
    PawnCanTakeEnemyPieceAgain(lineNumber,columnNumber);
};

void RuleDamesWidget::PawnTakeEnemyUpRight(int & lineNumber, int & columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceUpRight(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-2,columnNumber+2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-2,columnNumber+2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber-1,columnNumber+1).playerOnCase=-1;
    this->grid->GetCase(lineNumber-1,columnNumber+1).pieceType = EnumPiece::EMPTY;
    lineNumber=lineNumber-2;
    columnNumber=columnNumber+2;
    PawnCanTakeEnemyPieceAgain(lineNumber,columnNumber);
};

void RuleDamesWidget::PawnTakeEnemyDownLeft(int & lineNumber, int & columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceDownLeft(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+2,columnNumber-2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+2,columnNumber-2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber+1,columnNumber-1).playerOnCase=-1;
    this->grid->GetCase(lineNumber+1,columnNumber-1).pieceType = EnumPiece::EMPTY;
    lineNumber=lineNumber+2;
    columnNumber=columnNumber-2;
    PawnCanTakeEnemyPieceAgain(lineNumber,columnNumber);
};
void RuleDamesWidget::PawnTakeEnemyDownRight(int &lineNumber, int& columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceDownRight(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+2,columnNumber+2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+2,columnNumber+2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber+1,columnNumber+1).playerOnCase=-1;
    this->grid->GetCase(lineNumber+1,columnNumber+1).pieceType = EnumPiece::EMPTY;
    lineNumber=lineNumber+2;
    columnNumber=columnNumber+2;
    PawnCanTakeEnemyPieceAgain(lineNumber,columnNumber);
};


void RuleDamesWidget::PlayerChoosesACase(){
    int lineNumber = sender()->property(lineIndex).toInt();
    int columnNumber = sender()->property(columnIndex).toInt();

    CheckTypeOfPiece(lineNumber, columnNumber);
}

void RuleDamesWidget::CheckTypeOfPiece(int lineNumber, int columnNumber)
{
    if(!this->LineCoordinateIsValid(lineNumber)){
        m_lblInfos->setText("Line is not valid");
    }
    if(!this->ColumnCoordinateIsValid(columnNumber)){
        m_lblInfos->setText("Column is not valid");
    }

    if(this->grid->GetCase(lineNumber,columnNumber).pieceType==EnumPiece::PAWN
            && CaseBelongsToPlayer(lineNumber, columnNumber, m_currentPlayer)
            && (PawnCanMove(lineNumber,columnNumber,m_currentPlayer)
                || PawnCanTakeEnemyPiece(lineNumber,columnNumber))){

        m_selectedPieceLine = lineNumber;
        m_selectedPieceColumn = columnNumber;
        this->PawnTurn(lineNumber,columnNumber);
    }
    else if(this->grid->GetCase(lineNumber,columnNumber).pieceType==EnumPiece::QUEEN
            && CaseBelongsToPlayer(lineNumber, columnNumber, m_currentPlayer)
            && (QueenCanMove(lineNumber,columnNumber)
                 )){ //|| QueenCanTakeEnemyPiece(lineNumber, columnNumber)

        m_selectedPieceLine = lineNumber;
        m_selectedPieceColumn = columnNumber;
        this->QueenTurn(lineNumber,columnNumber);
    }
    else{
       m_lblInfos->setText("The case doesn't belong to you");
    }
}

void RuleDamesWidget::IterateGame(){
    RefreshBoard();
    m_lblInfos->setText("Please select a piece");

    if(this->VictoryCondition(m_currentPlayer)){
        m_lblInfos->setText("Player "+QString::number(m_currentPlayer.valueOfToken)+" has won !");
        //RemoveSaveFile();
        Rematch();
        return;
    }
    if(NoWinnerEndCondition()){
        m_lblInfos->setText("Game Over no contest");
        //RemoveSaveFile();
        Rematch();
        return;
    }
    else{
        RemovePiecesTaken();
        TransformPawnArrivedOnEndLine();
        SetEventPlayerChooseACase();
        NextPlayer();
        RefreshBoard();
        SaveGame();
    }
}

void RuleDamesWidget::PawnMovement(){
    int lineNumberToPlay = m_selectedPieceLine ;
    int columnNumberToPlay = m_selectedPieceColumn;
    Player& player = m_currentPlayer;

    int lineCaseFinish = sender()->property(lineIndex).toInt();
    int columnCaseFinish = sender()->property(columnIndex).toInt();

    //il faut calculer la direction

    int directionToPlay= ComputePawnDirectionToPlay(lineCaseFinish,columnCaseFinish);


    if(directionToPlay==1&&this->PawnCanMoveRight(lineNumberToPlay,columnNumberToPlay,player)){
        this->PawnMoveRight(lineNumberToPlay,columnNumberToPlay,player);
        IterateGame();
    }
    else if(directionToPlay==0&&this->PawnCanMoveLeft(lineNumberToPlay,columnNumberToPlay,player)){
        this->PawnMoveLeft(lineNumberToPlay,columnNumberToPlay,player);
        IterateGame();
    }
    else{
        m_lblInfos->setText("Select a valid movement please");
    }
}

int RuleDamesWidget::ComputePawnDirectionToPlay(int lineCaseFinish, int columnCaseFinish){
    // 0 for left and 1 for right
    int directionToPlay = -1;

    if(m_currentPlayer.name == playerList[0].name){
        directionToPlay = ComputePawnDirectionToPlayPlayer1(lineCaseFinish,columnCaseFinish);
    }
    else{
        directionToPlay = ComputePawnDirectionToPlayPlayer2(lineCaseFinish,columnCaseFinish);
    }

    return directionToPlay;
}

int RuleDamesWidget::ComputePawnDirectionToPlayPlayer1(int lineCaseFinish, int columnCaseFinish){
    // 0 for left and 1 for right
    int directionToPlay = -1;

    if(lineCaseFinish == m_selectedPieceLine-1 && columnCaseFinish == m_selectedPieceColumn-1){
        directionToPlay = 0;
    }
    else if(lineCaseFinish == m_selectedPieceLine-1 && columnCaseFinish == m_selectedPieceColumn+1){
        directionToPlay = 1;
    }

    return directionToPlay;
}

int RuleDamesWidget::ComputePawnDirectionToPlayPlayer2(int lineCaseFinish, int columnCaseFinish){
    // 0 for left and 1 for right
    int directionToPlay = -1;

    if(lineCaseFinish == m_selectedPieceLine+1 && columnCaseFinish == m_selectedPieceColumn-1){
        directionToPlay = 0;
    }
    else if(lineCaseFinish == m_selectedPieceLine+1 && columnCaseFinish == m_selectedPieceColumn+1){
        directionToPlay = 1;
    }

    return directionToPlay;
}

int RuleDamesWidget::ComputeDirectionToTakePiece(int lineCaseFinish, int columnCaseFinish){
    // 1 for up left ; 2 for up right ; 3 for down left and 4 for down right
    int directionToPlay = -1;

    if(lineCaseFinish <= m_selectedPieceLine-1 && columnCaseFinish <= m_selectedPieceColumn-1){
        directionToPlay = 1;
    }
    else if(lineCaseFinish <= m_selectedPieceLine-1 && columnCaseFinish >= m_selectedPieceColumn+1){
        directionToPlay = 2;
    }
    else if(lineCaseFinish >= m_selectedPieceLine+1 && columnCaseFinish <= m_selectedPieceColumn-1){
        directionToPlay = 3;
    }
    else if(lineCaseFinish >= m_selectedPieceLine+1 && columnCaseFinish >= m_selectedPieceColumn+1){
        directionToPlay = 4;
    }

    return directionToPlay;
}

//A CHECKER

void RuleDamesWidget::QueenMovement(){
    int lineNumberOfOrigin = m_selectedPieceLine;
    int columnNumberOfOrigin = m_selectedPieceColumn;
    Player& player = m_currentPlayer;

    int lineCaseFinish = sender()->property(lineIndex).toInt();
    int columnCaseFinish = sender()->property(columnIndex).toInt();

    if(!(this->CheckValidDestinationCaseCoordonates(lineCaseFinish,columnCaseFinish)
            &&(lineNumberOfOrigin!=lineCaseFinish)
            &&(columnNumberOfOrigin!=columnCaseFinish)
            &&this->QueenPathIsClear(this->grid->GetCase(lineNumberOfOrigin,columnNumberOfOrigin),this->grid->GetCase(lineCaseFinish,columnCaseFinish)))){

        m_lblInfos->setText("Finish case is not valid");
    }
    else{
        this->grid->GetCase(lineCaseFinish,columnCaseFinish).playerOnCase=player.valueOfToken;
        this->grid->GetCase(lineCaseFinish,columnCaseFinish).pieceType=EnumPiece::QUEEN;
        this->grid->GetCase(lineNumberOfOrigin,columnNumberOfOrigin).ResetDynamicCase();
        IterateGame();
    }
}

bool RuleDamesWidget::CheckValidDestinationCaseCoordonates(int& lineCaseFinish, int& columnCaseFinish){
    if(!this->CaseCoordinatesAreValid(lineCaseFinish,columnCaseFinish)){
        m_lblInfos->setText("Please input a destination case in the grid");
    }
    return(this->CaseCoordinatesAreValid(lineCaseFinish,columnCaseFinish));
}


void RuleDamesWidget::PawnTakeEnemyPiece(){
    int lineNumber = m_selectedPieceLine;
    int columnNumber = m_selectedPieceColumn;
    Player& player = m_currentPlayer;

    int lineCaseFinish = sender()->property(lineIndex).toInt();
    int columnCaseFinish = sender()->property(columnIndex).toInt();

    int directionToTakePiece = ComputeDirectionToTakePiece(lineCaseFinish, columnCaseFinish);
    switch (directionToTakePiece){
        case 1:
            this->PawnTakeEnemyUpLeft(lineNumber,columnNumber,player);
            break;
        case 2:
            this->PawnTakeEnemyUpRight(lineNumber,columnNumber,player);
            break;
        case 3:
            this->PawnTakeEnemyDownLeft(lineNumber,columnNumber,player);
            break;
        case 4:
            this->PawnTakeEnemyDownRight(lineNumber,columnNumber,player);
            break;
        default:
            m_lblInfos->setText("Please select a valid take");
    }
}

void RuleDamesWidget::PawnCanTakeEnemyPieceAgain(int lineCase, int columnCase){
    if(!PawnCanTakeEnemyPiece(lineCase, columnCase)){
        IterateGame();
    }
    else{
        RefreshBoard();
        m_selectedPieceLine = lineCase;
        m_selectedPieceColumn = columnCase;
        SetEventPawnTakeEnemyPiece();
    }
}

void RuleDamesWidget::RemovePiecesTaken(){
    for(int lineNumber=0;lineNumber<this->grid->GetNumberOfLine();++lineNumber){
        for(int columnNumber=0;columnNumber<this->grid->GetNumberOfColumn();++columnNumber){
            if(this->grid->GetCase(lineNumber,columnNumber).playerOnCase==-1){
                this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
            }
        }
    }
}

bool RuleDamesWidget::QueenCanMove(int lineNumber, int columnNumber){
    return(RuleDamesWidget::QueenCanMoveUpLeft(lineNumber,columnNumber)
           ||RuleDamesWidget::QueenCanMoveUpRight(lineNumber,columnNumber)
           ||RuleDamesWidget::QueenCanMoveDownLeft(lineNumber,columnNumber)
           ||RuleDamesWidget::QueenCanMoveDownRight(lineNumber,columnNumber));
}

bool RuleDamesWidget::QueenCanMoveUpLeft(int lineNumber,int columnNumber){
    return this->PawnCanMoveUpLeft(lineNumber,columnNumber);
}
bool RuleDamesWidget::QueenCanMoveUpRight(int lineNumber,int columnNumber){
    return this->PawnCanMoveUpRight(lineNumber,columnNumber);
}
bool RuleDamesWidget::QueenCanMoveDownLeft(int lineNumber,int columnNumber){
    return this->PawnCanMoveDownLeft(lineNumber,columnNumber);
}
bool RuleDamesWidget::QueenCanMoveDownRight(int lineNumber,int columnNumber){
    return this->PawnCanMoveDownRight(lineNumber,columnNumber);
}


bool RuleDamesWidget::QueenPathIsDiagonal(Case& CaseStart, Case& CaseFinish){
    int diffLine=abs(CaseStart.GetNumLine()-CaseFinish.GetNumLine());
    int diffColumn=abs(CaseStart.GetNumColumn()-CaseFinish.GetNumColumn());
    return(diffLine==diffColumn);
}

bool RuleDamesWidget::QueenPathGoesUp(Case& CaseStart, Case& CaseFinish){
    return(CaseFinish.GetNumLine()-CaseStart.GetNumLine()<0);
}

bool RuleDamesWidget::QueenPathGoesLeft(Case& CaseStart, Case& CaseFinish){
    return(CaseFinish.GetNumColumn()-CaseStart.GetNumColumn()<0);
}


bool RuleDamesWidget::QueenPathIsClear(Case &CaseStart, Case &CaseFinish){
    if(!QueenPathIsDiagonal(CaseStart,CaseFinish)){
        //m_lblInfos->setText("Path is not diagonal");
        return false;
    }
  bool pathGoesUp=this->QueenPathGoesUp(CaseStart,CaseFinish);
  bool pathGoesLeft=this->QueenPathGoesLeft(CaseStart,CaseFinish);
  if(pathGoesUp){
      if(pathGoesLeft){
          return this->QueenPathUpLeftIsClear(CaseStart,CaseFinish);
      }
      else{
          return this->QueenPathUpRightIsClear(CaseStart,CaseFinish);
      }
  }
  else{
      if(pathGoesLeft){
          return this->QueenPathDownLeftIsClear(CaseStart,CaseFinish);
      }
      else{
          return this->QueenPathDownRightIsClear(CaseStart,CaseFinish);
      }
  }
};

bool RuleDamesWidget::QueenPathUpLeftIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()-1;
    for(int lineToTest=CaseStart.GetNumLine()-1;lineToTest>=CaseFinish.GetNumLine();--lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            --columnToTest;
        }
    return true;
};

bool RuleDamesWidget::QueenPathUpRightIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()+1;
    for(int lineToTest=CaseStart.GetNumLine()-1;lineToTest>=CaseFinish.GetNumLine();--lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            ++columnToTest;
        }
    return true;
};

bool RuleDamesWidget::QueenPathDownLeftIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()-1;
    for(int lineToTest=CaseStart.GetNumLine()+1;lineToTest<=CaseFinish.GetNumLine();++lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            --columnToTest;
        }
    return true;
};
bool RuleDamesWidget::QueenPathDownRightIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()+1;
    for(int lineToTest=CaseStart.GetNumLine()+1;lineToTest<=CaseFinish.GetNumLine();++lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            ++columnToTest;
        }
    return true;
};

void RuleDamesWidget::PawnTurn(int lineNumberToPlay, int columnNumberToPlay){
    if(this->PawnCanTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay)){
        m_lblInfos->setText("Select a piece to take");
        SetEventPawnTakeEnemyPiece();
    }
    else{
        m_lblInfos->setText("Select a case to move");
        SetEventPawnMovement();
    }
}

bool RuleDamesWidget::QueenCanTakeEnemyPiece(int lineNumber, int columnNumber){
    return (  (this->QueenCanTakeEnemyPieceUpLeft(lineNumber,columnNumber))
            ||(this->QueenCanTakeEnemyPieceUpRight(lineNumber,columnNumber))
            ||(this->QueenCanTakeEnemyPieceDownLeft(lineNumber,columnNumber))
            ||(this->QueenCanTakeEnemyPieceDownRight(lineNumber,columnNumber))
           );
}

bool RuleDamesWidget::QueenCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest>=0;--lineToTest){
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
        if(this->PawnCanTakeEnemyPieceUpLeft(lineToTest,columnToTest)
                &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        --columnToTest;
    }
    return false;
}

bool RuleDamesWidget::QueenCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest>=0;--lineToTest){
        if(this->PawnCanTakeEnemyPieceUpRight(lineToTest,columnToTest)
                &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        ++columnToTest;
    }
    return false;
}

bool RuleDamesWidget::QueenCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest<this->grid->GetNumberOfLine();++lineToTest){
        if(this->PawnCanTakeEnemyPieceDownLeft(lineToTest,columnToTest)
                &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        --columnToTest;
    }
    return false;
}

bool RuleDamesWidget::QueenCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest<this->grid->GetNumberOfLine();++lineToTest){
        if(this->PawnCanTakeEnemyPieceDownRight(lineToTest,columnToTest)
          &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        ++columnToTest;
    }
    return false;
}

void RuleDamesWidget::QueenTakesEnemyPiece(){
    int lineNumber = sender()->property(lineIndex).toInt();
    int columnNumber = sender()->property(columnIndex).toInt();
    Player& player = m_currentPlayer;
    int directionToTakePiece=ComputeDirectionToTakePiece(lineNumber, columnNumber);

    switch (directionToTakePiece){
    case 1:
        if(this->QueenCanTakeEnemyPieceUpLeft(m_selectedPieceLine,m_selectedPieceColumn)){
            this->QueenTakesEnemyPieceUpLeft(m_selectedPieceLine,m_selectedPieceColumn,player);
        }
        break;
    case 2:
        if(this->QueenCanTakeEnemyPieceUpRight(m_selectedPieceLine,m_selectedPieceColumn)){
            this->QueenTakesEnemyPieceUpRight(m_selectedPieceLine,m_selectedPieceColumn,player);
        }
        break;
    case 3:
        if(this->QueenCanTakeEnemyPieceDownLeft(m_selectedPieceLine,m_selectedPieceColumn)){
            this->QueenTakesEnemyPieceDownLeft(m_selectedPieceLine,m_selectedPieceColumn,player);
        }
        break;
    case 4:
         if(this->QueenCanTakeEnemyPieceDownRight(m_selectedPieceLine,m_selectedPieceColumn)){
            this->QueenTakesEnemyPieceDownRight(m_selectedPieceLine,m_selectedPieceColumn,player);
        }
        break;
    default:
        m_lblInfos->setText("Please select a valid take");
    }
}

void RuleDamesWidget::QueenTakesEnemyPieceUpLeft(int & lineNumber, int & columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber-1;
    int lineToTest=lineNumber-1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathUpLeftIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest-1;
        columnToTest=columnToTest-1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest+1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    m_selectedPieceLine = lineToTest+1;
    m_selectedPieceColumn = columnToTest+1;
    //take
    this->grid->GetCase(lineToTest+1, columnToTest+1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest-1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    //this->grid->DisplayGrid();
    RefreshBoard();
    QMessageBox* continueMoveMessage=new QMessageBox(this);
    continueMoveMessage->setText("Do you want to continue your move ?");
    continueMoveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int moveCont=continueMoveMessage->exec();

    if(moveCont==QMessageBox::No){
        QueenCanTakeEnemyPieceAgain(lineToTest-1,columnNumber-1);
        return;
    }
    else{
        if(QueenCanMoveUpLeft(m_selectedPieceLine, m_selectedPieceColumn)){
            SetEventQueenMoveAfterTakeEnemyPieceUpLeft();
        }
        else{
            QueenCanTakeEnemyPieceAgain(lineToTest-1,columnNumber-1);
        }
    }
    RefreshBoard();
};

void RuleDamesWidget::QueenMoveAfterTakeEnemyPieceUpLeft(){
    int lineToTest = m_selectedPieceLine;
    int columnToTest = m_selectedPieceColumn;

    int lineToMove = sender()->property(lineIndex).toInt();
    int columnToMove = sender()->property(columnIndex).toInt();

    if(ComputeDirectionToTakePiece(lineToTest, columnToTest) == 1){
        Case caseIntermediate=Case();
        caseIntermediate=this->grid->GetCase(lineToTest-1,columnToTest-1);
        Case caseFinish=this->grid->GetCase(lineToMove,columnToMove);
        if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
            &&this->QueenPathUpLeftIsClear(caseIntermediate,caseFinish)){

            this->grid->GetCase(lineToTest-1, columnToTest-1).ResetDynamicCase();
            this->grid->GetCase(lineToMove,columnToMove).playerOnCase=m_currentPlayer.valueOfToken;
            this->grid->GetCase(lineToMove,columnToMove).pieceType=EnumPiece::QUEEN;
            //QueenCanTakeEnemyPieceAgain(lineToMove,columnToMove);
        }
    }
}

void RuleDamesWidget::QueenTakesEnemyPieceUpRight(int& lineNumber, int& columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber+1;
    int lineToTest=lineNumber-1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathUpRightIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest-1;
        columnToTest=columnToTest+1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest+1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    m_selectedPieceLine = lineToTest+1;
    m_selectedPieceColumn = columnToTest-1;
    //take
    this->grid->GetCase(lineToTest+1, columnToTest-1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest-1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    //this->grid->DisplayGrid();
    RefreshBoard();
    QMessageBox* continueMoveMessage=new QMessageBox(this);
    continueMoveMessage->setText("Do you want to continue your move ?");
    continueMoveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int moveCont=continueMoveMessage->exec();

    if(moveCont==QMessageBox::No){
        QueenCanTakeEnemyPieceAgain(lineToTest-1,columnToTest+1);
        return;
    }
    else{
        if(QueenCanMoveUpRight(m_selectedPieceLine, m_selectedPieceColumn)){
            SetEventQueenMoveAfterTakeEnemyPieceUpRight();
        }
        else{
            QueenCanTakeEnemyPieceAgain(lineToTest-1,columnToTest+1);
        }
    }
    RefreshBoard();
}

void RuleDamesWidget::QueenMoveAfterTakeEnemyPieceUpRight(){
    int lineToTest = m_selectedPieceLine;
    int columnToTest = m_selectedPieceColumn;

    int lineToMove = sender()->property(lineIndex).toInt();
    int columnToMove = sender()->property(columnIndex).toInt();

    if(ComputeDirectionToTakePiece(lineToTest, columnToTest) == 2){
        Case caseIntermediate=Case();
        caseIntermediate=this->grid->GetCase(lineToTest-1,columnToTest+1);
        Case caseFinish=this->grid->GetCase(lineToMove,columnToMove);
        if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
            &&this->QueenPathUpRightIsClear(caseIntermediate,caseFinish)){

            this->grid->GetCase(lineToTest-1, columnToTest+1).ResetDynamicCase();
            this->grid->GetCase(lineToMove,columnToMove).playerOnCase=m_currentPlayer.valueOfToken;
            this->grid->GetCase(lineToMove,columnToMove).pieceType=EnumPiece::QUEEN;
            //QueenCanTakeEnemyPieceAgain(lineToMove,columnToMove);
        }
    }
}

void RuleDamesWidget::QueenTakesEnemyPieceDownLeft(int & lineNumber, int & columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber-1;
    int lineToTest=lineNumber+1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathDownLeftIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest+1;
        columnToTest=columnToTest-1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest-1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    m_selectedPieceLine = lineToTest-1;
    m_selectedPieceColumn = columnToTest+1;
    //take
    this->grid->GetCase(lineToTest-1, columnToTest+1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest+1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    //this->grid->DisplayGrid();
    RefreshBoard();
    QMessageBox* continueMoveMessage=new QMessageBox(this);
    continueMoveMessage->setText("Do you want to continue your move ?");
    continueMoveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int moveCont=continueMoveMessage->exec();

    if(moveCont==QMessageBox::No){
        QueenCanTakeEnemyPieceAgain(lineToTest+1,columnToTest-1);
        return;
    }
    else{
        if(QueenCanMoveDownLeft(m_selectedPieceLine, m_selectedPieceColumn)){
            SetEventQueenMoveAfterTakeEnemyPieceDownLeft();
        }
        else{
            QueenCanTakeEnemyPieceAgain(lineToTest+1,columnToTest-1);
        }
    }
    RefreshBoard();
}

void RuleDamesWidget::QueenMoveAfterTakeEnemyPieceDownLeft(){
    int lineToTest = m_selectedPieceLine;
    int columnToTest = m_selectedPieceColumn;

    int lineToMove = sender()->property(lineIndex).toInt();
    int columnToMove = sender()->property(columnIndex).toInt();

    if(ComputeDirectionToTakePiece(lineToTest, columnToTest) == 3){
        Case caseIntermediate=Case();
        caseIntermediate=this->grid->GetCase(lineToTest+1,columnToTest-1);
        Case caseFinish=this->grid->GetCase(lineToMove,columnToMove);
        if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
            &&this->QueenPathDownLeftIsClear(caseIntermediate,caseFinish)){

            this->grid->GetCase(lineToTest+1, columnToTest-1).ResetDynamicCase();
            this->grid->GetCase(lineToMove,columnToMove).playerOnCase=m_currentPlayer.valueOfToken;
            this->grid->GetCase(lineToMove,columnToMove).pieceType=EnumPiece::QUEEN;
            //QueenCanTakeEnemyPieceAgain(lineToMove,columnToMove);
        }
    }
}

void RuleDamesWidget::QueenTakesEnemyPieceDownRight(int & lineNumber, int & columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber+1;
    int lineToTest=lineNumber+1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathDownRightIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest+1;
        columnToTest=columnToTest+1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }
    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest-1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    m_selectedPieceLine = lineToTest-1;
    m_selectedPieceColumn = columnToTest-1;
    //take
    this->grid->GetCase(lineToTest-1, columnToTest-1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest+1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    RefreshBoard();

    QMessageBox* continueMoveMessage=new QMessageBox(this);
    continueMoveMessage->setText("Do you want to continue your move ?");
    continueMoveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int moveCont=continueMoveMessage->exec();

    if(moveCont==QMessageBox::No || this->QueenCanTakeEnemyPiece(lineToTest+1,columnToTest+1)){
        QueenCanTakeEnemyPieceAgain(lineToTest+1,columnToTest+1);
        return;
    }
    else{
        if(QueenCanMoveDownRight(m_selectedPieceLine, m_selectedPieceColumn)){
            SetEventQueenMoveAfterTakeEnemyPieceDownRight();
        }
        else{
            QueenCanTakeEnemyPieceAgain(lineToTest+1,columnToTest+1);
        }
    }
    RefreshBoard();
}

void RuleDamesWidget::QueenMoveAfterTakeEnemyPieceDownRight(){
    int lineToTest = m_selectedPieceLine;
    int columnToTest = m_selectedPieceColumn;

    int lineToMove = sender()->property(lineIndex).toInt();
    int columnToMove = sender()->property(columnIndex).toInt();

    if(ComputeDirectionToTakePiece(lineToTest, columnToTest) == 4){
        Case caseIntermediate=Case();
        caseIntermediate=this->grid->GetCase(lineToTest+1,columnToTest+1);
        Case caseFinish=this->grid->GetCase(lineToMove,columnToMove);
        if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
            &&this->QueenPathDownRightIsClear(caseIntermediate,caseFinish)){

            this->grid->GetCase(lineToTest+1, columnToTest+1).ResetDynamicCase();
            this->grid->GetCase(lineToMove,columnToMove).playerOnCase=m_currentPlayer.valueOfToken;
            this->grid->GetCase(lineToMove,columnToMove).pieceType=EnumPiece::QUEEN;
            //QueenCanTakeEnemyPieceAgain(lineToMove,columnToMove);
        }
   }
}

void RuleDamesWidget::QueenCanTakeEnemyPieceAgain(int lineCase, int columnCase){
    if(!QueenCanTakeEnemyPiece(lineCase, columnCase)){
        IterateGame();
    }
    else{
        RefreshBoard();
        m_selectedPieceLine = lineCase;
        m_selectedPieceColumn = columnCase;
        SetEventQueenTakesEnemyPiece();
    }
}

void RuleDamesWidget::QueenTurn(int lineNumberToPlay, int columnNumberToPlay){
    if(this->QueenCanTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay)){
        m_lblInfos->setText("Select a piece to take");
        SetEventQueenTakesEnemyPiece();
    }
    else{
        m_lblInfos->setText("Select a case to move");
        SetEventQueenMovement();
    }
}

void RuleDamesWidget::PlayerTurn(const Player & player){
    m_currentPlayer = player;
    m_lblCurrentPlayer->setText("Your turn player "+QString::number(player.valueOfToken));
    m_lblInfos->setText("Please select a piece");
    SetEventPlayerChooseACase();
}

void RuleDamesWidget::NextPlayer(){
    if(m_currentPlayer.name == playerList[0].name){
        PlayerTurn(playerList[1]);
    }
    else{
        PlayerTurn(playerList[0]);
    }
}

bool RuleDamesWidget::VictoryCondition(const Player & player){
    for(int lineNumber=0;lineNumber<this->grid->GetNumberOfLine();++lineNumber){
        for(int columnNumber=0;columnNumber<this->grid->GetNumberOfColumn();++columnNumber){
            if(!(this->grid->GetCase(lineNumber,columnNumber).CaseIsEmpty()||this->CaseBelongsToPlayer(lineNumber,columnNumber,player)))
                return false;
        }
    }
    return true;
};

//Impossible de faire match nul aux dames, sauf si abandon des deux joueurs.
bool RuleDamesWidget::NoWinnerEndCondition(){
    return false;
};

void RuleDamesWidget::GameFlow(){
    m_lblInfos->setText("Would you like to load your previous game?");
    int saveChoice=-1;

    QMessageBox* saveMessage=new QMessageBox(this);
    saveMessage->setText("Load previous file ?");
    saveMessage->setInformativeText("Selecting No will create a new game");
    saveMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    saveChoice=saveMessage->exec();

    if(saveChoice == QMessageBox::Yes){
        m_saveFile = QFileDialog::getOpenFileName(this, tr("Open save file"), "..", tr("json Files (*.json)"));
        if(m_saveFile == NULL){
            m_saveFile = "saveDames.json";
            m_lblInfos->setText("New Game");
            this->SetupGame();
            this->PlayerTurn(playerList[0]);
        }
        else{
            m_lblInfos->setText("Game loaded");
            LoadGame();
            RefreshBoard();
        }
    }
    else{
        m_lblInfos->setText("New Game");
        this->SetupGame();
        this->PlayerTurn(playerList[0]);
    }
};

void RuleDamesWidget::LoadGame(){
    QFile loadFile(m_saveFile);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    QJsonObject json = loadDoc.object();
    if(json["GameType"].toString().compare(m_gameType) == 0){
        Read(json);
    }
    else{
        qWarning("Wrong save file.");
        m_lblInfos->setText("New Game");
        this->SetupGame();
        this->PlayerTurn(playerList[0]);
    }
}

void RuleDamesWidget::SaveGame() const{
    QFile saveFile(m_saveFile);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonObject gameObject;
    Write(gameObject);
    QJsonDocument saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());
}

void RuleDamesWidget::RemoveSaveFile() const{
    QFile saveFile(m_saveFile);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    saveFile.remove();
}

void RuleDamesWidget::Read(const QJsonObject &json){

    grid->ClearGrid();
    QJsonArray caseArray = json["cases"].toArray();
    for (int caseIndex = 0; caseIndex < caseArray.size(); ++caseIndex) {
        QJsonObject caseObject = caseArray[caseIndex].toObject();
        Case caseToRead;
        caseToRead.Read(caseObject);
        grid->SetCaseInGrid(caseToRead);
    }

    if(json["playerTurn"].toInt() == 1){
        this->PlayerTurn(playerList[0]);
    }
    else{
        this->PlayerTurn(playerList[1]);
    }
}

void RuleDamesWidget::Write(QJsonObject &json) const{

    QJsonArray caseArray;
    for(std::vector<Case> listCases : grid->GetGrid()) {
        for(Case caseToWrite : listCases) {
                QJsonObject caseObject;
                caseToWrite.Write(caseObject);
                caseArray.append(caseObject);
            }
    }
    json["GameType"] = m_gameType;
    json["playerTurn"] = m_currentPlayer.valueOfToken;
    json["cases"] = caseArray;
}

void RuleDamesWidget::InitBoard()
{
    grid->ClearGrid();
    SetupGame();
    for(int i = 0; i < grid->GetNumberOfLine(); i++)
    {
        for(int j = 0; j < grid->GetNumberOfColumn(); j++)
        {
            QPushButton* btn = new QPushButton(this);
            btn->move(25+j*40, 100+i*40);
            btn->setFixedSize(40, 40);
            btn->setFont(QFont("Arial", 13));
            auto indexI=QString{"%1"}.arg(i);
            auto indexJ=QString{"%1"}.arg(j);
            btn->setProperty(lineIndex,indexI );
            btn->setProperty(columnIndex,indexJ);
            connect(btn,SIGNAL(clicked()),this, SLOT(PlayerChoosesACase()));
            QString text=QString::number(grid->GetCase(i,j).playerOnCase);
            if(grid->GetCase(i,j).playerOnCase!=0){
                btn->setText(text);
            }
            m_boardButtons.push_back(btn);
        }
    }
    RefreshBoard();
}

void RuleDamesWidget::RefreshBoard(){
    for(QPushButton* btn : m_boardButtons){
        int lineNumber = btn->property(lineIndex).toInt();
        int columnNumber = btn->property(columnIndex).toInt();
        QString text = "";
        if(grid->GetCase(lineNumber,columnNumber).playerOnCase>0 ){
            text=(QString::number(grid->GetCase(lineNumber,columnNumber).playerOnCase)+QVariant::fromValue(grid->GetCase(lineNumber,columnNumber).pieceType).toInt());
        }
        else if(grid->GetCase(lineNumber,columnNumber).playerOnCase==-1){
            text=QVariant::fromValue(grid->GetCase(lineNumber,columnNumber).pieceType).toInt();
        }
        btn->setText(text);
    }
}

void RuleDamesWidget::Rematch(){
    QMessageBox* rematchMessage=new QMessageBox(this);
    rematchMessage->setText("Would you like a rematch ?");
    rematchMessage->setInformativeText("Selecting No will close this window");
    rematchMessage->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int rematchChoice=rematchMessage->exec();
    switch(rematchChoice){
        case QMessageBox::Yes:
            grid->ClearGrid();
            RefreshBoard();
            PlayerTurn(playerList[0]);
        break;
        default:
        ;
    }
}

void RuleDamesWidget::SetEventPlayerChooseACase()
{
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(PlayerChoosesACase()));
    }
}

void RuleDamesWidget::SetEventPawnTakeEnemyPiece()
{
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(PawnTakeEnemyPiece()));
    }
}

void RuleDamesWidget::SetEventPawnMovement()
{
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(PawnMovement()));
    }
}


void RuleDamesWidget::SetEventQueenTakesEnemyPiece()
{
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(QueenTakesEnemyPiece()));
    }
}

void RuleDamesWidget::SetEventQueenMovement()
{
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(QueenMovement()));
    }
}

void RuleDamesWidget::SetEventQueenMoveAfterTakeEnemyPieceUpLeft(){
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceUpLeft()));
    }
}

void RuleDamesWidget::SetEventQueenMoveAfterTakeEnemyPieceUpRight(){
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceUpRight()));
    }
}

void RuleDamesWidget::SetEventQueenMoveAfterTakeEnemyPieceDownLeft(){
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceDownLeft()));
    }
}

void RuleDamesWidget::SetEventQueenMoveAfterTakeEnemyPieceDownRight(){
    DisconnectAllButtonEvents();
    for(QPushButton* btn : m_boardButtons){
        connect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceDownRight()));
    }
}

void RuleDamesWidget::DisconnectAllButtonEvents()
{
    for(QPushButton* btn : m_boardButtons){
        disconnect(btn,SIGNAL(clicked()),this, SLOT(PlayerChoosesACase()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(PawnTakeEnemyPiece()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(PawnMovement()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(QueenTakesEnemyPiece()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(QueenMovement()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceUpLeft()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceUpRight()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceDownLeft()));
        disconnect(btn,SIGNAL(clicked()),this, SLOT(QueenMoveAfterTakeEnemyPieceDownRight()));
    }
}
