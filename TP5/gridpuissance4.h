#ifndef GRIDPUISSANCE4_H
#define GRIDPUISSANCE4_H

#include "gridstatic.h"
#include <memory>

class GridPuissance4 : public GridStatic
{
public:
    int numberOfAlignedCasesToWin=4;
    GridPuissance4();
    bool CheckIfANumberOfTokensAreAligned(CaseVector caseVectorToTest ,const Player &currentPlayer);
    bool LineCompletedByPlayer(int lineNumber, const Player &currentPlayer);
    bool ColumnCompletedByPlayer(int columnNumber, const Player &currentPlayer);
    bool DiagonalULDRCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer);
    bool DiagonalURDLCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer);
};

#endif // GRIDPUISSANCE4_H
