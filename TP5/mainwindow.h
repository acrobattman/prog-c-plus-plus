#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "mainmenuwidget.h"
#include "gamechoicewidget.h"
#include "rulemorpionwidget.h"
#include "rulepuissance4widget.h"
#include "ruledameswidget.h"
#include "ruleothellowidget.h"
#include <QMainWindow>
#include <QPushButton>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

signals:

public slots:
    void ReturnMenu();
    void GameSelection();
    void StartMorpion();
    void StartPuissance4();
    void StartDames();
    void StartOthello();

private:
    QWidget* ActiveWidget;
    QPushButton m_BtnQuit;
    MainMenuWidget* MainMenu;
    GameChoiceWidget* GameChoice;
    RuleMorpionWidget* GameMorpion;
    RulePuissance4Widget* GamePuissance4;
    RuleDamesWidget* GameDames;
    RuleOthelloWidget* GameOthello;

    void HideWidgets();
};

#endif // MAINWINDOW_H
