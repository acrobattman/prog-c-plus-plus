#ifndef MAINMENUWIDGET_H
#define MAINMENUWIDGET_H
#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>

class MainMenuWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainMenuWidget(QWidget *parentWidget = nullptr);


signals:
    void QuitApp();

public slots:

private:
    QPushButton* m_btnQuit;
    QPushButton* m_btnGame;
    QLabel* m_lblTitle;

};

#endif // MAINMENUWIDGET_H
