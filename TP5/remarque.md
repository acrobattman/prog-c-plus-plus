# Remarque sur le rendu du TP
-----------------------------

Nous avons essayé de respecter au maximum les différents principes SOLID. Cependant, il y a surement encore pas mal de choses qui pourraient être améliorées. Notamment, la classe ruleDame qui contient énormément de méthodes et qui ne fait surement pas qu'une seule chose, ainsi que l'affichage des grilles. Certaines méthodes ont beaucoup de paramètre (3, 4) et nous aurions peut être pu réduire cela en passant plus souvent des cases en paramètre et en créant une classe ou une structure position contenant le numéro de la colonne et celui de la ligne ainsi que les méthodes qui vont avec. Pour finir, il y très sûrement encore un peu de code qui se répète par endroit même si nous avons essayé de faire un maximum de fonction.

Ensuite, nous avons très souvent utilisé l'héritage, mais dans certains cas il aurait peut être fallut réfléchir si la composition n’aurait pas été plus adapté.

Il y a ensuite, très certainement certains choix de conceptions et de développements qui pourraient être discutés.

Nous avons malheureusement dû vers la fin du projet revoir la qualité de l'application (conception, choix techniques et développement) un peu à la baisse par manque de temps. De plus, cette variable était la seule sûre laquelle nous pouvions agir. Ce projet étant un projet pour les cours, nous ne pouvions pas par exemple augmenter les coûts et payer des personnes pour nous aider à faire quelque chose de plus propre et gagner du temps. Ensuite, il en va de même pour la variable délais sûre laquelle nous ne pouvions pas non plus intervenir. Il ne restait donc plus que la qualité du projet où nous pouvions agir pour finir le TP dans les temps. 

En conclusion, nous sommes dans l'ensemble satisfaits de notre travail même si nous aurions aimé faire mieux sur certains points.
