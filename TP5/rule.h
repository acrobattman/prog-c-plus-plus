#ifndef RULE_H
#define RULE_H
#include "player.h"
#include "vector"
#include "grid.h"
#include "gridstaticfactory.h"
#include "griddynamicfactory.h"
#include <string>
#include <QJsonObject>
#include <QList>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDialog>
#include <QMessageBox>
#include <QFileDialog>


class Rule
{
public:
    Rule();
    virtual void PlayerTurn(const Player & player)=0;
    virtual bool VictoryCondition(const Player & player)=0;
    virtual bool NoWinnerEndCondition()=0;
    virtual void GameFlow()=0;
    void AddPlayerToPlayerList(Player& playerToAdd);
    bool Rematch();

    virtual void LoadGame()=0;
    virtual void SaveGame() const=0;

    virtual void Read(const QJsonObject &json)=0;
    virtual void Write(QJsonObject &json) const=0;


protected:
    std::vector<Player> playerList;
};

    const char lineIndex[]="lineIndex";
    const char columnIndex[]="columnIndex";

#endif // RULE_H
