#include "gridstatic.h"
#include <memory>

GridStatic::GridStatic(int numberOfLine, int numberOfColumn):Grid(numberOfLine,numberOfColumn)
{

}

void GridStatic::PlayAToken(const Player &player, Case &caseToPlay){
    caseToPlay.playerOnCase=player.valueOfToken;
    caseToPlay.pieceType=EnumPiece::TOKEN;
}

bool GridStatic::CaseVectorCompletedByAPlayer(CaseVector& casevector, const Player &currentPlayer){
    return casevector.IsCompletedByPlayer(currentPlayer);
}


bool GridStatic::GridIsFull(){
    for(int lineNumber=0;lineNumber<this->numberOfLine;++lineNumber){
        for(int columnNumber=0;columnNumber<this->numberOfColumn;++columnNumber){
            if(this->GetCase(lineNumber,columnNumber).CaseIsEmpty()){
                return false;
            }
        }
    }
    return true;
}

bool GridStatic::LineNumberIsValid(int lineNumber){
    return (lineNumber>=0&&lineNumber<this->numberOfLine);
}

bool GridStatic::ColumnNumberIsValid(int columnNumber){
    return(columnNumber>=0&&columnNumber<this->numberOfColumn);
}

bool GridStatic::CaseCoordinatesAreValid(int lineNumber, int columnNumber){
    return(this->LineNumberIsValid(lineNumber)&&this->ColumnNumberIsValid(columnNumber));
}

Line GridStatic::CreateLine(int lineNumber){
    CaseVector casevector = CaseVector();
       for(int columnNumber=0;columnNumber<this->numberOfColumn;columnNumber++){
           casevector.AddCaseToVector(this->GetCase(lineNumber,columnNumber));
       }
       Line line = Line(casevector);
       return line;
}

Column GridStatic::CreateColumn(int columnNumber){
    CaseVector casevector = CaseVector();
       for(int lineNumber=0;lineNumber<this->numberOfLine;lineNumber++){
           casevector.AddCaseToVector(this->GetCase(lineNumber,columnNumber));
       }
       Column column = Column(casevector);
       return column;
}


DiagonalULDR GridStatic::CreateDiagonalULDR(int lineNumber, int columnNumber){
    CaseVector casevector = CaseVector();
    while((lineNumber>0)&&(columnNumber>0)){
        lineNumber=lineNumber-1;
        columnNumber=columnNumber-1;
    }
    while((lineNumber<this->numberOfLine)&&(columnNumber<this->numberOfColumn)){
         casevector.AddCaseToVector(this->GetCase(lineNumber,columnNumber));
         lineNumber=lineNumber+1;
         columnNumber=columnNumber+1;
    }
    DiagonalULDR diagonalULDR= DiagonalULDR(casevector);
    return diagonalULDR;
}


DiagonalURDL GridStatic::CreateDiagonalURDL(int lineNumber, int columnNumber){
    CaseVector casevector = CaseVector();
    while((lineNumber>0)&&(columnNumber<this->numberOfColumn-1)){
        lineNumber=lineNumber-1;
        columnNumber=columnNumber+1;
    }
    //std::cout<<lineNumber<<" "<<columnNumber<<std::endl;
    while((lineNumber<this->numberOfLine)&&(columnNumber>=0)){
         //std::cout<<"Ajout Case : "<<GetCase(lineNumber,columnNumber).GetNumLine()<<" "<<GetCase(lineNumber,columnNumber).GetNumColumn()<<std::endl;
         casevector.AddCaseToVector(this->GetCase(lineNumber,columnNumber));
         //std::cout<<"Case : "<<GetCase(lineNumber,columnNumber).GetNumLine()<<" "<<GetCase(lineNumber,columnNumber).GetNumColumn()<<" belong : "<<GetCase(lineNumber,columnNumber).playerOnCase<<std::endl;
         lineNumber=lineNumber+1;
         columnNumber=columnNumber-1;
    }
    DiagonalURDL diagonalURDL= DiagonalURDL(casevector);
    return diagonalURDL;
}



bool GridStatic::LineCompletedByPlayer(int lineNumber, const Player &currentPlayer){
    Line lineToTest=this->CreateLine(lineNumber);
    return (this->CaseVectorCompletedByAPlayer(lineToTest,currentPlayer));
}


bool GridStatic::ColumnCompletedByPlayer(int columnNumber, const Player &currentPlayer){
    Column columnToTest=this->CreateColumn(columnNumber);
    return (this->CaseVectorCompletedByAPlayer(columnToTest,currentPlayer));
}


bool GridStatic::DiagonalULDRCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer){
    DiagonalULDR diagonalULDRToTest=this->CreateDiagonalULDR(lineNumber,columnNumber);
    return (this->CaseVectorCompletedByAPlayer(diagonalULDRToTest,currentPlayer));
}


bool GridStatic::DiagonalURDLCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer){
    DiagonalURDL diagonalURDLToTest=this->CreateDiagonalURDL(lineNumber,columnNumber);
    return (this->CaseVectorCompletedByAPlayer(diagonalURDLToTest,currentPlayer));
}
