#ifndef GRID_H
#define GRID_H

#include <vector>
#include "case.h"
#include <memory>
#include <stdio.h>
#include <iostream>
#include "player.h"
#include "casevector.h"
#include "line.h"
#include "column.h"
#include "diagonaluldr.h"
#include "diagonalurdl.h"

class Grid
{
public:
    Grid(int numberOfLine, int numberOfColumn);
    const std::vector<std::vector<Case>> GetGrid();
    int GetNumberOfLine() const;
    int GetNumberOfColumn() const;
    Case& GetCase(int numLine, int numColumn);
    void SetCaseInGrid(Case &caseToSet);
    bool CaseIsEmpty(const Case &caseToTest);
    bool CaseIsOnTheBorderOfGrid(const Case & caseToTest) const;

    void SetNumberOfLine(int numberOfLine);
    void SetNumberOfColumn(int numberOfColumn);

    void DisplayGrid();
    void virtual ClearGrid();

    void virtual PlayAToken(const Player &player,Case &caseToPlay)=0;

protected:
    std::vector<std::vector<Case>> grid;
    int numberOfLine;
    int numberOfColumn;
};

#endif // GRID_H
