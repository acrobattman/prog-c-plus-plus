#ifndef RULEDAMESWIDGET_H
#define RULEDAMESWIDGET_H
#include "rule.h"
#include "griddame.h"
#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>

class RuleDamesWidget: public QWidget, public Rule
{
    Q_OBJECT
public:
    RuleDamesWidget(QWidget *parent = nullptr);
    void PlayerTurn(const Player & player) override;
    bool VictoryCondition(const Player & player) override;
    bool NoWinnerEndCondition() override;
    void GameFlow() override;

    void SetupGame();
    void SetupRowBeginBlack(int lineNumber, int valueOfPlayer);
    void SetupRowBeginWhite(int lineNumber, int valueOfPlayer);
    void SetupPlayer1();
    void SetupPlayer2();

    bool ValidCaseChosenByPlayer(int lineNumber, int columnNumber, const Player &player);
    bool CaseCoordinatesAreValid(int lineNumber, int columnNumber);
    bool LineCoordinateIsValid(int lineNumber);
    bool ColumnCoordinateIsValid(int columnNumber);
    bool CaseBelongsToPlayer(int lineNumber, int columnNumber, const Player & player);
    bool PieceOnCaseCanMoveOrTake(int lineNumber, int columnNumber,const Player & player);

    //void PlayerChoosesACase(int & lineNumber, int & columnNumber,const Player & player);

    void PawnBecomesQueen(int lineNumber, int columnNuber);

    void TransformPawnArrivedOnEndLine();

    //Pawn Controllers
    void PawnTurn(int lineNumber, int columnNumber);

    int ComputePawnDirectionToPlay(int lineCaseFinish, int columnCaseFinish);
    int ComputePawnDirectionToPlayPlayer1(int lineCaseFinish, int columnCaseFinish);
    int ComputePawnDirectionToPlayPlayer2(int lineCaseFinish, int columnCaseFinish);

    //Pawn Movements Checkers

    bool PawnCanMove(int lineNumber, int columnNumber,const Player & player);
    bool PawnCanMoveUpLeft(int lineNumber, int columnNumber);
    bool PawnCanMoveUpRight(int lineNumber, int columnNumber);
    bool PawnCanMoveDownLeft(int lineNumber, int columnNumber);
    bool PawnCanMoveDownRight(int lineNumber, int columnNumber);
    bool PawnCanMoveLeft(int lineNumber, int columnNumber,const Player & player);
    bool PawnCanMoveRight(int lineNumber, int columnNumber,const Player & player);
    bool PawnCanMovePlayer1(int lineNumber, int columnNumber);
    bool PawnCanMovePlayer2(int lineNumber, int columnNumber);

    //Pawn Jump Checkers

    bool PawnCanTakeEnemyPiece(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber);
    bool PawnCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber);
    bool PieceIsBeingTaken(int lineNumber, int columnNumber);
    void RemovePiecesTaken();
    void PawnCanTakeEnemyPieceAgain(int lineCase, int columnCase);

    //Pawn Movements

    void PawnMoveLeft(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveRight(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveUpLeft(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveUpRight(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveDownLeft(int lineNumber, int columnNumber,const Player & player);
    void PawnMoveDownRight(int lineNumber, int columnNumber,const Player & player);

    //Pawn Jumps

    void PawnTakeEnemyUpLeft(int& lineNumber, int& columnNumber,const Player &player);
    void PawnTakeEnemyUpRight(int& lineNumber, int& columnNumber,const Player &player);
    void PawnTakeEnemyDownLeft(int& lineNumber, int& columnNumber,const Player &player);
    void PawnTakeEnemyDownRight(int& lineNumber, int& columnNumber,const Player &player);

    //Queen Movements Checkers

    bool QueenCanMove(int lineNumber, int columnNumber);
    bool QueenCanMoveUpLeft(int lineNumber, int columnNumber);
    bool QueenCanMoveUpRight(int lineNumber, int columnNumber);
    bool QueenCanMoveDownLeft(int lineNumber, int columnNumber);
    bool QueenCanMoveDownRight(int lineNumber, int columnNumber);

    bool QueenPathIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathUpLeftIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathUpRightIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathDownLeftIsClear(Case& CaseStart, Case& CaseFinish);
    bool QueenPathDownRightIsClear(Case& CaseStart, Case& CaseFinish);

    bool QueenPathGoesUp(Case& CaseStart, Case& CaseFinish);
    bool QueenPathGoesLeft(Case& CaseStart, Case& CaseFinish);
    bool QueenPathIsDiagonal(Case& CaseStart, Case& CaseFinish);

    //Queen Jump Controller

    bool QueenCanTakeEnemyPiece(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber);
    bool QueenCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber);
    void QueenCanTakeEnemyPieceAgain(int lineCase, int columnCase);

    //Queen Jump


    void QueenTakesEnemyPieceUpLeft(int & lineNumber, int & columnNumber,const Player & player);
    void QueenTakesEnemyPieceUpRight(int & lineNumber, int & columnNumber,const Player & player);
    void QueenTakesEnemyPieceDownLeft(int & lineNumber, int & columnNumber,const Player & player);
    void QueenTakesEnemyPieceDownRight(int & lineNumber, int & columnNumber,const Player & player);

    //Queen Controller

    void QueenTurn(int lineNumber, int columnNumber);
    bool CheckValidDestinationCaseCoordonates(int& lineCaseFinish, int& columnCaseFinish);

    int ComputeDirectionToTakePiece(int lineCaseFinish, int columnCaseFinish);

    void SetupTest();

    void IterateGame();
    void CheckTypeOfPiece(int lineNumber, int columnNumber);
    void NextPlayer();

    void LoadGame() override;
    void SaveGame() const override;
    void RemoveSaveFile() const;

    void Read(const QJsonObject &json) override;
    void Write(QJsonObject &json) const override;

    std::unique_ptr<GridDynamic> grid = GridDynamicFactory::createGrid(GridDynamicFactory::Dame);

    std::unique_ptr<GridDynamic>& GetGrid();

private slots:
    void PlayerChoosesACase();

    void PawnMovement();
    void PawnTakeEnemyPiece();

    void QueenTakesEnemyPiece();
    void QueenMovement();

    void QueenMoveAfterTakeEnemyPieceUpLeft();
    void QueenMoveAfterTakeEnemyPieceUpRight();
    void QueenMoveAfterTakeEnemyPieceDownLeft();
    void QueenMoveAfterTakeEnemyPieceDownRight();

private:
    QLabel* m_lblTitle;
    QLabel* m_lblInfos;
    QLabel* m_lblCurrentPlayer;
    QPushButton* m_btnReturn;
    std::vector<QPushButton*> m_boardButtons;

    Player m_currentPlayer;

    QString m_saveFile;
    QString m_gameType = "dames";

    int m_selectedPieceLine;
    int m_selectedPieceColumn;

    void InitBoard();
    void RefreshBoard();
    void Rematch();

    void SetEventPlayerChooseACase();
    void SetEventPawnTakeEnemyPiece();
    void SetEventPawnMovement();
    void SetEventSelectValidDestinationCaseCoordonates();
    void SetEventQueenTakesEnemyPiece();
    void SetEventQueenMovement();
    void SetEventQueenMoveAfterTakeEnemyPieceUpLeft();
    void SetEventQueenMoveAfterTakeEnemyPieceUpRight();
    void SetEventQueenMoveAfterTakeEnemyPieceDownLeft();
    void SetEventQueenMoveAfterTakeEnemyPieceDownRight();
    void DisconnectAllButtonEvents();
};

#endif // RULEDAMESWIDGET_H
