#ifndef GRIDDYNAMIC_H
#define GRIDDYNAMIC_H

#include "grid.h"
#include <memory>


class GridDynamic : public Grid
{
public:
    GridDynamic(int numberOfLine, int numberOfColumn);
    void virtual PlayAToken(const Player &player,Case &caseToPlay);
    void ClearGrid();
    void DisplayGrid();
    void UpperLineDisplay();
    void EmptyLineDisplay();
    void EmptyLineSeparationDisplay();
};

#endif // GRIDDYNAMIC_H
