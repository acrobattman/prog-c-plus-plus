#ifndef RULEMORPIONWIDGET_H
#define RULEMORPIONWIDGET_H
#include "gridmorpion.h"
#include "rule.h"
#include <memory>
#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QSignalMapper>

class RuleMorpionWidget : public QWidget, public Rule
{
    Q_OBJECT
public:
    RuleMorpionWidget(QWidget *parent = nullptr);
    void PlayerTurn(const Player& player) override;
    bool VictoryCondition(const Player& player) override;
    bool NoWinnerEndCondition() override;
    void GameFlow() override;
    bool LineVictory(const Player& player);
    bool ColumnVictory(const Player& player);
    bool DiagonalVictory(const Player& player);
    bool DiagonalULDRVictory(const Player& player);
    bool DiagonalURDLVictory(const Player& player);

    void IterateGame();
    void CheckCoordinates(int lineNumber, int columnNumber);
    void NextPlayer();

    void LoadGame() override;
    void SaveGame() const override;
    void RemoveSaveFile() const;

    void Read(const QJsonObject &json) override;
    void Write(QJsonObject &json) const override;

    std::unique_ptr<GridStatic> grid = GridStaticFactory::createGrid(GridStaticFactory::Morpion);

    QSignalMapper * signalMapper;

private slots:
    void PlayerPutsToken();

private:
    QLabel* m_lblTitle;
    QLabel* m_lblInfos;
    QLabel* m_lblCurrentPlayer;
    QPushButton* m_btnReturn;
    std::vector<QPushButton*> m_boardButtons;

    Player m_currentPlayer;

    QString m_saveFile;
    QString m_gameType = "morpion";

    void InitBoard();
    void RefreshBoard();
    void Rematch();
};

#endif // RULEMORPIONWIDGET_H
