#include "ruledames.h"

RuleDames::RuleDames()
{

}

std::unique_ptr<GridDynamic>& RuleDames::GetGrid(){
    return this->grid;
}

void RuleDames::SetupGame(){
    this->grid->ClearGrid();
    this->SetupPlayer1();
    this->SetupPlayer2();
};

void RuleDames::SetupTest(){
    this->grid->ClearGrid();
    this->grid->GetCase(5,5).playerOnCase=1;
    this->grid->GetCase(5,5).pieceType=EnumPiece::QUEEN;

    this->grid->GetCase(4,4).playerOnCase=2;
    this->grid->GetCase(4,4).pieceType=EnumPiece::QUEEN;
}

void RuleDames::SetupPlayer1(){
    this->SetupRowBeginBlack(9,1);
    this->SetupRowBeginBlack(7,1);
    this->SetupRowBeginWhite(8,1);
    this->SetupRowBeginWhite(6,1);
}

void RuleDames::SetupPlayer2(){
    this->SetupRowBeginBlack(1,2);
    this->SetupRowBeginBlack(3,2);
    this->SetupRowBeginWhite(0,2);
    this->SetupRowBeginWhite(2,2);

}




void RuleDames::SetupRowBeginBlack(int lineNumber, int valueOfPlayer){
    for(int columnNumber=0; columnNumber<this->grid->GetNumberOfColumn();columnNumber+=2){
            this->grid->GetCase(lineNumber,columnNumber).pieceType=EnumPiece::PAWN;
            this->grid->GetCase(lineNumber,columnNumber).playerOnCase=valueOfPlayer;
    }
};

void RuleDames::SetupRowBeginWhite(int lineNumber,int valueOfPlayer){
    for(int columnNumber=1; columnNumber<this->grid->GetNumberOfColumn();columnNumber+=2){
            this->grid->GetCase(lineNumber,columnNumber).pieceType=EnumPiece::PAWN;
            this->grid->GetCase(lineNumber,columnNumber).playerOnCase=valueOfPlayer;
    }
};

void RuleDames::PawnBecomesQueen(int lineNumber, int columnNuber){
    this->grid->GetCase(lineNumber,columnNuber).pieceType=EnumPiece::QUEEN;
}

void RuleDames::TransformPawnArrivedOnEndLine(){
    for(int columnNumber=0; columnNumber<this->grid->GetNumberOfColumn();++columnNumber){
        if((this->grid->GetCase(0,columnNumber).playerOnCase==1)&&(this->grid->GetCase(0,columnNumber).pieceType==EnumPiece::PAWN)){
            this->PawnBecomesQueen(0,columnNumber);
        }
        if((this->grid->GetCase(this->grid->GetNumberOfLine()-1,columnNumber).playerOnCase==2)&&(this->grid->GetCase(this->grid->GetNumberOfLine()-1,columnNumber).pieceType==EnumPiece::PAWN)){
            this->PawnBecomesQueen(0,columnNumber);
        }
    }
}


bool RuleDames::ValidCaseChosenByPlayer(int lineNumber, int columnNumber, const Player & player){
    return(this->CaseCoordinatesAreValid(lineNumber,columnNumber)
           &&this->CaseBelongsToPlayer(lineNumber,columnNumber, player)
           &&this->PieceOnCaseCanMoveOrTake(lineNumber,columnNumber, player));
};

bool RuleDames::LineCoordinateIsValid(int lineNumber){
    return ((lineNumber>=0) && (lineNumber<this->grid->GetNumberOfLine()));
}

bool RuleDames::ColumnCoordinateIsValid(int columnNumber){
    return ((columnNumber>=0) && (columnNumber<this->grid->GetNumberOfColumn()));
}

bool RuleDames::CaseCoordinatesAreValid(int lineNumber, int columnNumber){
    return((this->LineCoordinateIsValid(lineNumber))&&(this->ColumnCoordinateIsValid(columnNumber)));
};

bool RuleDames::CaseBelongsToPlayer(int lineNumber, int columnNumber, const Player & player){
    if(!(this->grid->GetCase(lineNumber,columnNumber).playerOnCase==player.valueOfToken)){
        std::cout<<"The case doesn't belong to you"<<std::endl;
    }
    return (this->grid->GetCase(lineNumber,columnNumber).playerOnCase==player.valueOfToken);
};

bool RuleDames::PieceOnCaseCanMoveOrTake(int lineNumber, int columnNumber,const Player & player){
    return ((this->PawnCanTakeEnemyPiece(lineNumber,columnNumber))||(this->PawnCanMove(lineNumber,columnNumber,player)));
};

bool RuleDames::PieceIsBeingTaken(int lineNumber, int columnNumber){
    return this->grid->GetCase(lineNumber,columnNumber).playerOnCase==-1;
};

bool RuleDames::PawnCanMove(int lineNumber, int columnNumber, const Player & player){
    if(player.valueOfToken==1){
        return this->PawnCanMovePlayer1(lineNumber,columnNumber);
    }
    else if (player.valueOfToken==2){
        return this->PawnCanMovePlayer2(lineNumber,columnNumber);
    }
    return false;
};

bool RuleDames::PawnCanTakeEnemyPiece(int lineNumber, int columnNumber){
    return (  (this->PawnCanTakeEnemyPieceUpLeft(lineNumber,columnNumber))
            ||(this->PawnCanTakeEnemyPieceUpRight(lineNumber,columnNumber))
            ||(this->PawnCanTakeEnemyPieceDownLeft(lineNumber,columnNumber))
            ||(this->PawnCanTakeEnemyPieceDownRight(lineNumber,columnNumber))
           );
};

bool RuleDames::PawnCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber-2,columnNumber-2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber-1,columnNumber-1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber-1,columnNumber-1))
           ||(this->grid->GetCase(lineNumber-1,columnNumber-1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber-2,columnNumber-2).CaseIsEmpty());
};

bool RuleDames::PawnCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber-2,columnNumber+2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber-1,columnNumber+1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber-1,columnNumber+1))
           ||(this->grid->GetCase(lineNumber-1,columnNumber+1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber-2,columnNumber+2).CaseIsEmpty());
};
bool RuleDames::PawnCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber+2,columnNumber-2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber+1,columnNumber-1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber+1,columnNumber-1))
           ||(this->grid->GetCase(lineNumber+1,columnNumber-1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber+2,columnNumber-2).CaseIsEmpty());
};

bool RuleDames::PawnCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber){
    if(!(this->CaseCoordinatesAreValid(lineNumber+2,columnNumber+2))){
        return false;
    }
    return(!((this->grid->GetCase(lineNumber+1,columnNumber+1).CaseIsEmpty())
           ||(this->PieceIsBeingTaken(lineNumber+1,columnNumber+1))
           ||(this->grid->GetCase(lineNumber+1,columnNumber+1).playerOnCase==this->grid->GetCase(lineNumber,columnNumber).playerOnCase))
           &&
           this->grid->GetCase(lineNumber+2,columnNumber+2).CaseIsEmpty());
};


bool RuleDames::PawnCanMovePlayer1(int lineNumber, int columnNumber){
    if(!(this->PawnCanMoveUpLeft(lineNumber,columnNumber)||this->PawnCanMoveUpRight(lineNumber,columnNumber))){
        std::cout<<"You cannot move that piece"<<std::endl;
    }
    return (this->PawnCanMoveUpLeft(lineNumber,columnNumber)||this->PawnCanMoveUpRight(lineNumber,columnNumber));
};

bool RuleDames::PawnCanMovePlayer2(int lineNumber, int columnNumber){
    if(!(this->PawnCanMoveDownLeft(lineNumber,columnNumber)||this->PawnCanMoveDownRight(lineNumber,columnNumber))){
        std::cout<<"You cannot move that piece"<<std::endl;
    }
    return (this->PawnCanMoveDownLeft(lineNumber,columnNumber)||this->PawnCanMoveDownRight(lineNumber,columnNumber));
};

bool RuleDames::PawnCanMoveUpLeft(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber-1,columnNumber-1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber-1,columnNumber-1)));
    }
    return false;
};

bool RuleDames::PawnCanMoveUpRight(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber-1,columnNumber+1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber-1,columnNumber+1)));
    }
        return false;
}

bool RuleDames::PawnCanMoveDownLeft(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber+1,columnNumber-1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber+1,columnNumber-1)));
    }
    return false;
};

bool RuleDames::PawnCanMoveDownRight(int lineNumber, int columnNumber){
    if(this->CaseCoordinatesAreValid(lineNumber+1,columnNumber+1)){
        return (this->grid->CaseIsEmpty(this->grid->GetCase(lineNumber+1,columnNumber+1)));
    }
        return false;
}

bool RuleDames::PawnCanMoveLeft(int lineNumber, int columnNumber, const Player &player){
    if(player.valueOfToken==1){
        return this->PawnCanMoveUpLeft(lineNumber,columnNumber);
    }
    else if(player.valueOfToken==2){
        return this->PawnCanMoveDownLeft(lineNumber,columnNumber);
    }
    else{
        return false;
    }
}

bool RuleDames::PawnCanMoveRight(int lineNumber, int columnNumber, const Player &player){
    if(player.valueOfToken==1){
        return this->PawnCanMoveUpRight(lineNumber,columnNumber);
    }
    else if(player.valueOfToken==2){
        return this->PawnCanMoveDownRight(lineNumber,columnNumber);
    }
    else{
        return false;
    }
}


void RuleDames::PawnMoveLeft(int lineNumber, int columnNumber,const Player & player){
    if(player.valueOfToken==1){
        this->PawnMoveUpLeft(lineNumber,columnNumber,player);
    }
    if(player.valueOfToken==2){
        this->PawnMoveDownLeft(lineNumber,columnNumber,player);
    }
};
void RuleDames::PawnMoveRight(int lineNumber, int columnNumber,const Player & player){
    if(player.valueOfToken==1){
        this->PawnMoveUpRight(lineNumber,columnNumber,player);
    }
    if(player.valueOfToken==2){
        this->PawnMoveDownRight(lineNumber,columnNumber,player);
    }
};


void RuleDames::PawnMoveDownLeft(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+1,columnNumber-1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+1,columnNumber-1).playerOnCase=player.valueOfToken;
};

void RuleDames::PawnMoveDownRight(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+1,columnNumber+1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+1,columnNumber+1).playerOnCase=player.valueOfToken;
};

void RuleDames::PawnMoveUpLeft(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-1,columnNumber-1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-1,columnNumber-1).playerOnCase=player.valueOfToken;
};

void RuleDames::PawnMoveUpRight(int lineNumber, int columnNumber,const Player & player){
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-1,columnNumber+1).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-1,columnNumber+1).playerOnCase=player.valueOfToken;
};



void RuleDames::PawnTakeEnemyUpLeft(int & lineNumber, int  & columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceUpLeft(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-2,columnNumber-2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-2,columnNumber-2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber-1,columnNumber-1).playerOnCase=-1;
    lineNumber=lineNumber-2;
    columnNumber=columnNumber-2;
};

void RuleDames::PawnTakeEnemyUpRight(int & lineNumber, int & columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceUpRight(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber-2,columnNumber+2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber-2,columnNumber+2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber-1,columnNumber+1).playerOnCase=-1;
    lineNumber=lineNumber-2;
    columnNumber=columnNumber+2;
};

void RuleDames::PawnTakeEnemyDownLeft(int & lineNumber, int & columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceDownLeft(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+2,columnNumber-2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+2,columnNumber-2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber+1,columnNumber-1).playerOnCase=-1;
    lineNumber=lineNumber+2;
    columnNumber=columnNumber-2;
};
void RuleDames::PawnTakeEnemyDownRight(int &lineNumber, int& columnNumber,const Player &player){
    if(!this->PawnCanTakeEnemyPieceDownRight(lineNumber,columnNumber)){
        return;
    }
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineNumber+2,columnNumber+2).pieceType=EnumPiece::PAWN;
    this->grid->GetCase(lineNumber+2,columnNumber+2).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineNumber+1,columnNumber+1).playerOnCase=-1;
    lineNumber=lineNumber+2;
    columnNumber=columnNumber+2;
};


void RuleDames::PlayerChoosesACase(int & lineNumberToPlay, int & columnNumberToPlay,const Player & player){
    while(!this->ValidCaseChosenByPlayer(lineNumberToPlay,columnNumberToPlay,player)){
        this->grid->DisplayGrid();
        std::cout<<"Select the line of the piece you want to play, Player "<<player.valueOfToken<<std::endl;
        std::cin>>lineNumberToPlay;
        std::cout<<"Select the column of the piece you want to play, Player "<<player.valueOfToken<<std::endl;
        std::cin>>columnNumberToPlay;
        if(!this->LineCoordinateIsValid(lineNumberToPlay)){
            std::cout<<"Line is not valid"<<std::endl;
        }
        if(!this->ColumnCoordinateIsValid(columnNumberToPlay)){
            std::cout<<"Column is not valid"<<std::endl;
        }
    }
}

void RuleDames::PawnMovement(int lineNumberToPlay, int columnNumberToPlay,const Player & player){
    std::string directionToPlay="";
    bool movementMade=false;
    while(!movementMade){
        this->grid->DisplayGrid();
        std::cout<<"Do you want to move left or right ? R/L"<<std::endl;
        std::cin>>directionToPlay;
        if(directionToPlay.compare("R")==0&&this->PawnCanMoveRight(lineNumberToPlay,columnNumberToPlay,player)){
            this->PawnMoveRight(lineNumberToPlay,columnNumberToPlay,player);
            movementMade=true;
        }
        else if(directionToPlay.compare("L")==0&&this->PawnCanMoveLeft(lineNumberToPlay,columnNumberToPlay,player)){
            this->PawnMoveLeft(lineNumberToPlay,columnNumberToPlay,player);
            movementMade=true;
        }
        else{
            std::cout<<"Select a valid movement please"<<std::endl;
        }
    }
}

//A CHECKER

void RuleDames::QueenMovement(int lineNumberOfOrigin, int columnNumberOfOrigin,const Player & player){
    int lineCaseFinish=-1;
    int columnCaseFinish=-1;
    while(!(this->SelectValidDestinationCaseCoordonates(lineCaseFinish,columnCaseFinish)
            &&(lineNumberOfOrigin!=lineCaseFinish)
            &&(columnNumberOfOrigin!=columnCaseFinish)
            &&this->QueenPathIsClear(this->grid->GetCase(lineNumberOfOrigin,columnNumberOfOrigin),this->grid->GetCase(lineCaseFinish,columnCaseFinish)))){
        std::cout<<"Finish case is not valid"<<std::endl;
    }
    this->grid->GetCase(lineCaseFinish,columnCaseFinish).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineCaseFinish,columnCaseFinish).pieceType=EnumPiece::QUEEN;
    this->grid->GetCase(lineNumberOfOrigin,columnNumberOfOrigin).ResetDynamicCase();
}

bool RuleDames::SelectValidDestinationCaseCoordonates(int& lineCaseFinish, int& columnCaseFinish){
    this->grid->DisplayGrid();
    std::cout<<"Select the finish line number"<<std::endl;
    std::cin>>lineCaseFinish;
    std::cout<<"Select the finish column number"<<std::endl;
    std::cin>>columnCaseFinish;
    if(!this->CaseCoordinatesAreValid(lineCaseFinish,columnCaseFinish)){
        std::cout<<"Please input a destination case in the grid"<<std::endl;
    }
    return(this->CaseCoordinatesAreValid(lineCaseFinish,columnCaseFinish));
}


void RuleDames::PawnTakeEnemyPiece(int & lineNumber, int & columnNumber, const Player &player){
    int directionToTakePiece=0;
    bool takeMade=false;
    while(!takeMade){
        this->grid->DisplayGrid();
        std::cout<<"Where do you want to take a piece ?"<<std::endl;
        std::cout<<"Up-left(1)/Up-right(2)/Down-left(3)/Down-right(4)"<<std::endl;
        std::cin>>directionToTakePiece;
        switch (directionToTakePiece){
        case 1:
            this->PawnTakeEnemyUpLeft(lineNumber,columnNumber,player);
            takeMade=true;
            break;
        case 2:
            this->PawnTakeEnemyUpRight(lineNumber,columnNumber,player);
            takeMade=true;
            break;
        case 3:
            this->PawnTakeEnemyDownLeft(lineNumber,columnNumber,player);
            takeMade=true;
            break;
        case 4:
            this->PawnTakeEnemyDownRight(lineNumber,columnNumber,player);
            takeMade=true;
            break;
        default:
            std::cout<<"Please select a valid take"<<std::endl;
        }
    }
}

void RuleDames::RemovePiecesTaken(){
    for(int lineNumber=0;lineNumber<this->grid->GetNumberOfLine();++lineNumber){
        for(int columnNumber=0;columnNumber<this->grid->GetNumberOfColumn();++columnNumber){
            if(this->grid->GetCase(lineNumber,columnNumber).playerOnCase==-1){
                this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
            }
        }
    }
}

bool RuleDames::QueenCanMove(int lineNumber, int columnNumber){
    return(RuleDames::QueenCanMoveUpLeft(lineNumber,columnNumber)
           ||RuleDames::QueenCanMoveUpRight(lineNumber,columnNumber)
           ||RuleDames::QueenCanMoveDownLeft(lineNumber,columnNumber)
           ||RuleDames::QueenCanMoveDownRight(lineNumber,columnNumber));
}

bool RuleDames::QueenCanMoveUpLeft(int lineNumber,int columnNumber){
    return this->PawnCanMoveUpLeft(lineNumber,columnNumber);
}
bool RuleDames::QueenCanMoveUpRight(int lineNumber,int columnNumber){
    return this->PawnCanMoveUpRight(lineNumber,columnNumber);
}
bool RuleDames::QueenCanMoveDownLeft(int lineNumber,int columnNumber){
    return this->PawnCanMoveDownLeft(lineNumber,columnNumber);
}
bool RuleDames::QueenCanMoveDownRight(int lineNumber,int columnNumber){
    return this->PawnCanMoveDownRight(lineNumber,columnNumber);
}


bool RuleDames::QueenPathIsDiagonal(Case& CaseStart, Case& CaseFinish){
    int diffLine=abs(CaseStart.GetNumLine()-CaseFinish.GetNumLine());
    int diffColumn=abs(CaseStart.GetNumColumn()-CaseFinish.GetNumColumn());
    return(diffLine==diffColumn);
}

bool RuleDames::QueenPathGoesUp(Case& CaseStart, Case& CaseFinish){
    return(CaseFinish.GetNumLine()-CaseStart.GetNumLine()<0);
}

bool RuleDames::QueenPathGoesLeft(Case& CaseStart, Case& CaseFinish){
    return(CaseFinish.GetNumColumn()-CaseStart.GetNumColumn()<0);
}


bool RuleDames::QueenPathIsClear(Case &CaseStart, Case &CaseFinish){
    if(!QueenPathIsDiagonal(CaseStart,CaseFinish)){
        std::cout<<"Path is not diagonal"<<std::endl;
        return false;
    }
  bool pathGoesUp=this->QueenPathGoesUp(CaseStart,CaseFinish);
  bool pathGoesLeft=this->QueenPathGoesLeft(CaseStart,CaseFinish);
  if(pathGoesUp){
      if(pathGoesLeft){
          return this->QueenPathUpLeftIsClear(CaseStart,CaseFinish);
      }
      else{
          return this->QueenPathUpRightIsClear(CaseStart,CaseFinish);
      }
  }
  else{
      if(pathGoesLeft){
          return this->QueenPathDownLeftIsClear(CaseStart,CaseFinish);
      }
      else{
          return this->QueenPathDownRightIsClear(CaseStart,CaseFinish);
      }
  }
};

bool RuleDames::QueenPathUpLeftIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()-1;
    for(int lineToTest=CaseStart.GetNumLine()-1;lineToTest>=CaseFinish.GetNumLine();--lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            --columnToTest;
        }
    return true;
};

bool RuleDames::QueenPathUpRightIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()+1;
    for(int lineToTest=CaseStart.GetNumLine()-1;lineToTest>=CaseFinish.GetNumLine();--lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            ++columnToTest;
        }
    return true;
};

bool RuleDames::QueenPathDownLeftIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()-1;
    for(int lineToTest=CaseStart.GetNumLine()+1;lineToTest<=CaseFinish.GetNumLine();++lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            --columnToTest;
        }
    return true;
};
bool RuleDames::QueenPathDownRightIsClear(Case& CaseStart, Case& CaseFinish){
    int columnToTest=CaseStart.GetNumColumn()+1;
    for(int lineToTest=CaseStart.GetNumLine()+1;lineToTest<=CaseFinish.GetNumLine();++lineToTest){
            if(!this->grid->GetCase(lineToTest,columnToTest).CaseIsEmpty()){
                return false;
            }
            ++columnToTest;
        }
    return true;
};

void RuleDames::PawnTurn(int lineNumberToPlay, int columnNumberToPlay,const Player & player){
    int playerTakesPieces=false;
    if(this->PawnCanTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay)){
        playerTakesPieces=true;
    }
    while(this->PawnCanTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay)){
        this->PawnTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay,player);
    }
    if(!playerTakesPieces){
        this->PawnMovement(lineNumberToPlay,columnNumberToPlay, player);
    }
    else{
        this->RemovePiecesTaken();
    }
}

bool RuleDames::QueenCanTakeEnemyPiece(int lineNumber, int columnNumber){
    return (  (this->QueenCanTakeEnemyPieceUpLeft(lineNumber,columnNumber))
            ||(this->QueenCanTakeEnemyPieceUpRight(lineNumber,columnNumber))
            ||(this->QueenCanTakeEnemyPieceDownLeft(lineNumber,columnNumber))
            ||(this->QueenCanTakeEnemyPieceDownRight(lineNumber,columnNumber))
           );
}

bool RuleDames::QueenCanTakeEnemyPieceUpLeft(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest>=0;--lineToTest){
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
        if(this->PawnCanTakeEnemyPieceUpLeft(lineToTest,columnToTest)
                &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        --columnToTest;
    }
    return false;
}

bool RuleDames::QueenCanTakeEnemyPieceUpRight(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest>=0;--lineToTest){
        if(this->PawnCanTakeEnemyPieceUpRight(lineToTest,columnToTest)
                &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        ++columnToTest;
    }
    return false;
}

bool RuleDames::QueenCanTakeEnemyPieceDownLeft(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest<this->grid->GetNumberOfLine();++lineToTest){
        if(this->PawnCanTakeEnemyPieceDownLeft(lineToTest,columnToTest)
                &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        --columnToTest;
    }
    return false;
}

bool RuleDames::QueenCanTakeEnemyPieceDownRight(int lineNumber, int columnNumber){
    Case caseStart=Case();
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    Case caseFinish=Case();
    int columnToTest = columnNumber;
    for(int lineToTest=lineNumber;lineToTest<this->grid->GetNumberOfLine();++lineToTest){
        if(this->PawnCanTakeEnemyPieceDownRight(lineToTest,columnToTest)
          &&(this->QueenPathIsClear(caseStart,caseFinish)||lineNumber==lineToTest)){
            return true;
        }
        ++columnToTest;
    }
    return false;
}

void RuleDames::QueenTakesEnemyPiece(int & lineNumber, int & columnNumber,const Player & player){
    int directionToTakePiece=0;
    bool takeMade=false;
    while(!takeMade){
        this->grid->DisplayGrid();
        std::cout<<"Where do you want to take a piece ?"<<std::endl;
        std::cout<<"Up-left(1)/Up-right(2)/Down-left(3)/Down-right(4)"<<std::endl;
        std::cin>>directionToTakePiece;
        switch (directionToTakePiece){
        case 1:
            if(this->QueenCanTakeEnemyPieceUpLeft(lineNumber,columnNumber)){
                this->QueenTakesEnemyPieceUpLeft(lineNumber,columnNumber,player);
                takeMade=true;
            }
            break;
        case 2:
            if(this->QueenCanTakeEnemyPieceUpRight(lineNumber,columnNumber)){
                this->QueenTakesEnemyPieceUpRight(lineNumber,columnNumber,player);
                takeMade=true;
            }
            break;
        case 3:
            if(this->QueenCanTakeEnemyPieceDownLeft(lineNumber,columnNumber)){
                this->QueenTakesEnemyPieceDownLeft(lineNumber,columnNumber,player);
                takeMade=true;
            }
            break;
        case 4:
             if(this->QueenCanTakeEnemyPieceDownRight(lineNumber,columnNumber)){
                this->QueenTakesEnemyPieceDownRight(lineNumber,columnNumber,player);
            takeMade=true;
            }
            break;
        default:
            std::cout<<"Please select a valid take"<<std::endl;
        }
    }
}

void RuleDames::QueenTakesEnemyPieceUpLeft(int & lineNumber, int & columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber-1;
    int lineToTest=lineNumber-1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathUpLeftIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest-1;
        columnToTest=columnToTest-1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest+1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    //take
    this->grid->GetCase(lineToTest+1, columnToTest+1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest-1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    this->grid->DisplayGrid();

    int moveCont=-1;

    while(moveCont<0||moveCont>1){
        std::cout<<"Do you want to continue the movement O/1?"<<std::endl;
        std::cin>>moveCont;
        }
    if(moveCont==0){
        lineNumber=lineToTest-1;
        columnNumber=columnNumber-1;
        return;
    }
    else{
        int numberOfCaseForMovement;
        bool moveAfterJumpValid=false;
        while(!moveAfterJumpValid){
            std::cout<<"Input how many cases you want to move after the jump"<<std::endl;
            std::cin>>numberOfCaseForMovement;
            if(!this->CaseCoordinatesAreValid(lineToTest-1-numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement)){
                  std::cout<<"You're going out of the grid."<<std::endl;
            }
            else{
                Case caseIntermediate=Case();
                caseIntermediate=this->grid->GetCase(lineToTest-1,columnToTest-1);
                caseFinish=this->grid->GetCase(lineToTest-1-numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement);
                if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
                    &&this->QueenPathUpLeftIsClear(caseIntermediate,caseFinish)){
                    moveAfterJumpValid=true;
                }
            }
        }
        this->grid->GetCase(lineToTest-1, columnToTest-1).ResetDynamicCase();
        this->grid->GetCase(lineToTest-1-numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement).playerOnCase=player.valueOfToken;
        this->grid->GetCase(lineToTest-1-numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement).pieceType=EnumPiece::QUEEN;
        lineNumber=lineToTest-1-numberOfCaseForMovement;
        columnNumber=columnToTest-1-numberOfCaseForMovement;
    }
};

void RuleDames::QueenTakesEnemyPieceUpRight(int& lineNumber, int& columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber+1;
    int lineToTest=lineNumber-1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathUpRightIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest-1;
        columnToTest=columnToTest+1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest+1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    //take
    this->grid->GetCase(lineToTest+1, columnToTest-1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest-1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    this->grid->DisplayGrid();

    int moveCont=-1;

    while(moveCont<0||moveCont>1){
        std::cout<<"Do you want to continue the movement O/1?"<<std::endl;
        std::cin>>moveCont;
        }
    if(moveCont==0){
        lineNumber=lineToTest-1;
        columnNumber=columnToTest+1;
        return;
    }
    else{
        int numberOfCaseForMovement;
        bool moveAfterJumpValid=false;
        while(!moveAfterJumpValid){
            std::cout<<"Input how many cases you want to move after the jump"<<std::endl;
            std::cin>>numberOfCaseForMovement;
            if(!this->CaseCoordinatesAreValid(lineToTest-1-numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement)){
                  std::cout<<"You're going out of the grid."<<std::endl;
            }
            else{
                Case caseIntermediate=Case();
                caseIntermediate=this->grid->GetCase(lineToTest-1,columnToTest+1);
                caseFinish=this->grid->GetCase(lineToTest-1-numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement);
                if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
                    &&this->QueenPathUpRightIsClear(caseIntermediate,caseFinish)){
                    moveAfterJumpValid=true;
                }
            }
        }
        this->grid->GetCase(lineToTest-1, columnToTest+1).ResetDynamicCase();
        this->grid->GetCase(lineToTest-1-numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement).playerOnCase=player.valueOfToken;
        this->grid->GetCase(lineToTest-1-numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement).pieceType=EnumPiece::QUEEN;
        lineNumber=lineToTest-1-numberOfCaseForMovement;
        columnNumber=columnToTest+1+numberOfCaseForMovement;

    }
};

void RuleDames::QueenTakesEnemyPieceDownLeft(int & lineNumber, int & columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber-1;
    int lineToTest=lineNumber+1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathDownLeftIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest+1;
        columnToTest=columnToTest-1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest-1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    //take
    this->grid->GetCase(lineToTest-1, columnToTest+1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest+1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    this->grid->DisplayGrid();

    int moveCont=-1;

    while(moveCont<0||moveCont>1){
        std::cout<<"Do you want to continue the movement O/1?"<<std::endl;
        std::cin>>moveCont;
        }
    if(moveCont==0){
        lineNumber=lineToTest+1;
        columnNumber=columnToTest-1;
        return;
    }
    else{
        int numberOfCaseForMovement;
        bool moveAfterJumpValid=false;
        while(!moveAfterJumpValid){
            std::cout<<"Input how many cases you want to move after the jump"<<std::endl;
            std::cin>>numberOfCaseForMovement;
            if(!this->CaseCoordinatesAreValid(lineToTest+1+numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement)){
                  std::cout<<"You're going out of the grid."<<std::endl;
            }
            else{
                Case caseIntermediate=Case();
                caseIntermediate=this->grid->GetCase(lineToTest+1,columnToTest-1);
                caseFinish=this->grid->GetCase(lineToTest+1+numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement);
                if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
                    &&this->QueenPathDownLeftIsClear(caseIntermediate,caseFinish)){
                    moveAfterJumpValid=true;
                }
            }
        }
        this->grid->GetCase(lineToTest+1, columnToTest-1).ResetDynamicCase();
        this->grid->GetCase(lineToTest+1+numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement).playerOnCase=player.valueOfToken;
        this->grid->GetCase(lineToTest+1+numberOfCaseForMovement,columnToTest-1-numberOfCaseForMovement).pieceType=EnumPiece::QUEEN;
        lineNumber=lineToTest+1+numberOfCaseForMovement;
        columnNumber=columnToTest-1-numberOfCaseForMovement;
    }
};

void RuleDames::QueenTakesEnemyPieceDownRight(int & lineNumber, int & columnNumber,const Player & player){
    Case caseStart=Case();
    Case caseFinish=Case();
    int columnToTest=columnNumber+1;
    int lineToTest=lineNumber+1;
    caseStart=this->grid->GetCase(lineNumber,columnNumber);
    caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    while(this->QueenPathDownRightIsClear(caseStart,caseFinish)){
        lineToTest=lineToTest+1;
        columnToTest=columnToTest+1;
        caseFinish=this->grid->GetCase(lineToTest,columnToTest);
    }

    //first movement
    this->grid->GetCase(lineNumber,columnNumber).ResetDynamicCase();
    this->grid->GetCase(lineToTest-1,columnToTest-1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest-1,columnToTest-1).pieceType=EnumPiece::QUEEN;
    //take
    this->grid->GetCase(lineToTest-1, columnToTest-1).ResetDynamicCase();
    this->grid->GetCase(lineToTest,columnToTest).playerOnCase=-1;
    this->grid->GetCase(lineToTest,columnToTest).pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->grid->GetCase(lineToTest+1,columnToTest+1).playerOnCase=player.valueOfToken;
    this->grid->GetCase(lineToTest+1,columnToTest+1).pieceType=EnumPiece::QUEEN;
    this->grid->DisplayGrid();

    int moveCont=-1;

    while(moveCont<0||moveCont>1){
        std::cout<<"Do you want to continue the movement O/1?"<<std::endl;
        std::cin>>moveCont;
        }
    if(moveCont==0 || this->QueenCanTakeEnemyPiece(lineToTest+1,columnToTest+1)){
        lineNumber=lineToTest+1;
        columnNumber=columnToTest+1;
        return;
    }
    else{
        int numberOfCaseForMovement;
        bool moveAfterJumpValid=false;
        while(!moveAfterJumpValid){
            std::cout<<"Input how many cases you want to move after the jump"<<std::endl;
            std::cin>>numberOfCaseForMovement;
            if(!this->CaseCoordinatesAreValid(lineToTest+1+numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement)){
                  std::cout<<"You're going out of the grid."<<std::endl;
            }
            else{
            Case caseIntermediate=Case();
            caseIntermediate=this->grid->GetCase(lineToTest+1,columnToTest+1);
            caseFinish=this->grid->GetCase(lineToTest+1+numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement);
            if(this->CaseCoordinatesAreValid(caseFinish.GetNumLine(),caseFinish.GetNumColumn())
                &&this->QueenPathDownRightIsClear(caseIntermediate,caseFinish)){
                moveAfterJumpValid=true;
            }
           }
        }
        this->grid->GetCase(lineToTest+1, columnToTest+1).ResetDynamicCase();
        this->grid->GetCase(lineToTest+1+numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement).playerOnCase=player.valueOfToken;
        this->grid->GetCase(lineToTest+1+numberOfCaseForMovement,columnToTest+1+numberOfCaseForMovement).pieceType=EnumPiece::QUEEN;
        lineNumber=lineToTest+1+numberOfCaseForMovement;
        columnNumber=columnToTest+1+numberOfCaseForMovement;
    }
};

void RuleDames::QueenTurn(int lineNumberToPlay, int columnNumberToPlay,const Player & player){
    int playerTakesPieces=false;
    if(this->QueenCanTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay)){
        playerTakesPieces=true;
    }
    while(this->QueenCanTakeEnemyPiece(lineNumberToPlay,columnNumberToPlay)){
        this->QueenTakesEnemyPiece(lineNumberToPlay,columnNumberToPlay,player);
    }
    if(!playerTakesPieces){
        this->QueenMovement(lineNumberToPlay,columnNumberToPlay, player);
    }
    else{
        this->RemovePiecesTaken();
    }
}

void RuleDames::PlayerTurn(const Player & player){
    std::cout<<"Your turn Player "<<player.valueOfToken<<std::endl;
    int lineNumberToPlay=-1;
    int columnNumberToPlay=-1;
    this->PlayerChoosesACase(lineNumberToPlay,columnNumberToPlay,player);

    if(this->grid->GetCase(lineNumberToPlay,columnNumberToPlay).pieceType==EnumPiece::PAWN){
        this->PawnTurn(lineNumberToPlay,columnNumberToPlay,player);
    }
    else if(this->grid->GetCase(lineNumberToPlay,columnNumberToPlay).pieceType==EnumPiece::QUEEN){
        this->QueenTurn(lineNumberToPlay,columnNumberToPlay,player);
    };
    this->TransformPawnArrivedOnEndLine();
}

bool RuleDames::VictoryCondition(const Player & player){
    for(int lineNumber=0;lineNumber<this->grid->GetNumberOfLine();++lineNumber){
        for(int columnNumber=0;columnNumber<this->grid->GetNumberOfColumn();++columnNumber){
            if(!(this->grid->GetCase(lineNumber,columnNumber).CaseIsEmpty()||this->CaseBelongsToPlayer(lineNumber,columnNumber,player)))
                return false;
        }
    }
    return true;
};

//Impossible de faire match nul aux dames, sauf si abandon des deux joueurs.
bool RuleDames::NoWinnerEndCondition(){
    return false;
};

void RuleDames::GameFlow(){
    QFile loadFile(QStringLiteral("saveDame.json"));

    if (loadFile.open(QIODevice::ReadOnly)) {
        std::cout<<"Would you like to load your previous game? (1 for yes)"<<std::endl;
        int saveChoice;
        std::cin>>saveChoice;
        switch(saveChoice){
            case 1:
                LoadGame();
                break;
            default:
                this->SetupGame();
                break;
        }
    }
    else{
        this->SetupGame();
    }

//    this->SetupGame();
//    this->SetupTest();
    std::cout<<"Up-Left corner's coordinates are 0/0."<<std::endl;
    while(!this->NoWinnerEndCondition()){
        for(Player player:this->playerList){
            this->PlayerTurn(player);
            if(this->VictoryCondition(player)){
                std::cout<<"Player "<<player.valueOfToken<<" has won !"<<std::endl;
                this->grid->DisplayGrid();
                return;
            }
            if(this->NoWinnerEndCondition()){
                std::cout<<"Game Over, no contest"<<std::endl;
                this->grid->DisplayGrid();
                return;
            }
        }
        SaveGame();
    }
};

void RuleDames::LoadGame(){
    QFile loadFile(QStringLiteral("saveDame.json"));

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    Read(loadDoc.object());
}

void RuleDames::SaveGame() const{
    QFile saveFile(QStringLiteral("saveDame.json"));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonObject gameObject;
    Write(gameObject);
    QJsonDocument saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());
}

void RuleDames::Read(const QJsonObject &json){

    grid->ClearGrid();
    QJsonArray caseArray = json["cases"].toArray();
    for (int caseIndex = 0; caseIndex < caseArray.size(); ++caseIndex) {
        QJsonObject caseObject = caseArray[caseIndex].toObject();
        Case caseToRead;
        caseToRead.Read(caseObject);
        grid->SetCaseInGrid(caseToRead);
    }
}

void RuleDames::Write(QJsonObject &json) const{

    QJsonArray caseArray;
    for(std::vector<Case> listCases : grid->GetGrid()) {
        for(Case caseToWrite : listCases) {
                QJsonObject caseObject;
                caseToWrite.Write(caseObject);
                caseArray.append(caseObject);
            }
    }
    json["cases"] = caseArray;
}
