#include "ruleothello.h"

RuleOthello::RuleOthello()
{

}

std::unique_ptr<GridDynamic>& RuleOthello::GetGrid(){
    return this->grid;
}

void RuleOthello::SetupPlayer1(){
    Case& caseToSetup1=grid->GetCase(4,3);
    caseToSetup1.playerOnCase=1;
    Case& caseToSetup2=grid->GetCase(3,4);
    caseToSetup2.playerOnCase=1;
}
void RuleOthello::SetupPlayer2(){
    Case& caseToSetup1=grid->GetCase(3,3);
    caseToSetup1.playerOnCase=2;
    Case& caseToSetup2=grid->GetCase(4,4);
    caseToSetup2.playerOnCase=2;
}

void RuleOthello::Setup(){
    for(int numLine=0;numLine<grid->GetNumberOfLine();++numLine){
        for(int numColumn=0;numColumn<grid->GetNumberOfColumn();++numColumn){
            Case& caseToSetup=grid->GetCase(numLine,numColumn);
            caseToSetup.ResetDynamicCase();
        }
    }
    this->SetupPlayer1();
    this->SetupPlayer2();
}

void RuleOthello::PlayerTurn(const Player &player){
    if(PlayerCanPutValidTokenInGrid(player)){
        std::cout<<"Player "<<player.valueOfToken<<" can play"<<std::endl;
        PlayerPutsTokenInGrid(player);
    }
}
bool RuleOthello::PlayerCanPutValidTokenInGrid(const Player & player){
    for (int lineTest=0 ;lineTest<grid->GetNumberOfLine();lineTest++){
        for (int columnTest=0 ;columnTest<grid->GetNumberOfColumn();columnTest++){
            Case caseToCheck = grid->GetCase(lineTest,columnTest);
            if (PlayerCanPutValidTokenOnCase(player, caseToCheck)) {
                return true;
            }
        }
    }
return false;
};

bool RuleOthello::PlayerCanPutValidTokenOnCase(const Player & player, const Case & caseToCheck){
if(!caseToCheck.CaseIsEmpty()){
    return false;
}
else{
    return ValidMovePossibleOnLineOrColumnOrDiagonal(player, caseToCheck);
    };
}

bool RuleOthello::ValidMovePossibleOnLineOrColumnOrDiagonal(const Player &player, const Case &caseToCheck){
  return ValidMovePossibleOnLine(player, caseToCheck)||ValidMovePossibleOnColumn(player,caseToCheck)||ValidMovePossibleOnDiagonal(player, caseToCheck);
};

bool RuleOthello::ValidMovePossibleOnLine(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnLineToLeft(player, caseToCheck)||ValidMovePossibleOnLineToRight(player, caseToCheck);
}

bool RuleOthello::ValidMovePossibleOnLineToLeft(const Player &player, const Case &caseToPlay){
   int lineToCheck=caseToPlay.GetNumLine();
   int columnToCheck=caseToPlay.GetNumColumn();
   if(columnToCheck==0){
       return false;
   }
   columnToCheck=columnToCheck-1;
   Case & caseOnLeft=grid->GetCase(lineToCheck,columnToCheck);
   if(columnToCheck==0||caseOnLeft.CaseBelongsToPlayerValue(player.valueOfToken)||caseOnLeft.CaseIsEmpty()){
       return false;
   }
   for(columnToCheck=columnToCheck-1;columnToCheck>=0;--columnToCheck){
        Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
        if(caseToCheck.CaseIsEmpty()){
            return false;
        }
        else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
            return true;
        }
   }
    return false;
}

bool RuleOthello::ValidMovePossibleOnLineToRight(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(columnToCheck==grid->GetNumberOfColumn()-1){
        return false;
    }
    columnToCheck=columnToCheck+1;
    Case & caseOnRight=grid->GetCase(lineToCheck,columnToCheck);
    if(columnToCheck==0||caseOnRight.CaseBelongsToPlayerValue(player.valueOfToken)||caseOnRight.CaseIsEmpty()){
        return false;
    }
    for(columnToCheck=columnToCheck+1;columnToCheck<grid->GetNumberOfColumn();++columnToCheck){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
    }
     return false;
}

bool RuleOthello::ValidMovePossibleOnColumn(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnColumnDown(player, caseToCheck)||ValidMovePossibleOnColumnUp(player,caseToCheck);
}

bool RuleOthello::ValidMovePossibleOnColumnUp(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==0){
        return false;
    }
    lineToCheck=lineToCheck-1;
    Case & caseUp=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==0||caseUp.CaseBelongsToPlayerValue(player.valueOfToken)||caseUp.CaseIsEmpty()){
        return false;
    }
    for(lineToCheck=lineToCheck-1;lineToCheck>=0;--lineToCheck){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
    }
     return false;
}

bool RuleOthello::ValidMovePossibleOnColumnDown(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==grid->GetNumberOfLine()-1){
        return false;
    }
    lineToCheck=lineToCheck+1;
    Case & caseUp=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==lineToCheck+1||caseUp.CaseBelongsToPlayerValue(player.valueOfToken)||caseUp.CaseIsEmpty()){
        return false;
    }
    for(lineToCheck=lineToCheck+1;lineToCheck<grid->GetNumberOfLine();++lineToCheck){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
    }
     return false;
}

bool RuleOthello::ValidMovePossibleOnDiagonal(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnDiagonalULDR(player,caseToCheck)||ValidMovePossibleOnDiagonalURDL(player, caseToCheck);
}

bool RuleOthello::ValidMovePossibleOnDiagonalULDR(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnDiagonalToUpLeft(player, caseToCheck)||ValidMovePossibleOnDiagonalToDownRight(player,caseToCheck);
}

bool RuleOthello::ValidMovePossibleOnDiagonalURDL(const Player &player, const Case &caseToCheck){
    return ValidMovePossibleOnDiagonalToUpRight(player, caseToCheck)||ValidMovePossibleOnDiagonalToDownLeft(player, caseToCheck);
}

bool RuleOthello::ValidMovePossibleOnDiagonalToUpLeft(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==0||columnToCheck==0){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck-1;
    Case & caseUpLeft=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==0||columnToCheck==0||caseUpLeft.CaseBelongsToPlayerValue(player.valueOfToken)||caseUpLeft.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck-1;
    while(lineToCheck>=0 && columnToCheck>=0){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck-1;
             columnToCheck=columnToCheck-1;
         }
    }
     return false;
}

bool RuleOthello::ValidMovePossibleOnDiagonalToUpRight(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==0||columnToCheck==grid->GetNumberOfColumn()-1){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck+1;
    Case & caseUpRight=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==0||columnToCheck==grid->GetNumberOfColumn()-1||caseUpRight.CaseBelongsToPlayerValue(player.valueOfToken)||caseUpRight.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck-1;
    columnToCheck=columnToCheck+1;
    while(lineToCheck>=0 && columnToCheck<grid->GetNumberOfColumn()){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck-1;
             columnToCheck=columnToCheck+1;
         }
    }
     return false;
}

bool RuleOthello::ValidMovePossibleOnDiagonalToDownLeft(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==0){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck-1;
    Case & caseDownLeft=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==0||caseDownLeft.CaseBelongsToPlayerValue(player.valueOfToken)||caseDownLeft.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck-1;
    while(lineToCheck<grid->GetNumberOfLine() && columnToCheck>=0){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck+1;
             columnToCheck=columnToCheck-1;
         }
    }
     return false;
}

bool RuleOthello::ValidMovePossibleOnDiagonalToDownRight(const Player &player, const Case &caseToPlay){
    int lineToCheck=caseToPlay.GetNumLine();
    int columnToCheck=caseToPlay.GetNumColumn();
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==grid->GetNumberOfColumn()-1){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck+1;
    Case & caseDownRight=grid->GetCase(lineToCheck,columnToCheck);
    if(lineToCheck==grid->GetNumberOfLine()-1||columnToCheck==grid->GetNumberOfColumn()-1||caseDownRight.CaseBelongsToPlayerValue(player.valueOfToken)||caseDownRight.CaseIsEmpty()){
        return false;
    }
    lineToCheck=lineToCheck+1;
    columnToCheck=columnToCheck+1;
    while(lineToCheck<grid->GetNumberOfLine() && columnToCheck<grid->GetNumberOfColumn()){
         Case & caseToCheck=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToCheck.CaseIsEmpty()){
             return false;
         }
         else if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
             return true;
         }
         else{
             lineToCheck=lineToCheck+1;
             columnToCheck=columnToCheck+1;
         }
    }
     return false;
}

void RuleOthello::PlayerPutsTokenInGrid(const Player & player){
    bool validMove=false;
    int line;
    int column;
    while(!validMove){
        std::cout<<"Select the line"<<std::endl;
        std::cin>>line;
        std::cout<<"Select the column"<<std::endl;
        std::cin>>column;
        Case & caseToTest=grid->GetCase(line,column);
        validMove=PlayerCanPutValidTokenOnCase(player, caseToTest);
    }
    //mettre de jolis setters
    Case & caseToModify=grid->GetCase(line,column);
    FlipTokens(player, caseToModify);
    caseToModify.playerOnCase=player.valueOfToken;
}

void RuleOthello::FlipTokens(const Player &player, const Case &caseToCheck){
    if(ValidMovePossibleOnLineToLeft(player, caseToCheck)){
        FlipTokensToTheLeft(player, caseToCheck);
    }
    if(ValidMovePossibleOnLineToRight(player, caseToCheck)){
        FlipTokensToTheRight(player, caseToCheck);
    }
    if(ValidMovePossibleOnColumnUp(player, caseToCheck)){
        FlipTokensUp(player, caseToCheck);
    }
    if(ValidMovePossibleOnColumnDown(player, caseToCheck)){
        FlipTokensDown(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToUpLeft(player, caseToCheck)){
        FlipTokensToUpLeft(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToUpRight(player, caseToCheck)){
        FlipTokensToUpRight(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToDownLeft(player, caseToCheck)){
        FlipTokensToDownLeft(player, caseToCheck);
    }
    if(ValidMovePossibleOnDiagonalToDownRight(player, caseToCheck)){
        FlipTokensToDownRight(player, caseToCheck);
    }
}

void RuleOthello::FlipTokensToTheLeft(const Player & player, const Case & caseToCheck){
    int lineToCheck=caseToCheck.GetNumLine();
    for(int columnToCheck=caseToCheck.GetNumColumn()-1;columnToCheck>=0;--columnToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
         }
    }
     return;
};

//TODO
void RuleOthello::FlipTokensToTheRight(const Player & player, const Case & caseToCheck){
    int lineToCheck=caseToCheck.GetNumLine();
    for(int columnToCheck=caseToCheck.GetNumColumn()+1;columnToCheck<grid->GetNumberOfColumn();++columnToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
         }
    }
     return;
};
//TODO
void RuleOthello::FlipTokensUp(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn();
    for(int lineToCheck=caseToCheck.GetNumLine()-1;lineToCheck>=0;--lineToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
         }
    }
     return;
};
//TODO
void RuleOthello::FlipTokensDown(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn();
    for(int lineToCheck=caseToCheck.GetNumLine()+1;lineToCheck<grid->GetNumberOfLine();++lineToCheck){
         Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
         }
    }
     return;
};
//TODO
void RuleOthello::FlipTokensToUpLeft(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()-1;
    int lineToCheck=caseToCheck.GetNumLine()-1;
    while(lineToCheck>=0&&columnToCheck>=0){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             columnToCheck=columnToCheck-1;
             lineToCheck=lineToCheck-1;
         }
    }
     return;
};
//TODO
void RuleOthello::FlipTokensToUpRight(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()+1;
    int lineToCheck=caseToCheck.GetNumLine()-1;
    while(lineToCheck>=0&&columnToCheck<grid->GetNumberOfColumn()){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             columnToCheck=columnToCheck+1;
             lineToCheck=lineToCheck-1;
         }
    }
     return;
};
//TODO
void RuleOthello::FlipTokensToDownLeft(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()-1;
    int lineToCheck=caseToCheck.GetNumLine()+1;
    while(lineToCheck<grid->GetNumberOfLine()&&columnToCheck>=0){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             columnToCheck=columnToCheck-1;
             lineToCheck=lineToCheck+1;
         }
    }
     return;
};
//TODO
void RuleOthello::FlipTokensToDownRight(const Player & player, const Case & caseToCheck){
    int columnToCheck=caseToCheck.GetNumColumn()+1;
    int lineToCheck=caseToCheck.GetNumLine()+1;
    while(lineToCheck<grid->GetNumberOfLine()||columnToCheck<grid->GetNumberOfColumn()){
        Case & caseToModify=grid->GetCase(lineToCheck,columnToCheck);
         if(caseToModify.CaseBelongsToPlayerValue(player.valueOfToken)){
             return;
         }
         else{
             caseToModify.playerOnCase=player.valueOfToken;
             columnToCheck=columnToCheck+1;
             lineToCheck=lineToCheck+1;
         }
    }
     return;
};


bool RuleOthello::VictoryCondition(const Player &player){
    int numberOfCaseForPlayer=0;
    int numberOfCaseForOpponent=0;
    for(int lineNumber=0;lineNumber<grid->GetNumberOfLine();++lineNumber){
        for(int columnNumber=0;columnNumber<grid->GetNumberOfColumn();++columnNumber){
            Case & caseToCheck=grid->GetCase(lineNumber,columnNumber);
            if(caseToCheck.CaseBelongsToPlayerValue(player.valueOfToken)){
                numberOfCaseForPlayer=numberOfCaseForPlayer+1;
            }
            else if (caseToCheck.CaseBelongsToOpponent(player.valueOfToken)){
                numberOfCaseForOpponent=numberOfCaseForOpponent+1;
            }
        }
    }
    return numberOfCaseForPlayer>numberOfCaseForOpponent;
}

Player& RuleOthello::GetPlayer1(){
    return playerList[0];
}

Player& RuleOthello::GetPlayer2(){
    return playerList[1];
}

bool RuleOthello::EndOfTheGame(){
    return !(PlayerCanPutValidTokenInGrid(GetPlayer1())||PlayerCanPutValidTokenInGrid(GetPlayer2()));
}

bool RuleOthello::NoWinnerEndCondition(){
  return false;
};

void RuleOthello::EndGameManager(){
    std::cout<<"End of the game"<<std::endl;
    if(VictoryCondition(GetPlayer1())){
        std::cout<<"Player "<<GetPlayer1().valueOfToken<<" has won"<<std::endl;
    }
    else if (VictoryCondition(GetPlayer2())){
        std::cout<<"Player "<<GetPlayer2().valueOfToken<<" has won"<<std::endl;
    }
    else{
        std::cout<<"personne ne gagnexddddd"<<std::endl;
    }
    grid->DisplayGrid();
    return;
}

void RuleOthello::GameFlow(){
    QFile loadFile(QStringLiteral("saveOthello.json"));

    if (loadFile.open(QIODevice::ReadOnly)) {
        std::cout<<"Would you like to load your previous game? (1 for yes)"<<std::endl;
        int saveChoice;
        std::cin>>saveChoice;
        switch(saveChoice){
            case 1:
                LoadGame();
                break;
            default:
            Setup();
                break;
        }
    }
    else{
        Setup();
    }

    grid->DisplayGrid();

    bool endOfTheGame=false;
    while(!endOfTheGame){
        for(Player player:playerList){
            PlayerTurn(player);
            if(EndOfTheGame()){
                EndGameManager();
                return;
            }
            grid->DisplayGrid();
            SaveGame();
            endOfTheGame=EndOfTheGame();
        }
    }
}

void RuleOthello::LoadGame(){
    QFile loadFile(QStringLiteral("saveOthello.json"));

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    Read(loadDoc.object());
}

void RuleOthello::SaveGame() const{
    QFile saveFile(QStringLiteral("saveOthello.json"));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonObject gameObject;
    Write(gameObject);
    QJsonDocument saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());
}

void RuleOthello::Read(const QJsonObject &json){

    grid->ClearGrid();
    QJsonArray caseArray = json["cases"].toArray();
    for (int caseIndex = 0; caseIndex < caseArray.size(); ++caseIndex) {
        QJsonObject caseObject = caseArray[caseIndex].toObject();
        Case caseToRead;
        caseToRead.Read(caseObject);
        grid->SetCaseInGrid(caseToRead);
    }
}

void RuleOthello::Write(QJsonObject &json) const{

    QJsonArray caseArray;
    for(std::vector<Case> listCases : grid->GetGrid()) {
        for(Case caseToWrite : listCases) {
                QJsonObject caseObject;
                caseToWrite.Write(caseObject);
                caseArray.append(caseObject);
            }
    }
    json["cases"] = caseArray;
}
