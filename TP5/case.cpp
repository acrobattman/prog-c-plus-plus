#include "case.h"

Case::Case()
{
}

Case::Case(int _numLine, int _numColumn): numLine(_numLine), numColumn(_numColumn), playerOnCase(0), pieceType(EnumPiece::EMPTY)
{
}

Case::Case(int _numLine, int _numColumn, int _value, const EnumPiece& _pieceType): numLine(_numLine), numColumn(_numColumn), playerOnCase(_value), pieceType(_pieceType)
{
}

Case::~Case(){

}

bool Case::CaseIsEmpty() const
{
    return(this->playerOnCase==0);
    //return(this->pieceType==EnumPiece::EMPTY); Ne marche plus. Pourquoi ? Car PAWN et EMPTY sont égaux (caractère vide).
}

void Case::ResetCase(){
    this->pieceType=EnumPiece::EMPTY;
    this->playerOnCase=0;
}

void Case::ResetDynamicCase(){
    this->pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->playerOnCase=0;
}

bool Case::CaseBelongsToPlayerValue(int playerValue){
    return playerValue==playerOnCase;
}

bool Case::CaseBelongsToOpponent(int playerValue){
    return(!(CaseIsEmpty()||CaseBelongsToPlayerValue(playerValue)));
}

void Case::DisplayCase(){
    if(this->playerOnCase==-1){
        std::cout<<"X";
    }
    else{
        std::cout<<this->playerOnCase;
    }
    if(static_cast<char>(this->pieceType)!='X'){
       std::cout<<static_cast<char>(this->pieceType);
    }
}

int Case::GetNumColumn()const{
    return this->numColumn;
}

int Case::GetNumLine()const{
    return this->numLine;
}

void Case::Read(const QJsonObject &json){
    numLine = json["numLigne"].toInt() ;
    numColumn = json["numColonne"].toInt();
    playerOnCase = json["playerOnCase"].toInt();
    pieceType = QVariant::fromValue(json["pieceType"]).value<EnumPiece>();

}

void Case::Write(QJsonObject &json) const{
    json["numLigne"] = numLine;
    json["numColonne"] = numColumn;
    json["playerOnCase"] = playerOnCase;
    json["pieceType"] = QVariant::fromValue(pieceType).toInt();
}
