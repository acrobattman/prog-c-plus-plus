#include "mainmenuwidget.h"

MainMenuWidget::MainMenuWidget(QWidget* parentWidget)
{
    this->setParent(parentWidget);
    this->setFixedSize(parentWidget->size());

    m_lblTitle = new QLabel("Board Games", this);
    m_lblTitle->setFont(QFont("Arial", 48));
    m_lblTitle->move(200, 20);
    m_lblTitle->setFixedSize(480,60);

    m_btnGame = new QPushButton("Play !", this);
    m_btnGame->setFont(QFont("Arial", 24));
    m_btnGame->move(250, 180);
    m_btnGame->setFixedSize(300, 100);
    QObject::connect(m_btnGame, SIGNAL(clicked()), this->parent(), SLOT(GameSelection()));

    m_btnQuit = new QPushButton("Quit", this);
    m_btnQuit->setFont(QFont("Arial", 24));
    m_btnQuit->move(250, 330);
    m_btnQuit->setFixedSize(300, 100);
    QObject::connect(m_btnQuit, SIGNAL(clicked()), qApp, SLOT(quit()));
}
