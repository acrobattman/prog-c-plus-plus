# Système de savegarde

## Le principe

Les données d'un jeu sont sauvegardées dans un fichier json sous forme d'un tableau contenant les cases de la grille.

Chaque élément du tableau contient alors les informations qui sont propres à une case.

## La mise en place

Les classes de jeu, c'est-à-dire RuleDame, RuleMoprion et RulePuissance4 contiennent chacune les méthodes SaveGame() et LoadGame(), qui permettent soit de sauvegarder, soit de charger une partie selon un fichier qui est (pour l'instant) unique et différent pour chaque classes.

Ces 3 classes contiennent également la méthode Write() qui permet d'écrire les données de la grille dans le fichier json et la méthode Read() qui permet de les lire.

Ces méthodes appellent les autres méthodes Write() et Read() qui se situent dans la classe Case.

Write() convertit les cases de la grille en objets json, objet qui contient les attibuts de la case.

Read() convertit les objets json du fichier en cases, en affectant les valeurs du fichier en attibuts.