#ifndef DIAGONAL_H
#define DIAGONAL_H

#include <vector>
#include "case.h"

class Diagonal
{
public:
    Diagonal();
    Diagonal(int numLine, int numCol, std::vector<std::vector<Case>> grid);
    const std::vector<Case> getLeftDiagonal();
    const std::vector<Case> getRightDiagonal();

private:
    std::vector<Case> leftDiagonal;
    std::vector<Case> rightDiagonal;
};

#endif // DIAGONAL_H
