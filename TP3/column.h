#ifndef COLUMN_H
#define COLUMN_H

#include <vector>
#include "case.h"

class Column
{
public:
    Column();
    Column(int numCol, std::vector<std::vector<Case>> grid);
    const std::vector<Case> getColumn();

private:
    std::vector<Case> cases;
};

#endif // COLUMN_H
