TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.cpp \
        column.cpp \
        diagonal.cpp \
        game.cpp \
        grid.cpp \
        gridfactory.cpp \
        gridmorpion.cpp \
        gridpuissance4.cpp \
        line.cpp \
        main.cpp \
        player.cpp

HEADERS += \
    case.h \
    column.h \
    diagonal.h \
    game.h \
    grid.h \
    gridfactory.h \
    gridmorpion.h \
    gridpuissance4.h \
    line.h \
    player.h
