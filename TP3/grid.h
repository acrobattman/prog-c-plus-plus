#ifndef GRID_H
#define GRID_H

#include "iostream"
#include <vector>

#include "case.h"
#include "line.h"
#include "column.h"
#include "diagonal.h"
#include "player.h"

class Grid
{
public:
    std::vector<std::vector<Case>> getGrid();
    void setGrid(const Case &caseToSet);
    virtual int getNumberOfLine();
    virtual int getNumberOfColumn();
    bool caseIsEmpty(const Case &caseToTest);
    virtual void playAToken(const Player &player, const Case &caseToPlay) = 0;
    virtual bool lineIsWinning(Line &lineToTest, const Player &currentPlayer) = 0;
    virtual bool columnIsWinning(Column &columnToTest, const Player &currentPlayer) = 0;
    virtual bool diagonalWin(std::vector<Case> diagonalToTest, const Player &currentPlayer) = 0;
    virtual bool diagonalIsWinning(Diagonal &diagonalToTest, const Player &currentPlayer) = 0;

    bool victory(const Case &lastCasePlayed, const Player &currentPlayer);

    bool gridIsComplete();
    void displayGrid();
    void clearGrid();

protected:
    std::vector<std::vector<Case>> grid;
    int numberOfLine;
    int numberOfColumn;
};

#endif // GRID_H
