#include "line.h"

Line::Line()
{

}

Line::Line(int numLine, std::vector<std::vector<Case>> grid)
{
    // On cast la taille du vector car la valeur retourné est un long.
    int numberOfLine = static_cast<int>(grid[0].size());

    for (int i = 0; i < numberOfLine; i++)
    {
        cases.push_back(grid[numLine][i]);
    }
}

const std::vector<Case> Line::getLine()
{
    return cases;
}
