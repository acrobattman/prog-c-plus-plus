#ifndef GAME_H
#define GAME_H

#include "gridfactory.h"
#include "player.h"


class Game
{
public:
    Game(const GridFactory::GridGame &_gridGame, const Player &_firstPlayer, const Player &_secondPlayer);
    Player nextPlayer();
    int askWhereToPlay();
    void checkInput(int input);
    void playToken(bool* caseOk);
    void playAMove();
    bool testEndGame();
    void replay();

private:
    std::unique_ptr<Grid> game;
    GridFactory::GridGame gridGame;
    Player firstPlayer;
    Player secondPlayer;
    Player currentPlayer;
    Case currentCase;
};

#endif // GAME_H
