#include "grid.h"

std::vector<std::vector<Case>> Grid::getGrid(){
    return grid;
}

void Grid::setGrid(const Case &caseToSet){
    grid[caseToSet.numLine][caseToSet.numColumn] = caseToSet;
}

int Grid::getNumberOfLine(){
    return numberOfLine;
}

int Grid::getNumberOfColumn(){
    return numberOfColumn;
}

bool Grid::caseIsEmpty(const Case &caseToTest){
    return grid[caseToTest.numLine][caseToTest.numColumn].value == 0;
}

bool Grid::victory(const Case &lastCasePlayed, const Player &currentPlayer){
    Line currentLine = Line(lastCasePlayed.numLine, grid);
    Column currentColumn = Column(lastCasePlayed.numColumn, grid);
    Diagonal currentDiagonals = Diagonal(lastCasePlayed.numLine, lastCasePlayed.numColumn, grid);

    return lineIsWinning(currentLine, currentPlayer) || (columnIsWinning(currentColumn, currentPlayer)) || (diagonalIsWinning(currentDiagonals, currentPlayer));
}

bool Grid::gridIsComplete(){
    // On cast la taille du vector car la valeur retourné est un long.
    int numberOfLine = static_cast<int>(grid.size());
    int numberOfColumn = static_cast<int>(grid[0].size());

    for(int i = 0; i < numberOfLine; i++){
        for(int j = 0; j < numberOfColumn; j++){
            if(grid[i][j].value == 0)
            {
                return false;
            }
        }
    }
    return true;
}

void Grid::displayGrid(){
    // On cast la taille du vector car la valeur retourné est un long.
    int numberOfLine = static_cast<int>(grid.size());
    int numberOfColumn = static_cast<int>(grid[0].size());

    for(int i = 0; i < numberOfLine; i++){
            std::cout << "----------------------" << std::endl;
        for(int j = 0; j < numberOfColumn; j++){
            std::cout << grid[i][j].value << "|";
        }
        std::cout << std::endl;
    }
}

void Grid::clearGrid(){
    // On cast la taille du vector car la valeur retourné est un long.
    int numberOfLine = static_cast<int>(grid.size());
    int numberOfColumn = static_cast<int>(grid[0].size());

    for(int i = 0; i < numberOfLine; i++){
        for(int j = 0; j < numberOfColumn; j++){
            grid[i][j].value = 0;
        }
    }

    displayGrid();
}
