#include "gridpuissance4.h"

GridPuissance4::GridPuissance4()
{
    numberOfLine = 4;
    numberOfColumn = 7;

    for (int i = 0; i < numberOfLine; i++)
    {
        std::vector<Case> lineN;
        for (int j = 0; j < numberOfColumn; j++)
        {
            lineN.push_back(Case(i, j));
        }
        grid.push_back(lineN);
    }
}

void GridPuissance4::playAToken(const Player &player, const Case &caseToPlay){
    Column columnToPlay = Column(caseToPlay.numColumn,  grid);

    std::vector<Case> columnValues = columnToPlay.getColumn();

    // On cast la taille du vector car la valeur retourné est un long.
    int size = static_cast<int>(columnValues.size());

    for(int i = size - 1; i >= 0; i--){
        if(columnValues[i].value == 0){
            grid[i][caseToPlay.numColumn].value = player.valueOfToken;
            break;
        }
    }
}

bool GridPuissance4::lineIsWinning(Line &lineToTest, const Player &currentPlayer){
    std::vector<Case> lineValues = lineToTest.getLine();

    int size = static_cast<int>(lineToTest.getLine().size());

    int count = 1;
    for(int i=0; i < size; i++){
      if(lineValues[i].value != 0 && (lineValues[i].value == lineValues[i+1].value && lineValues[i].value == currentPlayer.valueOfToken)){
         count++;
      }
      else{
          count=1;
      }
      if(count == 4){
          return true;
      }
    }
    return false;
}

bool GridPuissance4::columnIsWinning(Column &columnToTest, const Player &currentPlayer){
    // On cast la taille du vector car la valeur retourné est un long.
    int size = static_cast<int>(columnToTest.getColumn().size());

    // i < size - 1 car sinon on sortira du tableau pour le dernier test du if.
    for (int i = 0; i < size; i++)
    {
        if (columnToTest.getColumn()[i].value == 0 || (columnToTest.getColumn()[i].value != currentPlayer.valueOfToken)){

            return false;
        }
    }
    return true;
}

bool GridPuissance4::diagonalWin(std::vector<Case> diagonalToTest, const Player &currentPlayer){
    int size = static_cast<int>(diagonalToTest.size());

    int count = 1;
    for(int i=0; i < size; i++){
      if(diagonalToTest[i].value != 0 && (diagonalToTest[i].value == diagonalToTest[i+1].value && diagonalToTest[i].value == currentPlayer.valueOfToken)){
         count++;
      }
      else{
          count=1;
      }
      if(count == 4){
          return true;
      }
    }
    return false;
}

bool GridPuissance4::diagonalIsWinning(Diagonal &diagonalToTest, const Player &currentPlayer){
    std::vector<Case> leftDiagonal = diagonalToTest.getLeftDiagonal();
    std::vector<Case> rightDiagonal = diagonalToTest.getRightDiagonal();

    // i < size - 1 car sinon on sortira du tableau pour le dernier test du if.
    int sizeLeftDiagonal = static_cast<int>(leftDiagonal.size());
    int sizeRightDiagonal = static_cast<int>(rightDiagonal.size());

    return ((sizeLeftDiagonal >= 4 && diagonalWin(leftDiagonal, currentPlayer)) || (sizeRightDiagonal >= 4 && diagonalWin(rightDiagonal, currentPlayer)));

}
