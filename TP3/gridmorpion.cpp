#include "gridmorpion.h"

GridMorpion::GridMorpion()
{
    numberOfLine = 3;
    numberOfColumn = 3;

    for (int i = 0; i < numberOfLine; i++)
    {
        std::vector<Case> lineN;
        for (int j = 0; j < numberOfColumn; j++)
        {
            lineN.push_back(Case(i, j));
        }
        grid.push_back(lineN);
    }
}

void GridMorpion::playAToken(const Player &player, const Case &caseToPlay)
{
    grid[caseToPlay.numLine][caseToPlay.numColumn].value = player.valueOfToken;
}

bool GridMorpion::lineIsWinning(Line &lineToTest, const Player &currentPlayer){
    // On cast la taille du vector car la valeur retourné est un long.
    int size = static_cast<int>(lineToTest.getLine().size());

    // i < size - 1 car sinon on sortira du tableau pour le dernier test du if.
    for (int i = 0; i < size; i++)
    {
        if (lineToTest.getLine()[i].value == 0 || (lineToTest.getLine()[i].value != currentPlayer.valueOfToken))
        {
            return false;
        }
    }
    return true;
}

bool GridMorpion::columnIsWinning(Column &columnToTest, const Player &currentPlayer){
    // On cast la taille du vector car la valeur retourné est un long.
    int size = static_cast<int>(columnToTest.getColumn().size());

    // i < size - 1 car sinon on sortira du tableau pour le dernier test du if.
    for (int i = 0; i < size; i++)
    {
        if (columnToTest.getColumn()[i].value == 0 || (columnToTest.getColumn()[i].value != currentPlayer.valueOfToken))
        {

            return false;
        }
    }
    return true;
}

bool GridMorpion::diagonalWin(std::vector<Case> diagonalToTest, const Player &currentPlayer){
    for (int i = 0; i < 3; i++)
    {
        if (diagonalToTest[i].value == 0 || diagonalToTest[i].value != currentPlayer.valueOfToken)
        {

            return false;
        }
    }
    return true;
}

bool GridMorpion::diagonalIsWinning(Diagonal &diagonalToTest, const Player &currentPlayer){

    std::vector<Case> leftDiagonal = diagonalToTest.getLeftDiagonal();
    std::vector<Case> rightDiagonal = diagonalToTest.getRightDiagonal();

    // i < size - 1 car sinon on sortira du tableau pour le dernier test du if.
    int sizeLeftDiagonal = static_cast<int>(leftDiagonal.size());
    int sizeRightDiagonal = static_cast<int>(rightDiagonal.size());

    return ((sizeLeftDiagonal == 3 && diagonalWin(leftDiagonal, currentPlayer)) || (sizeRightDiagonal == 3 && diagonalWin(rightDiagonal, currentPlayer)));
}
