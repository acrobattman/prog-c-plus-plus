#ifndef LINE_H
#define LINE_H

#include <vector>
#include "case.h"

class Line
{
public:
    Line();
    Line(int numLine, std::vector<std::vector<Case>> grid);
    const std::vector<Case> getLine();

private:
    std::vector<Case> cases;
};

#endif // LINE_H
