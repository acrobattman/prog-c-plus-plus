#include "gridfactory.h"

std::unique_ptr<Grid> GridFactory::createGrid(GridGame gridGame){
    switch (gridGame) {
        case Morpion:
            return std::make_unique<GridMorpion>();
        case Puissance4:
            return std::make_unique<GridPuissance4>();
    }
    throw "invalid game type !";
}
