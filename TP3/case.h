#ifndef CASE_H
#define CASE_H

struct Case{
    int numLine;
    int numColumn;
    int value;

    Case();

    Case(int _numLine, int _numColumn);

    Case(int _numLine, int _numColumn, int _value);
};

#endif // CASE_H
