#ifndef GRIDPUISSANCE4_H
#define GRIDPUISSANCE4_H

#include "iostream"
#include <vector>

#include "case.h"
#include "grid.h"

class GridPuissance4 : public Grid
{
public:
    GridPuissance4();
    void playAToken(const Player &player, const Case &caseToPlay) override;
    bool lineIsWinning(Line &lineToTest, const Player &currentPlayer) override;
    bool columnIsWinning(Column &columnToTest, const Player &currentPlayer) override;
    bool diagonalWin(std::vector<Case> diagonalToTest, const Player &currentPlayer) override;
    bool diagonalIsWinning(Diagonal &diagonalToTest, const Player &currentPlayer) override;
};

#endif // GRIDPUISSANCE4_H
