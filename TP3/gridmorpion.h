#ifndef GRIDMORPION_H
#define GRIDMORPION_H

#include "iostream"
#include <vector>

#include "case.h"
#include "grid.h"


class GridMorpion : public Grid
{
public:
    GridMorpion();
    void playAToken(const Player &player, const Case &caseToPlay) override;
    bool lineIsWinning(Line &lineToTest, const Player &currentPlayer) override;
    bool columnIsWinning(Column &columnToTest, const Player &currentPlayer) override;
    bool diagonalWin(std::vector<Case> diagonalToTest, const Player &currentPlayer) override;
    bool diagonalIsWinning(Diagonal &diagonalToTest, const Player &currentPlayer) override;
};

#endif // GRIDMORPION_H
