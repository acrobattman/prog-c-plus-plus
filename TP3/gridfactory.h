#ifndef GRIDFACTORY_H
#define GRIDFACTORY_H

#include "iostream"
#include "grid.h"
#include "gridmorpion.h"
#include "gridpuissance4.h"

class GridFactory
{
public:
    enum GridGame {
        Morpion,
        Puissance4
    };

    static std::unique_ptr<Grid> createGrid(GridGame gridGame);
};

#endif // GRIDFACTORY_H
