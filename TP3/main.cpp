#include <iostream>

#include "gridfactory.h"
#include "grid.h"
#include "game.h"

using namespace std;

int main()
{

    string nameFirstPlayer;
    string nameSecondPlayer;

    cout << "Joueur 1 entre ton nom : ";
    cin >> nameFirstPlayer;

    Player firstPlayer = Player(nameFirstPlayer, 1);

    cout << "Joueur 2 entre ton nom : ";
    cin >> nameSecondPlayer;

    Player secondPlayer = Player(nameSecondPlayer, 2);

    Player currentPlayer = firstPlayer;

    cout << "Choissez un mot de jeu :" << std::endl;
    cout << "Morpion : 1" << std::endl;
    cout << "Puissace4 : 2" << std::endl;
    cout << "Votre choix : ";

    int choice;

    while ( !( cin >> choice ) || choice <= 0 || choice > 2 )  // On test si la valeur saissit est ok et on répéte temps que pas valide !
    {
      std::cin.clear();
      std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
      std::cout << "Vous devez entrer un nombre entre 1 et 2 correspondant au jeu souhaité ! Veuillez-saissir une nouvelle valeur ! : ";
    }

    GridFactory::GridGame gameChoosen;

    switch (choice) {
    case 1:
        gameChoosen = GridFactory::Morpion;
        break;
    case 2:
        gameChoosen = GridFactory::Puissance4;
        break;
    }

    Game party = Game(gameChoosen, firstPlayer, secondPlayer);

    bool replay = false;

    do{

        do{
            party.playAMove();
        }while(!party.testEndGame());

        std::cout << "Souhaitez-vous rejouer ? 1 : oui ou 2 : non " << std::endl;

        int wantToReplay;

        while ( !( cin >> wantToReplay ) || wantToReplay <= 0 || wantToReplay > 2 )  // On test si la valeur saissit est ok et on répéte temps que pas valide !
        {
          std::cin.clear();
          std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
          std::cout << "Vous devez entrer un nombre entre 1 et 2 ! Veuillez-saissir une nouvelle valeur ! : ";
        }

        if(wantToReplay == 1){
            replay = true;
            party.replay();
        }
        else{
            replay = false;
        }

    }while(replay);
}
