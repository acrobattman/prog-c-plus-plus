#include "iostream"
#include "diagonal.h"

Diagonal::Diagonal()
{

}

Diagonal::Diagonal(int numLine, int numColumn, std::vector<std::vector<Case>> grid)
{
    // On cast le nombre de ligne du vector car la valeur retourné est un long.
    int numberOfLine = static_cast<int>(grid.size());
    int numberOfColumn = static_cast<int>(grid[0].size());

    std::vector<Case> gridIn1D;

    for(int i = 0; i < numberOfLine; i++){
        for(int j = 0; j < numberOfColumn; j++){
            gridIn1D.push_back(grid[i][j]);
        }
    }

    // Index line et col
    int index = numLine * numberOfColumn + numColumn;

    int numberOfLineGrid1D = index / numberOfColumn;
    int numberOfColumnGrid1D = index % numberOfColumn;

    for(int line = numberOfLineGrid1D - std::min(numberOfLineGrid1D, numberOfColumnGrid1D), column = numberOfColumnGrid1D - std::min(numberOfLineGrid1D, numberOfColumnGrid1D); line < numberOfLine && column < numberOfColumn; line++, column++){
        leftDiagonal.push_back(gridIn1D[line* numberOfColumn +column]);
    }

    for(int line = numberOfLineGrid1D - std::min(numberOfLineGrid1D, numberOfColumn - numberOfColumnGrid1D - 1 ), column = numberOfColumnGrid1D + std::min(numberOfLineGrid1D, numberOfColumn - numberOfColumnGrid1D - 1); line < numberOfLine && 0 <= column; line++, column--){
        rightDiagonal.push_back(gridIn1D[line* numberOfColumn +column]);
    }
}

const std::vector<Case> Diagonal::getLeftDiagonal(){
    return leftDiagonal;
}

const std::vector<Case> Diagonal::getRightDiagonal(){
    return rightDiagonal;
}
