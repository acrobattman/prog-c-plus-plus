#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>

struct Player
{
    std::string name;
    int valueOfToken;

    Player(std::string _name, int _valueOfToken);
};

#endif // PLAYER_H
