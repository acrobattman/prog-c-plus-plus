#include "column.h"
#include <iostream>

Column::Column()
{

}

Column::Column(int numCol, std::vector<std::vector<Case>> grid)
{
    // On cast le nombre de ligne du vector car la valeur retourné est un long.
    int numberOfLine = static_cast<int>(grid.size());

    for(int i = 0; i < numberOfLine; i++){
        cases.push_back(grid[i][numCol]);
    }
}

const std::vector<Case> Column::getColumn(){
    return cases;
}
