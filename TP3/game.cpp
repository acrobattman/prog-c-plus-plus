#include "game.h"

Game::Game(const GridFactory::GridGame &_gridGame, const Player &_firstPlayer, const Player &_secondPlayer) : gridGame(_gridGame), firstPlayer(_firstPlayer), secondPlayer(_secondPlayer), currentPlayer(_firstPlayer)
{
    game = GridFactory::createGrid(gridGame);
    game->displayGrid();
}

Player Game::nextPlayer(){
    if(currentPlayer.name == firstPlayer.name){
        return secondPlayer;
    }
    else{
        return firstPlayer;
    }
}

int Game::askWhereToPlay(){
    int input;
    switch (gridGame) {
    case GridFactory::Morpion:
        std::cout << "Quelle est la case dans laquelle tu veux jouer ? : ";
    case GridFactory::Puissance4:
        std::cout << "Quelle est la colonne dans laquelle tu veux jouer ? : ";
    }

    while ( !( std::cin >> input ) || input < 0 )  // On test si la valeur saissit est ok et on répéte temps que pas valide !
    {
      std::cin.clear();
      std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
      std::cout << "Vous devez entrer un nombre positif ! Veuillez-saissir une nouvelle valeur ! : ";
    }
    return input;
}

void Game::playToken(bool* caseOk)
{
    if(game->caseIsEmpty(currentCase)){
        *caseOk = true;
        game->playAToken(currentPlayer, currentCase);
        game->displayGrid();
    }
    else{
        std::cout << "La case choissit est déjâ remplit ! Veuillez-saissir une nouvelle valeur ! : ";
    }
}

void Game::checkInput(int input){
    if(gridGame == GridFactory::Morpion && input > 0 && input < (game->getNumberOfLine() * game->getNumberOfColumn() + 1)){
        int numLine = (input - 1 ) / game->getNumberOfLine();
        int numColumn = (input - 1) % game->getNumberOfColumn();
        currentCase = Case(numLine, numColumn);

    }
    else if(gridGame == GridFactory::Puissance4 && input > 0 && input < (game->getNumberOfColumn() + 1)){
        Column column = Column(input - 1, game->getGrid());
        std::vector<Case> columnValue = column.getColumn();
        int size = static_cast<int>(columnValue.size());

        int indexLine = 0;
        for(int i = size - 1; i >= 0; i--){
            if(columnValue[i].value == 0){
                indexLine = i;
                break;
            }
        }
        currentCase = Case(indexLine, input -1);
    }
    else{
        std::cout << "La case choissit n'est pas dans la grille ! Veuillez-saissir une nouvelle valeur !" << std::endl;
    }
}

void Game::playAMove(){
    bool caseOk = false;

    do{
        int whereToPlay = askWhereToPlay();

        checkInput(whereToPlay);

        playToken(&caseOk);

    }while(!caseOk);
}

bool Game::testEndGame(){
    if(game->victory(currentCase, currentPlayer)){
        std::cout << "Félication à toi, " << currentPlayer.name << " pour ta victoire !" << std::endl;
        return true;
    }
    else if(game->gridIsComplete()){
        std::cout << "La grille est complète égalité !" << std::endl;
        return true;
    }
    currentPlayer = nextPlayer();
    return false;

}

void Game::replay(){
    game->clearGrid();
}
