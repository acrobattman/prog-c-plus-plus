# C++ (ECKSTEIN Maxime)

Lien du google Doc de justification du choix de projet pour le Projet Groupé

https://docs.google.com/document/d/1YdntMHrJ82tm6SH2DW97MMhxW06CEe8sspi1DcysDbI/edit


## TP1


Le rendu du TP1 ce trouve dans le dossier TP1.

Ce dossier comporte 3 dossiers :
* Manipulation-de-nombre: Ce dossier correspond à la partie I du premier TP.
* Jeu-tennis: Ce dossier correspond à la partie II du premier TP.
* Chaine-caractere: Ce dernier dossier correspond à la III partie du premier TP.

Pour tester les différentes fonctions écritent il suffit de ce déplacer dans un des trois dossiers cité plus haut et de lancer la commande : **make**.

Une fois ceci fait il ne restera plus qu'à lancer la commande **./main** et le programme ce lancera.
