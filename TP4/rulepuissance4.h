#ifndef RULEPUISSANCE4_H
#define RULEPUISSANCE4_H
#include "rule.h"


class RulePuissance4 : public Rule
{
public:
    RulePuissance4();
    void PlayerTurn(const Player& player);
    bool VictoryCondition(const Player& player);
    bool NoWinnerEndCondition();
    void GameFlow();
    bool LineVictory(const Player& player);
    bool ColumnVictory(const Player& player);
    bool DiagonalVictory(const Player& player);
    bool DiagonalULDRVictory(const Player& player);
    bool DiagonalURDLVictory(const Player& player);
    void PlayerPutsToken(const Player& player);
    int FirstLineOfEmptyCaseInColumn(int columnNumber);

    std::unique_ptr<GridStatic> grid = GridStaticFactory::createGrid(GridStaticFactory::Puissance4);
};

#endif // RULEPUISSANCE4_H
