#include "gridpuissance4.h"
#include <memory>

GridPuissance4::GridPuissance4():GridStatic(this->numberOfLine=7, this->numberOfColumn=7)
{

}

bool GridPuissance4::CheckIfANumberOfTokensAreAligned(CaseVector caseVectorToTest ,const Player &currentPlayer){
    int numberOfCasesAligned=0;
    for(Case caseToTest:caseVectorToTest.GetCases()){
        if(caseToTest.playerOnCase==currentPlayer.valueOfToken){
            numberOfCasesAligned=numberOfCasesAligned+1;
        }
        else{
            numberOfCasesAligned=0;
        }
        if(numberOfCasesAligned==this->numberOfAlignedCasesToWin){
            return true;
        }
    }
    return false;
}

bool GridPuissance4::LineCompletedByPlayer(int lineNumber, const Player &currentPlayer){
    Line lineToTest = this->CreateLine(lineNumber);
    return this->CheckIfANumberOfTokensAreAligned(lineToTest,currentPlayer);
}


bool GridPuissance4::ColumnCompletedByPlayer(int columnNumber, const Player &currentPlayer){
    Column columnToTest = this->CreateColumn(columnNumber);
    return this->CheckIfANumberOfTokensAreAligned(columnToTest,currentPlayer);
}

bool GridPuissance4::DiagonalULDRCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer){
    DiagonalULDR diagonalULDRToTest=this->CreateDiagonalULDR(lineNumber,columnNumber);
    return this->CheckIfANumberOfTokensAreAligned(diagonalULDRToTest, currentPlayer);
}

bool GridPuissance4::DiagonalURDLCompletedByPlayer(int lineNumber, int columnNumber, const Player &currentPlayer){
    DiagonalURDL diagonalURDLToTest=this->CreateDiagonalURDL(lineNumber,columnNumber);
    return this->CheckIfANumberOfTokensAreAligned(diagonalURDLToTest, currentPlayer);
}

