#include "case.h"

Case::Case()
{
}

Case::Case(int _numLine, int _numColumn): numLine(_numLine), numColumn(_numColumn), playerOnCase(0), pieceType(EnumPiece::EMPTY)
{
}

Case::Case(int _numLine, int _numColumn, int _value, const EnumPiece& _pieceType): numLine(_numLine), numColumn(_numColumn), playerOnCase(_value), pieceType(_pieceType)
{
}

Case::~Case(){

}

bool Case::CaseIsEmpty() const
{
    return(this->playerOnCase==0);
    //return(this->pieceType==EnumPiece::EMPTY); Ne marche plus. Pourquoi ? Car PAWN et EMPTY sont égaux (caractère vide).
}

void Case::ResetCase(){
    this->pieceType=EnumPiece::EMPTY;
    this->playerOnCase=0;
}

void Case::ResetDynamicCase(){
    this->pieceType=EnumPiece::EMPTYSPACEDISPLAY;
    this->playerOnCase=0;
}

void Case::DisplayCase(){
    if(this->playerOnCase==-1){
        std::cout<<"X";
    }
    else{
        std::cout<<this->playerOnCase;
    }
    if(static_cast<char>(this->pieceType)!='X'){
       std::cout<<static_cast<char>(this->pieceType);
    }
}

int Case::GetNumColumn(){
    return this->numColumn;
}

int Case::GetNumLine(){
    return this->numLine;
}
