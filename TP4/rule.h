#ifndef RULE_H
#define RULE_H
#include "player.h"
#include "vector"
#include "grid.h"
#include "gridstaticfactory.h"
#include "griddynamicfactory.h"
#include <string>


class Rule
{
public:
    Rule();
    virtual void PlayerTurn(const Player & player)=0;
    virtual bool VictoryCondition(const Player & player)=0;
    virtual bool NoWinnerEndCondition()=0;
    virtual void GameFlow()=0;
    void AddPlayerToPlayerList(Player& playerToAdd);
    bool Rematch();

protected:
    std::vector<Player> playerList;
};

#endif // RULE_H
