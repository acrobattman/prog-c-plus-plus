#ifndef LINE_H
#define LINE_H
#include "casevector.h"

class Line : public CaseVector
{
public:
    Line(CaseVector& casevector);
};

#endif // LINE_H
