#ifndef DIAGONALURDL_H
#define DIAGONALURDL_H
#include "casevector.h"

class DiagonalURDL : public CaseVector
{
public:
    DiagonalURDL(CaseVector& casevector);
};

#endif // DIAGONALURDL_H
