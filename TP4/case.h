#ifndef CASE_H
#define CASE_H

#include "enumPiece.h"
#include <stdio.h>
#include <iostream>

class Case{

protected:
    int numLine;
    int numColumn;

public:

    int playerOnCase;
    EnumPiece pieceType;

    Case();

    Case(int _numLine, int _numColumn);

    Case(int _numLine, int _numColumn, int _playerOnCase,const EnumPiece& _pieceType);

    ~Case();

    bool CaseIsEmpty() const;

    int GetNumLine();

    int GetNumColumn();

    EnumPiece& GetPieceType();

    void ResetCase();

    void ResetDynamicCase();

    void DisplayCase();

};

#endif // CASE_H

