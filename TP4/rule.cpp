#include "rule.h"

Rule::Rule()
{
    Player player1 = Player("joueur1", 1);
    Player player2 = Player("joueur2",2);
    this->AddPlayerToPlayerList(player1);
    this->AddPlayerToPlayerList(player2);
}

void Rule::AddPlayerToPlayerList(Player &playerToAdd){
    this->playerList.push_back(playerToAdd);
}

bool Rule::Rematch(){
    std::cout<<"Game Over : the grid is full"<<std::endl;
    std::cout<<"Would you like a rematch ? Y/N"<<std::endl;
    std::string rematch;
    std::cin>>rematch;
    return(rematch.compare("Y")||rematch.compare("y"));
}
