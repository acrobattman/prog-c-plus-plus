#ifndef CASEVECTOR_H
#define CASEVECTOR_H

#include <vector>
#include "case.h"
#include "player.h"

class CaseVector
{
public:
        CaseVector();
        void AddCaseToVector(const Case& caseToAdd);
        const std::vector<Case> GetCases();
        int GetLength();
        bool IsFull();
        bool IsEmpty();
        bool IsCompletedByPlayer(const Player & player);
protected:
    std::vector<Case> cases;
};

#endif // CASEVECTOR_H
