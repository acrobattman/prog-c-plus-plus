#include "griddynamicfactory.h"

std::unique_ptr<GridDynamic> GridDynamicFactory::createGrid(GridGame gridGame){
    switch (gridGame) {
        case Dame:
            return std::make_unique<GridDame>();
    }
    throw "invalid game type !";
}
