#ifndef ENUMPIECE_H
#define ENUMPIECE_H

enum class EnumPiece : char {
    EMPTY= 'X',
    EMPTYSPACEDISPLAY= '\0',
    TOKEN = 'X',
    PAWN= 'p',
    QUEEN = 'Q',
};

#endif // ENUMPIECE_H
