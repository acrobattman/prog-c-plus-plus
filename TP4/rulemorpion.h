#ifndef RULEMORPION_H
#define RULEMORPION_H
#include "gridmorpion.h"
#include "rule.h"
#include <memory>


class RuleMorpion : public Rule
{
public:
    RuleMorpion();
    void PlayerTurn(const Player& player);
    bool VictoryCondition(const Player& player);
    bool NoWinnerEndCondition();
    void GameFlow();
    bool LineVictory(const Player& player);
    bool ColumnVictory(const Player& player);
    bool DiagonalVictory(const Player& player);
    bool DiagonalULDRVictory(const Player& player);
    bool DiagonalURDLVictory(const Player& player);
    void PlayerPutsToken(const Player& player);

    std::unique_ptr<GridStatic> grid = GridStaticFactory::createGrid(GridStaticFactory::Morpion);
};

#endif // RULEMORPION_H
