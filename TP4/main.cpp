#include <iostream>
#include "gridfactory.h"
#include "gridstaticfactory.h"
#include "rulemorpion.h"
#include "rulepuissance4.h"
#include "ruledames.h"

using namespace std;

int main()
{
    std::cout<<"Select your game"<<std::endl<<"1 Morpion"<<std::endl<<"2 Puissance 4"<<std::endl<<"3 Checkers"<<std::endl;
    int gameChoice;
    std::cin>>gameChoice;
    if(!std::cin){
        std::cout<<"Please enter a valid number"<<std::endl;
        std::cin.clear();
        std::cin.ignore(INT_MAX, '\n');
    }
    switch (gameChoice) {
    case 1: {
        std::cout<<"Morpion"<<std::endl;
        RuleMorpion jeu2morpion = RuleMorpion();
        jeu2morpion.GameFlow();
        break;}
    case 2:{
        std::cout<<"Puissance 4"<<std::endl;
        RulePuissance4 jeu2Puissance = RulePuissance4();
        jeu2Puissance.GameFlow();
        break;}
    case 3:{
        std::cout<<"Checkers"<<std::endl;
        RuleDames jeu2Dames= RuleDames();
        jeu2Dames.GameFlow();
        break; }
    default:{
        std::cout<<"Default Checkers"<<std::endl;
        RuleDames jeu2Dames= RuleDames();
        jeu2Dames.GameFlow();
        }
    }
    return 0;
}

