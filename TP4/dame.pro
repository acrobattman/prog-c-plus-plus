TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.cpp \
        casevector.cpp \
        column.cpp \
        diagonaluldr.cpp \
        diagonalurdl.cpp \
        grid.cpp \
        griddame.cpp \
        griddynamic.cpp \
        griddynamicfactory.cpp \
        gridmorpion.cpp \
        gridpuissance4.cpp \
        gridstatic.cpp \
        gridstaticfactory.cpp \
        line.cpp \
        main.cpp \
        player.cpp \
        rule.cpp \
        ruledames.cpp \
        rulemorpion.cpp \
        rulepuissance4.cpp

HEADERS += \
    case.h \
    casevector.h \
    column.h \
    diagonaluldr.h \
    diagonalurdl.h \
    enumPiece.h \
    grid.h \
    griddame.h \
    griddynamic.h \
    griddynamicfactory.h \
    gridmorpion.h \
    gridpuissance4.h \
    gridstatic.h \
    gridstaticfactory.h \
    line.h \
    player.h \
    rule.h \
    ruledames.h \
    rulemorpion.h \
    rulepuissance4.h
