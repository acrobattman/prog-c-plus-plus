#include "grid.h"
#include <memory>

Grid::Grid(int numberOfLine, int numberOfColumn)
{
    this->numberOfLine = numberOfLine;
    this->numColumn = numColumn;

    for(int lineNum=0; lineNum<numberOfLine;++lineNum){
        std::vector<Case> lineN;
                for (int colNum = 0; colNum < numberOfColumn; colNum++)
                {
                    lineN.push_back(Case(lineNum, colNum));
                }
                grid.push_back(lineN);
}
}

void Grid::SetNumberOfLine(int numberOfLine){
    this->numberOfLine=numberOfLine;
};

void Grid::SetNumberOfColumn(int numberOfColumn){
    this->numberOfColumn=numberOfColumn;
};

const std::vector<std::vector<Case>> Grid::GetGrid(){
    return this->grid;
}

int Grid::GetNumberOfColumn() const{
    return this->numberOfColumn;
}

int Grid::GetNumberOfLine() const{
    return this->numberOfLine;
}

Case& Grid::GetCase(int numLine, int numColumn){
   return this->grid[numLine][numColumn];
}

void Grid::SetCaseInGrid(Case &caseToSet){
    int numColumnToSet = caseToSet.GetNumColumn();
    int numLineToSet = caseToSet.GetNumLine();
    this->grid[numLineToSet][numColumnToSet]=caseToSet;
}

bool Grid::CaseIsEmpty(const Case &caseToTest){
    return caseToTest.CaseIsEmpty();
}

void Grid::DisplayGrid(){
    for(int lineNumber=0;lineNumber<this->numberOfLine;++lineNumber){
        for(int columnNumber=0;columnNumber<this->numberOfColumn;columnNumber++){
            std::cout<<" ";
            this->GetCase(lineNumber, columnNumber).DisplayCase();
            std::cout<<" ";
        }
        std::cout<<std::endl;
    }
}


void Grid::ClearGrid(){
    for(int lineNumber=0;lineNumber<this->numberOfLine;++lineNumber){
        for(int columnNumber=0;columnNumber<this->numberOfColumn;columnNumber++){
            this->GetCase(lineNumber, columnNumber).ResetCase();
        }
    }
}
