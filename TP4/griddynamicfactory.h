#ifndef GRIDDYNAMICFACTORY_H
#define GRIDDYNAMICFACTORY_H

#include "iostream"
#include <memory>
#include "griddame.h"
#include "griddynamic.h"

class GridDynamicFactory
{
public:
    enum GridGame {
        Dame
    };

   static std::unique_ptr<GridDynamic>createGrid(GridGame gridGame);
};

#endif // GRIDDYNAMICFACTORY_H
