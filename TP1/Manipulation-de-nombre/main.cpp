/**
 * @file main.cpp
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 19 Septembre 2020
 * 
 * Partie I - Manipulation de nombre
 * 
 **/

#include <iostream>
#include "manipNum.h"

int main()
{
    std::cout << "Choissisez une option du menu : " << std::endl
              << "0 : Faire la somme de deux entiers." << std::endl
              << "1 : Inverser la valeur de deux entiers." << std::endl
              << "2 : Remplacer la valeur du troisième paramètre par la somme des deux premiers (avec pointeur)." << std::endl
              << "3 : Remplace la valeur du troisième paramètre par la somme des deux premiers (avec référence)." << std::endl
              << "4 : Chosir la taille du tableau et tri croissant ou décroissant puis tri inverse du tableau." << std::endl
              << "Votre choix : ";

    int i;
    std::cin >> i;

    // On mets des accolade autour de des cas où on déclare des variables sinon on a une erreur lié au scope
    switch (i)
    {
    case 0:
        std::cout << "La somme de 2 et 4 est : " << sum(2, 4) << std::endl;
        break;
    case 1:
    {
        int a = 6;
        int b = 10;

        std::cout << "Avant inversion a = " << a << " est b = " << b << std::endl;
        swap(a, b);
        std::cout << "Après inversion a = " << a << " est b = " << b << std::endl;
        break;
    }
    case 2:
    {
        int a = 10;
        int b = 40;
        int c = 100;
        int *pc = &c;

        std::cout << "Valeur de départ de c : " << c << std::endl;
        swapSumPointer(a, b, pc);
        std::cout << "Valeur final de c : " << c << std::endl;
        break;
    }
    case 3:
    {
        int a = 20;
        int b = 40;
        int c = 100;

        std::cout << "Valeur de départ de c : " << c << std::endl;
        swapSumRef(a, b, c);
        std::cout << "Valeur final de c : " << c << std::endl;
        break;
    }
    case 4:
        sortArrayInverse();
        break;
    default:
        std::cout << "L'option choissit n'est pas valide !";
        break;
    }

    return 0;
}