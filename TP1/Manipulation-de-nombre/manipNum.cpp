/**
 * @file manipNum.cpp
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 26 Septembre 2020
 * 
 * Partie I - Manipulation de nombre
 * 
 **/

#include <iostream>
#include <ctime>

/**
 * @brief Fonction qui calcule la somme de deux entiers.
 * @param a un entier.
 * @param b un entier.
 * @return la somme de a et b.
 **/
int sum(const int a, const int b)
{
    return a + b;
}

/**
 * @brief Fonction qui inverse les valeurs de deux entiers.
 * @param a la référence d'un entier.
 * @param b la référence d'un entier.
 **/
void swap(int &a, int &b)
{
    int temp = b;
    b = a;
    a = temp;
}

/**
 * @brief Fonction qui remplace la valeur du 3eme paramètre par la somme des deux premiers (avec pointeur).
 * @param a un entier.
 * @param b un entier. 
 * @param c un pointeur sur un entier.
 **/
void swapSumPointer(const int a, const int b, int *c)
{
    *c = a + b;
}

// V2 Avec référence
/**
 * @brief Fonction qui remplace la valeur du 3eme paramètre par la somme des deux premiers (avec référence).
 * @param a un entier.
 * @param b un entier. 
 * @param c la référence d'un entier.
 **/
void swapSumRef(const int a, const int b, int &c)
{
    c = a + b;
}

/**
 * @brief Fonction qui crée un tableau d'entier aléatoirement.
 * @details Le langage ne permettant apparemment pas de retourner un tableau on retourne un pointeur sur un entier qui
 * correspond à l'adresse à laquelle va ce situer la première cas tableau généré. On aurait également pu simplement passer un tableau
 * en paramètre et le remplir.
 * @param size la taille du tableau.
 * @return tab un pointeur sur un entier qui correspond à l'adresse où ce trouve la premier case du tableau.
 **/
int *createRandomArray(const int size)
{

    // Création du tableau.
    int *tab = new int[size];

    // Mise en place de la graine aléatoire.
    std::srand((unsigned)time(NULL));

    for (int i = 0; i < size; i++)
    {
        // Génération d'un entier entre 0 et 100 000.
        // Comme on commence à 0 le 0 pourrait être omis.
        tab[i] = 0 + (rand() % (100000 + 1));
    }

    return tab;
}

/**
 * @brief Fonction qui affiche les valeurs contenues dans un tableau.
 * @param tab pointeur sur un entier qui correspond à l'adresse où ce trouve la premier case du tableau.
 * @param size taille du tableau.
 **/
void printArray(const int *tab, const int size)
{
    for (int i = 0; i < size; i++)
    {
        std::cout << "tab[" << i << "] : " << *(tab + i) << std::endl;
    }
}

/**
 * @brief  Fonction qui tri un tableau dans l'ordre croissant.
 * @param tab pointeur sur un entier qui correspond à l'adresse où ce trouve la premier case du tableau.
 * @param size taille du tableau.
 **/
void sortArrayASC(int *tab, const int size)
{
    int index = 0;

    // Booléen qui va permettre d'arrêter la boucle while une fois le tableau trié.
    bool permutation = true;

    while (permutation)
    {
        permutation = false;
        for (int i = 0; i < size - index - 1; i++)
        {
            if (tab[i] > tab[i + 1])
            {
                swap(tab[i], tab[i + 1]);
                permutation = true;
            }
        }
        index++;
    }
}

/**
 * @brief Fonction qui tri un tableau dans l'ordre décroissant.
 * @param tab pointeur sur un entier qui correspond à l'adresse où ce trouve la premier case du tableau.
 * @param size taille du tableau.
 **/
void sortArrayDSC(int *tab, const int size)
{
    int index = 0;
    bool permutation = true;

    // Booléen qui va permettre d'arreter la boucle while une fois le tableau trié
    while (permutation)
    {
        permutation = false;
        for (int i = 0; i < size - index - 1; i++)
        {
            if (tab[i] < tab[i + 1])
            {
                swap(tab[i], tab[i + 1]);
                permutation = true;
            }
        }
        index++;
    }
}

/**
 * @brief Fonction qui fait le tri inverse d'un tableau
 * @details demande à l'utilisateur la taille du tableau, le type de tri à effectuer puis réalise le tri inverse.
 **/
void sortArrayInverse()
{
    std::cout << "Entrez la taille du tableau : ";
    int size;
    std::cin >> size;

    std::cout << "Souhaitez-vous trier le tableau dans l'ordre croissant ou décroissant ? (1 ou 2) : ";
    int sort;
    std::cin >> sort;

    int *p;

    p = createRandomArray(size);

    std::cout << "Tableau avant le tri" << std::endl;
    printArray(p, size);

    std::cout << "Tableau après le tri" << std::endl;

    // Choix du tri croissant (1) ou décroissant (2)
    if (sort == 1)
    {
        sortArrayASC(p, size);
    }
    else if (sort == 2)
    {
        sortArrayDSC(p, size);
    }

    printArray(p, size);

    std::cout << "Tri inverse du tableau" << std::endl;

    for (int i = 0; i < size / 2; i++)
    {
        swap(p[i], p[size - 1 - i]);
    }

    printArray(p, size);
}