/**
 * @file manipNum.h
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 19 Septembre 2020
 * 
 * Partie I - Manipulation de nombre
 * 
 **/

#ifndef manipNum_h
#define manipNum_h

int sum(const int a, const int b);
void swap(int &a, int &b);
void swapSumPointer(const int a, const int b, int *c);
void swapSumRef(const int a, const int b, int &c);
int *createRandomArray(const int size);
void printArray(const int *tab, const int size);
void sortArrayASC(int *tab, const int size);
void sortArrayDSC(int *tab, const int size);
void sortArrayInverse();

#endif /* manipNum_h */