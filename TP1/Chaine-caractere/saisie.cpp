/**
 * @file saisie.cpp
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 20 Septembre 2020
 * 
 * Partie III - Inscription dans la console et récupération des saisies.
 * 
 **/

#include <iostream>
#include <math.h>

/**
 * @brief Salut l'utilisateur.
 * @details Le nom et prénom de l'utilisateur sont saisi en une seule saisi. Quel que soit le format
 * de saisi de l’utilisateur, seule la première lettre du prénom est en majuscule, toutes les lettres du nom
 * de famille sont mises en majuscules.
 **/
void hello()
{
    std::cout << "What is your name ?" << std::endl;

    std::string name;
    // GetLine permet de récupérer toute la ligne saisie et non juste le premier string
    //(le problème viens du fait qu'on sépare le nom et le prènom par un espace).
    std::getline(std::cin, name);

    std::cout << "Hello ";

    size_t i = 0;
    bool firstWord = true;

    // Boucle exécuté temps qu'on est sur le nom.
    while (firstWord)
    {
        // Si on arrive à l'espace on change la valeur de firstWord pour arrêter la boucle et
        // on met la premier lettre du prénom en majuscule.
        if (name[i] == ' ')
        {
            firstWord = false;
            putchar(' ');
            putchar(toupper(name[i + 1]));
            // On incrémente de deux sinon on affiche deux fois la 1er lettre du prénom.
            i += 2;
        }
        else
        {
            putchar(toupper(name[i]));
            i++;
        }
    }

    // Boucle sur le prénom à partir de la deuxieme lettre
    for (size_t j = i; j < name.length(); j++)
    {
        putchar(tolower(name[j]));
    }

    std::cout << " !" << std::endl;
}

/**
 * @brief Affiche un message à l'écran.
 * @param message le message à afficher.
 **/
void printMessage(std::string message)
{
    std::cout << message << std::endl;
}

/**
 * @brief Génère un nombre aléatoire entre 0 et 1000.
 * @return Le nombre généré aléatoirement.
 **/
int randomNumber()
{
    // Mise en place de la graine aléatoire.
    std::srand((unsigned)time(NULL));

    return rand() % (1000 + 1);
}

/**
 * @brief L'utilisateur saisi un nombre et on lui dit si le nombre saisi est plus grand ou plus petit que le sien.
 **/
void isGreaterOrLowerV1()
{
    std::cout << "Veillez saisir un nombre (Entre 0 et 1000): ";
    int number;
    std::cin >> number;

    int random = randomNumber();

    number<random ? printMessage("Saisie < aléa") : number> random ? printMessage("Saisie > aléa!")
                                                                   : printMessage("Saisie = aléa");
}

/**
 * @brief L'utilisateur doit trouver un nombre généré aléatoirement.
 **/
void isGreaterOrLowerV2()
{
    // Génération du nombre aléatoire à trouver.
    int random = randomNumber();

    // Booleen pour sortir de la boucle while une fois le nombre aléatoire trouvé.
    bool found = false;

    // Compteur pour compter le nombre de coup nécessaire pour trouver le nombre aléatoire.
    int compteur = 0;

    // Boucle exécuté temps que le nombre aléatoire n'a pas était trouvé.
    while (!found)
    {
        std::cout << "Veillez saisir un nombre (Entre 0 et 1000): ";
        int number;
        std::cin >> number;

        if (number == random)
        {
            found = true;
            std::string message = "Félication vous avez trouvez le nombre généré aléatoirement en " + std::to_string(compteur) + " coups !";
            printMessage(message);
        }
        else if (number < random)
        {
            printMessage("Le nombre aléatoire est plus grand ! ");
            compteur += 1;
        }
        else if (number > random)
        {
            printMessage("Le nombre aléatoire est plus petit !");
            compteur += 1;
        }
        else
        {
            // J'ai fait le choix de ne pas incrémenter dans ce cas ce qui est discutable.
            printMessage("Le caractère saisi n'est pas un nombre !");
        }
    }
}

/**
 * @brief L'utilisateur choissit un nombre entre 0 et 1000 et la machine doit le trouver.
 **/
void isGreaterOrLowerV3()
{
    std::cout << "Veillez saisir un nombre (Entre 0 et 1000): ";
    int number;
    std::cin >> number;

    int min = 0;
    int max = 1000;
    // max + 1 si jamais le nombre saisi est le maximum (1000)
    int top = max + 1;
    bool found = false;

    while (!found)
    {
        int guess = (min + top) / 2;

        if (guess == number)
        {
            found = true;
            std::string message = "Félication vous avez trouvez le nombre généré aléatoirement : " + std::to_string(number);
            printMessage(message);
        }
        else if (guess < number)
        {
            printMessage("Le nombre proposé : " + std::to_string(guess));
            printMessage("Le nombre aléatoire est plus grand ! ");
            min = guess;
        }
        else
        {
            printMessage("Le nombre proposé : " + std::to_string(guess));
            printMessage("Le nombre aléatoire est plus petit !");
            top = guess;
        }
    }
}