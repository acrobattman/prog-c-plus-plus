/**
 * @file saisie.h
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 20 Septembre 2020
 * 
 * Partie III - Inscription dans la console et récupération des saisies.
 * 
 **/

#ifndef _saisie_h
#define _saisie_h

void hello();
void printMessage(std::string message);
int randomNumber();
void isGreaterOrLowerV1();
void isGreaterOrLowerV2();
void isGreaterOrLowerV3();

#endif /* saisie_h */