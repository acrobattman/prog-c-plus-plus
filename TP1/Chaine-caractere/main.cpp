#include <iostream>
#include "saisie.h"

int main()
{
    std::cout << "Choissisez une option du menu : " << std::endl
              << "0 : Saluer l'utilisateur." << std::endl
              << "1 : Is greater or lower V1." << std::endl
              << "2 : Is greater or lower V2." << std::endl
              << "3 : Is greater or lower V3." << std::endl
              << "Votre choix : ";

    //std::string input;
    //std::getline(std::cin, i);
    //std::getline(std::cin, input);

    //int i =

    int i;
    std::cin >> i;

    // Permet d'éviter que le programme ne face n'importe si on choisit le cas 0 à cause du getLine dans hello().
    // Sans cette ligne le nom n'est pas demandé est la console affiche n'importe quoi.
    std::cin.ignore();

    switch (i)
    {
    case 0:
        hello();
        break;
    case 1:
        isGreaterOrLowerV1();
        break;
    case 2:
        isGreaterOrLowerV2();
        break;
    case 3:
        isGreaterOrLowerV3();
        break;
    default:
        std::cout << "L'option choissit n'est pas valide !";
        break;
    }
    return 0;
}