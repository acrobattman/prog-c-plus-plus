/**
 * @file tennis.h
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 19 Septembre 2020
 * 
 * Partie II - Jeu de tennis
 * 
 **/

#ifndef _tennis_h
#define _tennis_h

void match();
int point(int g);

#endif /* tennis_h */