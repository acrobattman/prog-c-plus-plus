/**
 * @file tennis.cpp
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 26 Septembre 2020
 * 
 * Partie II - Jeu de tennis
 * 
 **/

#include <iostream>
#include <math.h>

/**
 * @brief Fonction qui calcule le nombre de point(s) d'un joueur.
 * @details J'ai fait le choix de mettre le nombre de point à 0 si l'utilisateur rentre des valeurs négatives.
 * @param g le nombre d'échange(s) gagné par le joueur.
 * @return le nombre de point(s) du joueur.
 **/
int point(int g)
{
    return g == 1 ? 15 : g == 2 ? 30 : g >= 3 ? 40 : 0;
}

/**
 * @brief Fonction qui recalcul le nombre de point(s) d'un joueur
 * @param n pointeur sur le nombre d'échange(s) gagné.
 * @param pn pointeur sur le nombre de point(s).
 **/
void nextEchange(int *n, int *pn)
{
    *n += 1;

    *pn = point(*n);
}

/**
 * @brief Fonction qui détermine le gagnant d'un échange et recalcul ses points.
 * @param pta pointeur sur le nombre d'échange(s) gagné par le joueur A.
 * @param ptpa pointeur sur le nombre de point(s) du joueur A.
 * @param ptb pointeur sur le nombre d'échange(s) gagné par le joueur B.
 * @param ptpb pointeur sur le nombre de point(s) du joueur B.
 */
void echangeWinner(int *pta, int *ptpa, int *ptb, int *ptpb)
{
    std::cout << "Qui gagne le prochain échange A ou B ? Tapez 0 pour A et 1 pour B : ";
    int gagnant;
    std::cin >> gagnant;

    if (gagnant == 0)
    {
        nextEchange(pta, ptpa);
    }
    else if (gagnant == 1)
    {
        nextEchange(ptb, ptpb);
    }
    else
    {
        std::cout << "Erreur ni A ni B n'a gagné l'échange" << std::endl;
    }
}

/**
 * @brief Fonction qui simule un match de tennis. 
 */
void match()
{
    std::cout << "Entrer le nombre d'échange remporté par le joueur A : ";

    int a;
    std::cin >> a;

    std::cout << "Entrer le nombre d'échange remporté par le joueur B : ";

    int b;
    std::cin >> b;

    int pa = point(a);

    int pb = point(b);

    // Pointeur sur les points et le nombre d'échange(s) gagné utilisé par la fonction nextEnchange.
    int *pta = &a;
    int *ptb = &b;
    int *ptpa = &pa;
    int *ptpb = &pb;

    std::cout << "Valeur de A : " << a << " point de A : " << pa << std::endl;
    std::cout << "Valeur de B : " << b << " point de B : " << pb << std::endl;

    // Booleen pour arrêter la boucle quand le match est fini
    bool finish = false;

    while (!finish)
    {
        // Si A et B ont moins de 40 points chacun
        if (pa < 40 && pb < 40)
        {
            std::cout << "Nombre de point(s) de A : " << pa << " , Nombre de point(s) de B : " << pb << std::endl;
            echangeWinner(pta, ptpa, ptb, ptpb);
        }
        // Si un des deux joueurs à 40 points et l'autre moins.
        else if ((pa < 40 && pb == 40) || (pb < 40 && pa == 40))
        {
            // Si A a 40 points et a gagné 4 échanges.
            if (pa == 40 && a == 4)
            {
                std::cout << "A gagne le match " << pa << " à " << pb << " !" << std::endl;
                finish = true;
            }
            // Si B a 40 points et a gagné 4 échanges.
            else if (pb == 40 && b == 4)
            {
                std::cout << "B gagne le match " << pb << " à " << pa << " !" << std::endl;
                finish = true;
            }
            // Si A a 40 points et a gagné 3 échanges.
            else if (pa == 40 && a == 3)
            {
                std::cout << "A à la balle de match " << pa << " à " << pb << " !" << std::endl;
                echangeWinner(pta, ptpa, ptb, ptpb);
            }
            // Si B a 40 points et a gagné 3 échanges.
            else if (pb == 40 && b == 3)
            {
                std::cout << "B à la balle de match " << pb << " à " << pa << " !" << std::endl;
                echangeWinner(pta, ptpa, ptb, ptpb);
            }
            // Valeurs impossibles comme par exemple A qui gagne 6 échanges et B qui en gagne 2
            // (A aurait déjà du gagner deux échanges avant et donc le dernier échange n'aurait pas dû avoir lieu).
            else
            {
                std::cout << "Erreur dans les paramètres renseignés !" << std::endl;
            }
        }
        // Si A et B ont tout les deux 40 points.
        else
        {
            // Si A à gagné un échange de plus que B.
            if (a - b == 1)
            {
                std::cout << "Le jeu est à égalité, A à la balle de match !" << std::endl;
                echangeWinner(pta, ptpa, ptb, ptpb);
            }
            // Si B à gagné un échange de plus que A.
            else if (b - a == 1)
            {
                std::cout << "Le jeu est à égalité, B à la balle de match !" << std::endl;
                echangeWinner(pta, ptpa, ptb, ptpb);
            }
            // Si A à gagné deux échanges de plus que B.
            else if (a - b == 2)
            {
                std::cout << "A gagne match !" << std::endl;
                finish = true;
            }
            // Si B à gagné deux échanges de plus que A.
            else if (b - a == 2)
            {
                std::cout << "B gagne match !" << std::endl;
                finish = true;
            }
            // Si A et B ont gagné le même nombre d'échange(s).
            else if (a == b)
            {
                std::cout << "Egalité !" << std::endl;
                echangeWinner(pta, ptpa, ptb, ptpb);
            }
            // Valeurs impossibles comme par exemple A qui gagne 4 échanges et B qui en gagne 7
            // (B aurait déjà du gagner un échange avant et donc le dernier échange n'aurait pas dû avoir lieu).
            else
            {
                std::cout << "Erreur dans les paramètres renseignés !" << std::endl;
            }
        }
    }
}