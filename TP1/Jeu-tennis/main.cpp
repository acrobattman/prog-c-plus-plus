/**
 * @file main.cpp
 * @brief Séance 1 - les bases du développement C++
 * @author Maxime ECKSTEIN
 * @date 26 Septembre 2020
 * 
 * Partie II - Jeu de tennis
 * 
 **/

#include <iostream>
#include "tennis.h"

int main()
{
    match();
    return 0;
}